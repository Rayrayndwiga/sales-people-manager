﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SalesPeople
{
    public partial class PrintingWindow : Form
    {
        private readonly Form1 f1;
        public PrintingWindow(Form1 form)
        {
            InitializeComponent();
            f1 = form;
        }

        /*This event prevents one from choosing multiple items in the checked
         * listbox
         */
        private void printCheckedListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (printCheckedListBox.CheckedItems.Count >= 1 && e.CurrentValue != CheckState.Checked)
            {
                e.NewValue = e.CurrentValue;
            }
        }

        private void searchPrintButton_Click(object sender, EventArgs e)
        {
            try
            {
                errorProvider.Clear();

                if (printTextBox.Text != "")
                {
                    if (f1.SDB.NameContained(printTextBox.Text) == true)
                    {
                        f1.SDB.ListClientName(printTextBox.Text, printCheckedListBox.Items);
                        printTextBox.Text = "";
                    }
                    else
                    {
                        throw new Exception("This name is not associated with any of the sales people");
                    }
                }
                else
                {
                    errorProvider.SetError(printButton, "Fill in a name");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        
    }
}
