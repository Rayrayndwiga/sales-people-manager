﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.Drawing.Printing;
using System.IO;

namespace SalesPeople
{
    public partial class TeamWindow : Form
    {
        private readonly Form1 f1;
        public TeamWindow(Form1 form)
        {
            InitializeComponent();
            f1=form;
        }

        private void teamsListBox_Click(object sender, EventArgs e)
        {
            if (teamsListBox.SelectedItem != null)
            {
                string name = teamsListBox.SelectedItem.ToString();

                for (int i = 0; i < f1.SDB.totalTeams; i++)
                {
                    if (f1.SDB.myTeams[i].IsTeamName(name))
                    {
                        TeamInformation teaminfo = new TeamInformation(f1);

                        teaminfo.teamNameTexBox.Text = f1.SDB.myTeams[i].TeamName;
                        teaminfo.AreaNameTextBox.Text = f1.SDB.myTeams[i].TeamAreaName;
                        f1.SDB.AddTeamAmount();
                        teaminfo.salesTextBox.Text = f1.SDB.myTeams[i].Amount.ToString();
                        teaminfo.memberNumberTextBox.Text = f1.SDB.myTeams[i].Members.ToString();
                        teaminfo.teamLeaderNameTextBox.Text = f1.SDB.myTeams[i].TeamLeaderName;

                        teaminfo.ShowDialog();
                    }
                }
            }
        }

        /*This click event is used to add a team by opening the form 
         * where you can fill out the details of the new team
         */
        private void addTeamButton_Click(object sender, EventArgs e)
        {
            AddTeamWindow newTeam = new AddTeamWindow(f1);
            f1.SDB.Regions(newTeam.operationAreaComboBox.Items);
            newTeam.ShowDialog();
        }

        /*This click event is used to list all the teams
         */
        private void listTeamButton_Click(object sender, EventArgs e)
        {
            f1.SDB.ListTeams(teamsListBox.Items);
        }

        /*This click event is used to remove a team
         */
        private void removeTeamButton_Click(object sender, EventArgs e)
        {
            removeTeam teamRemove = new removeTeam();

            f1.SDB.ListTeams(teamRemove.removeTeamComboBox.Items);

            if (teamRemove.ShowDialog() == DialogResult.OK)
            {
                if (!f1.SDB.RemoveTeam(teamRemove.removeTeamComboBox.Text))
                {
                    MessageBox.Show("Cannot remove this team");
                }
                else
                {
                    MessageBox.Show("Team has been successfully removed", "Removed Team", MessageBoxButtons.OK);
                }

            }
        }

        /*This click event opens the form where one can change the details of a team that is already present
         */
        private void updateTeamDetailsButton_Click(object sender, EventArgs e)
        {
            try
            {
                Team_ToChange change = new Team_ToChange();

                f1.SDB.ListTeams(change.teamToChangeComboBox.Items);

                if (change.ShowDialog() == DialogResult.OK)
                {
                    if (!f1.SDB.IsATeamName(change.teamToChangeComboBox.Text))
                    {
                        MessageBox.Show("This is not a Team in the database", "Team not found", MessageBoxButtons.OK);
                    }
                    else
                    {
                        ChangeTeam f4 = new ChangeTeam(f1, change.teamToChangeComboBox.Text);

                        for (int i = 0; i < f1.SDB.totalTeams; i++)
                        {
                            if (f1.SDB.myTeams[i] != null)
                            {
                                if (f1.SDB.myTeams[i].IsTeamName(change.teamToChangeComboBox.Text))
                                {
                                    f4.updateTeamNameTextBox.Text = f1.SDB.myTeams[i].TeamName;
                                    f4.updateTeamOperationAreaTextBox.Text = f1.SDB.myTeams[i].TeamAreaName;
                                    f4.updateTeamLeaderTextBox.Text = f1.SDB.myTeams[i].TeamLeaderName;
                                }
                            }
                        }

                        f4.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*This event is used to add members to a team
         */
        private void addTeamMembersButton_Click(object sender, EventArgs e)
        {
            AddMember addMember = new AddMember();

            f1.SDB.ListTeams(addMember.teamListComboBox.Items);

            if (addMember.ShowDialog(this) == DialogResult.OK)
            {
                if (!f1.SDB.IsATeamName(addMember.teamListComboBox.Text))
                {
                    MessageBox.Show("This is not a team in the database");
                }
                else
                {
                    AddMemberWindow members = new AddMemberWindow(f1);

                    

                    members.teamLabelName.Text = addMember.teamListComboBox.Text + " " + "Team";
                    f1.SDB.RegionMembers(addMember.teamListComboBox.Text, members.membersCheckedListBox.Items);
                    f1.SDB.TeamMembersList(members.teamMembersListBox.Items, addMember.teamListComboBox.Text);
                    members.teamName = addMember.teamListComboBox.Text;
                    members.ShowDialog(this);
                }

            }
        }

        /*This event is used to remove team members
         */
        private void removeTeamMembersButton_Click(object sender, EventArgs e)
        {
            RemoveMember removeMembers = new RemoveMember();

            f1.SDB.ListTeams(removeMembers.removeListComboBox.Items);

            if (removeMembers.ShowDialog(this) == DialogResult.OK)
            {
                if (!f1.SDB.IsATeamName(removeMembers.removeListComboBox.Text))
                {
                    MessageBox.Show("This is not a team in the database");
                }
                else
                {
                    RemoveMembersWindow members = new RemoveMembersWindow(f1);

                    f1.SDB.TeamMembersList(members.removeMembersCheckedListBox.Items, removeMembers.removeListComboBox.Text);
                    f1.SDB.TeamMembersList(members.removeMembersListBox.Items, removeMembers.removeListComboBox.Text);
                    members.teamName = removeMembers.removeListComboBox.Text;
                    members.ShowDialog(this);
                }
            }
        }

        /*This event is used to search for teams that have sales over a value entered by the user
         */
        private void searchTeamSalesOverButton_Click(object sender, EventArgs e)
        {
            try
            {
                float total;
                bool isValid = float.TryParse(searchTeamSalesOverTextBox.Text, NumberStyles.Currency, CultureInfo.GetCultureInfo("en-US"), out total);
                if (searchTeamSalesOverTextBox.Text != "")
                {
                    if (isValid != true)
                    {
                        throw new Exception("Enter a valid amount");
                    }
                    else
                    {
                        f1.SDB.TeamsSalesOver(total, teamsListBox.Items);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*This event is used to print a record of a selected sales person
         */
        private void selectedSalespersonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int staffnumber;
            string name;
            PrintingWindow printWindow = new PrintingWindow(f1);

            if (printWindow.ShowDialog() == DialogResult.OK)
            {
                if (printWindow.printCheckedListBox.CheckedItems.Count > 0)
                {
                    name = printWindow.printCheckedListBox.CheckedItems[0].ToString();

                    string[] names = name.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                    staffnumber = Convert.ToInt32(names[1]);

                    f1.SDB.PageToPrintSelectedPerson(staffnumber);
                }
            }

            PrintDocument document = new PrintDocument();

            document.PrintPage += new PrintPageEventHandler(f1.print.PrintTextHandler);

            PrintDialog printDG = new PrintDialog();

            if (printDG.ShowDialog() == DialogResult.OK)
            {
                document.Print();
            }
        }

        /*This click event is used to print all the sales people records
         */
        private void allSalespeopleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            f1.SDB.PageToPrintAllPeople();

            PrintDocument document = new PrintDocument();

            document.PrintPage += new PrintPageEventHandler(f1.print.PrintTextHandler);

            PrintDialog printDG = new PrintDialog();

            if (printDG.ShowDialog() == DialogResult.OK)
            {
                document.Print();
            }
        }

        /*This click event is used to print a selected record of a team
         */
        private void selectedTeamToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string name;

            PrintingWindow printWindow = new PrintingWindow(f1);

            printWindow.printTextBox.Visible = false;
            printWindow.label1.Visible = false;
            printWindow.label3.Visible = false;
            printWindow.searchPrintButton.Visible = false;

            f1.SDB.ListTeams(printWindow.printCheckedListBox.Items);

            if (printWindow.ShowDialog() == DialogResult.OK)
            {
                if (printWindow.printCheckedListBox.CheckedItems.Count > 0)
                {
                    name = printWindow.printCheckedListBox.CheckedItems[0].ToString();

                    f1.SDB.PageToPrintSelectedTeam(name);
                }
            }

            PrintDocument document = new PrintDocument();

            document.PrintPage += new PrintPageEventHandler(f1.print.PrintTextHandler);

            PrintDialog printDG = new PrintDialog();

            if (printDG.ShowDialog() == DialogResult.OK)
            {
                document.Print();
            }
        }

        /*This event is used to print all the team records
         */
        private void allTeamsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            f1.SDB.PageToPrintAllTeams();

            PrintDocument document = new PrintDocument();

            document.PrintPage += new PrintPageEventHandler(f1.print.PrintTextHandler);

            PrintDialog printDG = new PrintDialog();

            if (printDG.ShowDialog() == DialogResult.OK)
            {
                document.Print();
            }
        }

        /*This click event shows the relevant information about the system
         */
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileStream about = new FileStream(@"..\..\Help\About.txt", FileMode.Open);

            StreamReader aboutSR = new StreamReader(about);

            About aboutWindow = new About();

            aboutWindow.aboutTextBox.Text = aboutSR.ReadToEnd();

            aboutSR.Close();

            aboutWindow.Show(this);
        }

        /*This click event opens the help window which shows the help information for 
         * using the system
         */
        private void helpToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FileStream help = new FileStream(@"..\..\Help\Help.txt", FileMode.Open);

            StreamReader helpSR = new StreamReader(help);

            Help helpWindow = new Help();

            helpWindow.helpTextBox.Text = helpSR.ReadToEnd();

            helpSR.Close();

            helpWindow.Show(this);
        }

        /*This click event is used to save details into a file that is already open
         */
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (File.Exists(f1.openFile))
            {
                FileStream existing = new FileStream(f1.openFile, FileMode.Truncate);

                StreamWriter existingSW = new StreamWriter(existing);

                for (int i = 0; i < f1.SDB.totalSalesPeople; i++)
                {
                    if (f1.SDB.mySalesPeople[i] != null || f1.SDB.myTeams[i] != null)
                    {
                        if (f1.SDB.mySalesPeople[i] != null)
                        {
                            if (f1.SDB.mySalesPeople[i].HomeAddress == "")
                            {
                                f1.SDB.mySalesPeople[i].HomeAddress = "null";
                            }
                            if (f1.SDB.mySalesPeople[i].Email == "")
                            {
                                f1.SDB.mySalesPeople[i].Email = "null";
                            }
                            if (f1.SDB.mySalesPeople[i].Country == "")
                            {
                                f1.SDB.mySalesPeople[i].Country = "null";
                            }
                            if (f1.SDB.mySalesPeople[i].Region == "")
                            {
                                f1.SDB.mySalesPeople[i].Region = "null";
                            }
                            if (f1.SDB.mySalesPeople[i].teamName == "")
                            {
                                f1.SDB.mySalesPeople[i].teamName = "null";
                            }

                            if (f1.SDB.myTeams[i] != null)
                            {
                                if (f1.SDB.myTeams[i].TeamLeaderName == "")
                                {
                                    f1.SDB.myTeams[i].TeamLeaderName = "null";
                                }

                                existingSW.WriteLine(f1.SDB.mySalesPeople[i].FullName + ";" + f1.SDB.mySalesPeople[i].Birthday + ";" + f1.SDB.mySalesPeople[i].Number + ";" + f1.SDB.mySalesPeople[i].Email + ";" + f1.SDB.mySalesPeople[i].HomeAddress + ";" + f1.SDB.mySalesPeople[i].Country + ";" + f1.SDB.mySalesPeople[i].Region + ";" + f1.SDB.mySalesPeople[i].Sales + ";" + f1.SDB.mySalesPeople[i].TeamName + ";" + f1.SDB.mySalesPeople[i].StaffNumber + ">" + f1.SDB.myTeams[i].TeamName + ":" + f1.SDB.myTeams[i].TeamAreaName + ":" + f1.SDB.myTeams[i].TeamLeaderName + ":" + f1.SDB.myTeams[i].Members + ":" + f1.SDB.myTeams[i].Amount);
                            }
                            else
                            {
                                existingSW.WriteLine(f1.SDB.mySalesPeople[i].FullName + ";" + f1.SDB.mySalesPeople[i].Birthday + ";" + f1.SDB.mySalesPeople[i].Number + ";" + f1.SDB.mySalesPeople[i].Email + ";" + f1.SDB.mySalesPeople[i].HomeAddress + ";" + f1.SDB.mySalesPeople[i].Country + ";" + f1.SDB.mySalesPeople[i].Region + ";" + f1.SDB.mySalesPeople[i].Sales + ";" + f1.SDB.mySalesPeople[i].TeamName + ";" + f1.SDB.mySalesPeople[i].StaffNumber + ">");
                            }
                        }
                    }
                }

                existingSW.Close();
                existing.Close();
            }
        }

        /*This click event is used to save a salesperson record into a new file
         */
        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {

                SaveFileDialog saving = new SaveFileDialog();

                saving.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                saving.Title = "Save Personnel Data";
                saving.RestoreDirectory = true;
                String path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), @"..\..\Records");
                saving.InitialDirectory = path;

                saving.ShowDialog(this);

                if (saving.FileName != "")
                {

                    FileStream savingFS = new FileStream(saving.FileName, FileMode.OpenOrCreate);
                    StreamWriter savingSW = new StreamWriter(savingFS);

                    for (int i = 0; i < f1.SDB.GetConstant(); i++)
                    {
                        if (f1.SDB.mySalesPeople[i] != null || f1.SDB.myTeams[i] != null)
                        {
                            if (f1.SDB.mySalesPeople[i].HomeAddress == "")
                            {
                                f1.SDB.mySalesPeople[i].HomeAddress = "null";
                            }
                            if (f1.SDB.mySalesPeople[i].Email == "")
                            {
                                f1.SDB.mySalesPeople[i].Email = "null";
                            }
                            if (f1.SDB.mySalesPeople[i].Country == "")
                            {
                                f1.SDB.mySalesPeople[i].Country = "null";
                            }
                            if (f1.SDB.mySalesPeople[i].Region == "")
                            {
                                f1.SDB.mySalesPeople[i].Region = "null";
                            }
                            if (f1.SDB.mySalesPeople[i].teamName == "")
                            {
                                f1.SDB.mySalesPeople[i].teamName = "null";
                            }

                            if (f1.SDB.myTeams[i] != null)
                            {
                                if (f1.SDB.myTeams[i].TeamLeaderName == "")
                                {
                                    f1.SDB.myTeams[i].TeamLeaderName = "null";
                                }

                                savingSW.WriteLine(f1.SDB.mySalesPeople[i].FullName + ";" + f1.SDB.mySalesPeople[i].Birthday + ";" + f1.SDB.mySalesPeople[i].Number + ";" + f1.SDB.mySalesPeople[i].Email + ";" + f1.SDB.mySalesPeople[i].HomeAddress + ";" + f1.SDB.mySalesPeople[i].Country + ";" + f1.SDB.mySalesPeople[i].Region + ";" + f1.SDB.mySalesPeople[i].Sales + ";" + f1.SDB.mySalesPeople[i].TeamName + ";" + f1.SDB.mySalesPeople[i].StaffNumber + ">" + f1.SDB.myTeams[i].TeamName + ":" + f1.SDB.myTeams[i].TeamAreaName + ":" + f1.SDB.myTeams[i].TeamLeaderName + ":" + f1.SDB.myTeams[i].Members + ":" + f1.SDB.myTeams[i].Amount);
                            }
                            else
                            {
                                savingSW.WriteLine(f1.SDB.mySalesPeople[i].FullName + ";" + f1.SDB.mySalesPeople[i].Birthday + ";" + f1.SDB.mySalesPeople[i].Number + ";" + f1.SDB.mySalesPeople[i].Email + ";" + f1.SDB.mySalesPeople[i].HomeAddress + ";" + f1.SDB.mySalesPeople[i].Country + ";" + f1.SDB.mySalesPeople[i].Region + ";" + f1.SDB.mySalesPeople[i].Sales + ";" + f1.SDB.mySalesPeople[i].TeamName + ";" + f1.SDB.mySalesPeople[i].StaffNumber + ">");
                            }
                        }
                    }
                    savingSW.Close();
                    savingFS.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*This click event invokes the openfiledialog 
        */
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog opening = new OpenFileDialog();

                String path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), @"..\..\Records");

                opening.InitialDirectory = path;
                opening.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                opening.Title = "Open Saved Personnel Data";

                opening.ShowDialog();


                if (opening.FileName != "")
                {

                    List<string> personnelList = new List<string>();


                    for (int i = 0; i < f1.SDB.GetConstant(); i++)
                    {
                        if (f1.SDB.mySalesPeople[i] != null)
                        {
                            f1.SDB.mySalesPeople[i] = null;
                        }

                        if (f1.SDB.myTeams[i] != null)
                        {
                            f1.SDB.myTeams[i] = null;
                        }
                    }

                    FileStream openFS = new FileStream(opening.FileName, FileMode.Open);
                    f1.openFile = opening.FileName;

                    StreamReader openingSR = new StreamReader(openFS);

                    string lines;

                    while ((lines = openingSR.ReadLine()) != null)
                    {
                        personnelList.Add(lines);
                    }


                    foreach (string s in personnelList)
                    {
                        string[] info = s.Split(new string[] { ">" }, StringSplitOptions.RemoveEmptyEntries);

                        int n = info.Length;


                        string[] person = info[0].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

                        int m = person.Length;



                        if (n == 2)
                        {
                            string[] team = info[1].Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);

                            int k = team.Length;

                            //Adding the teams
                            f1.SDB.AddTeam(team[0], team[1]);

                            for (int i = 0; i < f1.SDB.GetConstant(); i++)
                            {
                                if (f1.SDB.myTeams[i] != null)
                                {
                                    if (f1.SDB.myTeams[i].IsTeamName(team[0]))
                                    {
                                        f1.SDB.myTeams[i].TeamLeaderName = team[2];
                                        f1.SDB.myTeams[i].Amount = Convert.ToInt32(team[4]);
                                    }
                                }
                            }

                            //Adding the Sales People

                            f1.SDB.AddSalesPerson(person[0], person[1], person[2], person[3], person[4], person[5], person[6], Convert.ToSingle(person[7]), Convert.ToInt32(person[9]));

                            for (int i = 0; i < f1.SDB.GetConstant(); i++)
                            {
                                if (f1.SDB.mySalesPeople[i] != null)
                                {
                                    if (f1.SDB.mySalesPeople[i].StaffNumber == Convert.ToInt32(person[9]))
                                    {
                                        f1.SDB.mySalesPeople[i].TeamName = person[8];
                                        f1.SDB.AddTeamMember(f1.SDB.mySalesPeople[i].FullName + ";" + f1.SDB.mySalesPeople[i].StaffNumber, f1.SDB.mySalesPeople[i].TeamName);
                                    }
                                }
                            }
                        }
                        else if (n == 1)
                        {
                            f1.SDB.AddSalesPerson(person[0], person[1], person[2], person[3], person[4], person[5], person[6], Convert.ToSingle(person[7]), Convert.ToInt32(person[9]));

                            for (int i = 0; i < f1.SDB.GetConstant(); i++)
                            {
                                if (f1.SDB.mySalesPeople[i] != null)
                                {
                                    if (f1.SDB.mySalesPeople[i].StaffNumber == Convert.ToInt32(person[9]))
                                    {
                                        f1.SDB.mySalesPeople[i].TeamName = person[8];
                                        f1.SDB.AddTeamMember(f1.SDB.mySalesPeople[i].FullName + ";" + f1.SDB.mySalesPeople[i].StaffNumber, f1.SDB.mySalesPeople[i].TeamName);
                                    }
                                }
                            }
                        }
                    }

                    openFS.Close();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        

        
    }
}
