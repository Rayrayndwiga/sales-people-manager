﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using System.Globalization;

namespace SalesPeople
{
    public class SalesPersonDB
    {
        public int totalSalesPeople;
        public int totalTeams;
        public SalesPerson[] mySalesPeople;
        public Teams[] myTeams;
        private string CompanyName;
        public const int MAX_SALESPEOPLE = 1000;
        public const int MAX_TEAMS = MAX_SALESPEOPLE;
        public List<string> teamnames = new List<string>();
         
        /*This is the constructor for the SalesDB class
         */
        public SalesPersonDB(string CompanyName)
        {
            this.CompanyName = CompanyName;
            totalSalesPeople = 0;
            mySalesPeople = new SalesPerson[MAX_SALESPEOPLE];
            myTeams = new Teams[MAX_TEAMS];
        }

        /*This method is used to add a salesperson
         */
        public bool AddSalesPerson(string Name,string Birthday,string number,string Email,string Address,string Country,string Area,float Sales,int StaffNumber)
        {
            int index = -1;

            for (int i = 0; i < MAX_SALESPEOPLE; ++i)
                if (mySalesPeople[i] == null)
                {
                    index = i;
                    break;
                }

            if (index != -1)
            {
                mySalesPeople[index] = new SalesPerson(Name, Birthday, number, Email, Address, Country, Area, Sales, StaffNumber);
                ++totalSalesPeople;
                return true;

            }
            return false;
        }

        /*This method is used to remove a salesperson
         */
        public bool RemoveSalesPerson(string salesPersonName)
        {
            int staffnumber = 0;

            string[] names = salesPersonName.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            staffnumber = Convert.ToInt32(names[1]);

            for (int i = 0; i < MAX_SALESPEOPLE; ++i)
            {
                if (mySalesPeople[i] != null)
                {
                    if (mySalesPeople[i].StaffNumber==staffnumber)
                    {
                        mySalesPeople[i] = null;
                        --totalSalesPeople;
                        return true;
                    }
                }
            }
            return false;
        }

        /*This method returns the value of the constants
         */
        public int GetConstant()
        {
            return MAX_SALESPEOPLE;
        }

        /*This method is used to check if the name passed into the method is the 
         * name of a salesperson
         */
        public bool IsASalesPerson(string salesPersonName)
        {
            for (int i = 0; i < MAX_SALESPEOPLE; ++i)
            {
                if (mySalesPeople[i] != null)
                {
                    if (mySalesPeople[i].IsSame(salesPersonName))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        
        /*This method is used to determine if the  staff number 
         * is a staff number of another sales person
         */
        public bool IsStaffNumber(int staffNumber)
        {
            for (int i = 0; i < MAX_SALESPEOPLE; i++)
            {
                if (mySalesPeople[i] != null)
                {
                    if (mySalesPeople[i].StaffNumber == staffNumber)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /*This is method is used to check if the name of the area passed is the name of 
         * an area of any sales person
         */
        public bool IsArea(string areaName)
        {
            for (int i = 0; i < MAX_SALESPEOPLE; i++)
            {
                if (mySalesPeople[i] != null)
                {
                    if (mySalesPeople[i].IsPersonArea(areaName))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /*This method checks if the string passed into the method is contained in
         * any of the sales persons names
         */
        public bool NameContained(string name)
        {
            for (int i = 0; i < MAX_SALESPEOPLE; i++)
            {
                if (mySalesPeople[i] != null)
                {
                    if (mySalesPeople[i].FullName.Contains(name))
                        return true;
                }
            }
            return false;
        }

        /*This method is used to list all the salespeople
         */
        public void ListClients(IList list)
        {
            list.Clear();
            for (int i = 0; i < MAX_SALESPEOPLE; ++i)
            {
                if (mySalesPeople[i] == null)
                    continue;
                list.Add(mySalesPeople[i].FullName + "   " + "StaffNumber;" + mySalesPeople[i].StaffNumber.ToString());
            }
        }

        /*This method lists all the salespeople whose name contains a certain string that is 
         * passed into the method
         */
        public void ListClientName(string name,IList namelist)
        {
            namelist.Clear();
            for (int i = 0; i < MAX_SALESPEOPLE; i++)
            {
                if (mySalesPeople[i] != null)
                {
                    if (mySalesPeople[i].FullName.Contains(name))
                    {
                        namelist.Add(mySalesPeople[i].FullName + "   " + "StaffNumber;" + mySalesPeople[i].StaffNumber.ToString());
                    }
                }
            }
        }

        /*This method is used to list all the sales people whose sales are above 1 million
         */
        public void overMillion(IList saleslist)
        {
            saleslist.Clear();

            for(int i=0;i<MAX_SALESPEOPLE;i++)
            {
                if(mySalesPeople[i]!=null)
                {
                    if(mySalesPeople[i].Sales>1000000)
                    {
                        saleslist.Add(mySalesPeople[i].FullName + "   " + "StaffNumber;" + mySalesPeople[i].StaffNumber.ToString());
                    }
                }
            }
        }

        /*This method is used to search for the sales people that have sales 
         * over a certain amount that the user inputs
         */
        public void OverSalesValue(IList list,float value)
        {
           list.Clear();

            for (int i = 0; i < MAX_SALESPEOPLE; i++)
            {
                if (mySalesPeople[i] != null)
                {
                    if (mySalesPeople[i].Sales > value)
                    {
                        list.Add(mySalesPeople[i].FullName + "   " + "StaffNumber;" + mySalesPeople[i].StaffNumber.ToString());
                    }
                }
            }
        }

        /*This method is used to list all the sales people from a particular area
         */
        public void AreaList(string area,IList areaList)
        {
            areaList.Clear();

            for (int i = 0; i < MAX_SALESPEOPLE; i++)
            {
                if (mySalesPeople[i] != null)
                {
                    if (mySalesPeople[i].Region == area)
                    {
                        areaList.Add(mySalesPeople[i].FullName + "   " + "StaffNumber;" + mySalesPeople[i].StaffNumber.ToString());
                    }
                }
            }
        }

        /*This method is used to add a team
         */
        public bool AddTeam(string teamName,string teamAreaName)
        {
            int index = -1;

            for (int i = 0; i < MAX_TEAMS; ++i)
                if (myTeams[i] == null)
                {
                    index = i;
                    break;
                }

            if (index != -1)
            {
                myTeams[index] = new Teams(teamName,teamAreaName);
                ++totalTeams;
                return true;
            }
            return false;
        }

        /*This method is used to remove a team
         */
        public bool RemoveTeam(string Teamname)
        {
            for (int i = 0; i < MAX_TEAMS; ++i)
            {
                if (myTeams[i] != null)
                {
                    if (myTeams[i].IsTeamName(Teamname))
                    {
                        myTeams[i] = null;
                        --totalTeams;
                        return true;
                    }
                }
            }
            return false;
        }


        /*This method is used to list the members of a particular region
         */
        public void RegionMembers(string name,IList regionMemberslist)
        {
            regionMemberslist.Clear();

            for (int i = 0; i < MAX_TEAMS; i++)
            {
                if (myTeams[i] != null)
                {
                    if (myTeams[i].IsTeamName(name))
                    {
                        for (int k = 0; k < MAX_SALESPEOPLE; k++)
                        {
                            if (mySalesPeople[k] != null)
                            {
                                if (mySalesPeople[k].Region == myTeams[i].TeamAreaName)
                                {
                                    if (!IsInTeam(mySalesPeople[k]))
                                    {
                                        regionMemberslist.Add(mySalesPeople[k].FullName + "   " + "StaffNumber;" + mySalesPeople[k].StaffNumber.ToString());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /*This method is used to add a member to a team
         */
        public bool AddTeamMember(string name,string teamName)
        {
            int staffnumber = 0;

            string[] names = name.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            staffnumber = Convert.ToInt32(names[1]);

            for (int i = 0; i < MAX_TEAMS; i++)
            {
                if (myTeams[i] != null)
                {
                    for (int k = 0; k < MAX_SALESPEOPLE; k++)
                    {
                        if (myTeams[i].IsTeamName(teamName))
                        {
                            if (mySalesPeople[k] != null)
                            {
                                if (mySalesPeople[k].StaffNumber == staffnumber)
                                {
                                    myTeams[i].TeamMembers.Add(mySalesPeople[k]);
                                    mySalesPeople[k].TeamName = myTeams[i].TeamName;
                                    myTeams[i].Members++;
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }

        /*This method is used to remove a team member from a team
         */
        public bool RemoveTeamMember(string name, string teamName)
        {
            int staffnumber = 0;

            string[] names = name.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            staffnumber = Convert.ToInt32(names[1]);


            for (int i = 0; i < MAX_TEAMS; i++)
            {
                if (myTeams[i] != null)
                {
                    if (myTeams[i].IsTeamName(teamName))
                    {
                        for (int k = 0; k < MAX_SALESPEOPLE; k++)
                        {
                            if (mySalesPeople[k] != null)
                            {
                                if (mySalesPeople[k].StaffNumber == staffnumber)
                                {
                                    myTeams[i].TeamMembers.Remove(mySalesPeople[k]);
                                    mySalesPeople[k].TeamName = "";
                                    myTeams[i].Members--;
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }

        /*This method is used to get the name of the team that a 
         * particular sales person belongs to
         */
        public string memberTeamName(string name)
        {
            string teamname="";

            int staffnumber = 0;

            string[] names = name.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            staffnumber = Convert.ToInt32(names[1]);

            for (int i = 0; i < MAX_TEAMS; i++)
            {
                if (myTeams[i] != null)
                {
                    for (int k = 0; k < MAX_SALESPEOPLE; k++)
                    {
                        if (mySalesPeople[k] != null)
                        {
                            foreach (object person in myTeams[i].TeamMembers)
                            {
                                if (mySalesPeople[k].StaffNumber == staffnumber)
                                {
                                    if (person == mySalesPeople[k])
                                    {
                                        teamname = myTeams[i].TeamName;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return teamname;
        }

        /*This method is used to check if a sales person is a team leader
         */
        public bool IsTeamLeader(string name,string teamname)
        {
            int staffnumber = 0;
            int n;
            int teamleaderstaffnumber = 0;

            string[] names = name.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            staffnumber = Convert.ToInt32(names[1]);

            for (int i = 0; i < MAX_TEAMS; i++)
            {
                if(myTeams[i] != null)
                {
                    if (myTeams[i].TeamName == teamname)
                    {
                        string[] leadernames = myTeams[i].TeamLeaderName.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

                        n = leadernames.Length;

                        if (n > 1)
                        {
                            teamleaderstaffnumber = Convert.ToInt32(leadernames[1]);
                        }

                        for (int k = 0; k < MAX_SALESPEOPLE; k++)
                        {
                            if (mySalesPeople[k] != null)
                            {
                                if (mySalesPeople[k].StaffNumber == staffnumber)
                                {
                                    if (mySalesPeople[k].StaffNumber == teamleaderstaffnumber)
                                    {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }

        /*This method removes a team leader from a team if the person has
         * been removed as a sales person or as a team member
         */
        public void RemoveTeamLeader(string teamnName)
        {
            for (int i = 0; i < MAX_TEAMS; i++)
            {
                if (myTeams[i] != null)
                {
                    if (myTeams[i].IsTeamName(teamnName))
                    {
                        myTeams[i].TeamLeaderName = "";
                    }
                }
            }
        }

        /*This method is used to compute the total sales of members in the team
         */
        public void AddTeamAmount()
        {
            double total = 0;
            for (int i = 0; i < MAX_TEAMS; i++)
            {
                if (myTeams[i] != null)
                {
                    for (int k = 0; k < MAX_SALESPEOPLE; k++)
                    {
                        if (mySalesPeople[k] != null)
                        {
                            foreach (object person in myTeams[i].TeamMembers)
                            {
                                if (mySalesPeople[k] == person)
                                {
                                    total = total + mySalesPeople[k].Sales;
                                }
                            }
                        }
                    }
                    myTeams[i].Amount = total;
                    total = 0;
                }   
            }
        }

        /*This method is used to search for teams whose total amount is greater than 
         * an amount the user enters in the form
         */
        public void TeamsSalesOver(float total,IList list)
        {
            list.Clear();
            AddTeamAmount();
            for (int i = 0; i < MAX_TEAMS; i++)
            {
                if (myTeams[i] != null)
                {
                    if (myTeams[i].Amount > total)
                    {
                        list.Add(myTeams[i].TeamName);
                    }
                }
            }
        }

        /*This method checks if the name that is passed into ot is the name of a particular team
         */
        public bool IsATeamName(string teamName)
        {
            for (int i = 0; i < MAX_TEAMS; ++i)
            {
                if (myTeams[i] != null)
                {
                    if (myTeams[i].IsTeamName(teamName))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        

        /*This method is used to count the number of teams in an region
         */
        public int NumberTeamsRegion(string region)
        {
            int number = 0;

            for (int i = 0; i < MAX_TEAMS; i++)
            {
                if (myTeams[i] != null)
                {
                    if (myTeams[i].TeamAreaName == region)
                    {
                        number = number + 1;
                    }
                }
            }

            return number;
        }

        /*This method is used to check if a team has been removed and then used to 
         * determine the name of the next team that will be added in a particular region
         */
        public int ChangedTeams(string region)
        {
            int teamnumber;
            int numberofteams;
            int value = 0;
            for (int i = 0; i < MAX_TEAMS; i++)
            {
                if (myTeams[i] != null)
                {
                    string[] names = myTeams[i].TeamName.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    teamnumber = Convert.ToInt32(names[1]);

                    numberofteams = NumberTeamsRegion(region);
                    if (numberofteams == 0)
                    {
                        value = 0;
                    }
                    else if (teamnumber > numberofteams)
                    {
                        value = teamnumber - 2;
                    }
                    else
                    {
                        value = numberofteams;
                    }
                }
            }
            return value;
        }

        /*This method is used to check if a particular sales person is in a particular team
         */
        public bool IsInTeam(SalesPerson person)
        {
            
            for (int i = 0; i < MAX_TEAMS; i++)
            {
                if (myTeams[i] != null)
                {
                    for (int k = 0; k < MAX_SALESPEOPLE; k++)
                    {
                        if (mySalesPeople[k] != null)
                        {
                            if (mySalesPeople[k] == person)
                            {
                                foreach (object salesperson in myTeams[i].TeamMembers)
                                {
                                    if (mySalesPeople[k] == salesperson)
                                    {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }

        /*This method is used to list all the names of all the teams
         */
        public void ListTeams(IList list)
        {
            list.Clear();

            for (int i = 0; i < MAX_TEAMS; i++)
            {
                if (myTeams[i] != null)
                {
                    list.Add(myTeams[i].TeamName);
                }
            }
        }

        /*This method is used to list all the names of sales people in a particular team
         */
        public void TeamMembersList(IList memberList,string Team)
        {
            memberList.Clear();

            for (int i = 0; i < MAX_TEAMS; i++)
            {
                if (myTeams[i] != null)
                {
                    if(myTeams[i].IsTeamName(Team))
                    {
                        for (int k = 0; k < MAX_SALESPEOPLE;k++)
                        {
                            foreach (object person in myTeams[i].TeamMembers)
                            {
                                if (person == mySalesPeople[k])
                                {
                                    memberList.Add(mySalesPeople[k].FullName + " " + "StaffNumber;" + mySalesPeople[k].StaffNumber);
                                }
                            } 
                        }
                    }
                }
            }
        }

        /*This method add the list of countries to the combobox in the forms 
         */
        public void Countries(IList countryList)
        {
            List<string> countriesList = new List<string>();

            FileStream countriesFile = new FileStream(@"..\..\Countries\Countries.txt", FileMode.Open);
            StreamReader countriesSR = new StreamReader(countriesFile);

            string lines;

            while ((lines = countriesSR.ReadLine()) != null)
            {
                countriesList.Add(lines);
            }


            IEnumerable<string> countryNames = countriesList.OrderBy(names => names).Distinct();

            foreach (string name in countryNames)
            {
                countryList.Add(name);
            }

            countriesSR.Close();
        }

        /*This method is used to add the names regions of the world from a text file
         */
        public void Regions(IList regionList)
        {
            List<string> regions=new List<string>();

            FileStream regionsFile = new FileStream(@"..\..\Countries\Regions.txt",FileMode.Open);
            StreamReader regionsSR = new StreamReader(regionsFile);

            string lines;

            while ((lines = regionsSR.ReadLine()) != null)
            {
                regions.Add(lines);
            }


            IEnumerable<string> regionNames = regions.OrderBy(names => names).Distinct();

            foreach (string name in regionNames)
            {
                regionList.Add(name);
            }

            regionsSR.Close();
        }

        /*This method is used to create the page to be printed for a
         * selected sales person
         */
        public void PageToPrintSelectedPerson(int number)
        {
            for (int i = 0; i < MAX_SALESPEOPLE; i++)
            {
                if (mySalesPeople[i] != null)
                {
                    if (mySalesPeople[i].StaffNumber == number)
                    {
                        FileStream to_print = new FileStream(@"..\..\Printing\recordprint.txt", FileMode.Create);
                        StreamWriter printSW = new StreamWriter(to_print);


                        printSW.WriteLine("Full Name: {0}", mySalesPeople[i].FullName);
                        printSW.WriteLine("Staff Number: {0}",mySalesPeople[i].StaffNumber);
                        printSW.WriteLine("Date Of Birth: {0}",mySalesPeople[i].Birthday);
                        printSW.WriteLine("Email Address: {0}",mySalesPeople[i].Email);
                        printSW.WriteLine("Home Address {0}",mySalesPeople[i].HomeAddress);
                        printSW.WriteLine("Phone Number: {0}",mySalesPeople[i].Number);
                        printSW.WriteLine("Country: {0}",mySalesPeople[i].Country);
                        printSW.WriteLine("Region: {0}",mySalesPeople[i].Region);
                        printSW.WriteLine("Total Sales: {0}",mySalesPeople[i].Sales);
                        printSW.WriteLine("Team Name: {0}",mySalesPeople[i].TeamName);

                        printSW.Close();
                        to_print.Close();
                    }
                }
            }
        }

        /*This method is used to create the page to print all the sales peoples records
         */
        public void PageToPrintAllPeople()
        {
            FileStream to_print = new FileStream(@"..\..\Printing\recordprint.txt", FileMode.Create);
            StreamWriter printSW = new StreamWriter(to_print);
            for (int i = 0; i < MAX_SALESPEOPLE; i++)
            {
                if (mySalesPeople[i] != null)
                {
                    printSW.WriteLine("Sales Person {0}", i + 1);
                    printSW.WriteLine("------------------------------------------------");
                    printSW.WriteLine("Full Name: {0}", mySalesPeople[i].FullName);
                    printSW.WriteLine("Staff Number: {0}", mySalesPeople[i].StaffNumber);
                    printSW.WriteLine("Date Of Birth: {0}", mySalesPeople[i].Birthday);
                    printSW.WriteLine("Email Address: {0}", mySalesPeople[i].Email);
                    printSW.WriteLine("Home Address {0}", mySalesPeople[i].HomeAddress);
                    printSW.WriteLine("Phone Number: {0}", mySalesPeople[i].Number);
                    printSW.WriteLine("Country: {0}", mySalesPeople[i].Country);
                    printSW.WriteLine("Region: {0}", mySalesPeople[i].Region);
                    printSW.WriteLine("Total Sales: {0}", mySalesPeople[i].Sales);
                    printSW.WriteLine("Team Name: {0}", mySalesPeople[i].TeamName);
                    printSW.WriteLine("\n\n");
                }
            }
            printSW.Close();
            to_print.Close();
        }

        /*This method is used to create the page to print 
         * a selected team record
         */
        public void PageToPrintSelectedTeam(string name)
        {
            for (int i = 0; i < MAX_TEAMS; i++)
            {
                if (myTeams[i] != null)
                {
                    AddTeamAmount();
                    if (myTeams[i].IsTeamName(name))
                    {
                        FileStream to_print = new FileStream(@"..\..\Printing\recordprint.txt", FileMode.Create);
                        StreamWriter printSW = new StreamWriter(to_print);


                        printSW.WriteLine("Team Name: {0}", myTeams[i].TeamName);
                        printSW.WriteLine("Region: {0}", myTeams[i].TeamAreaName);
                        printSW.WriteLine("Team Leader: {0}", myTeams[i].TeamLeaderName);
                        printSW.WriteLine("Number of Team Members: {0}", myTeams[i].Members);
                        printSW.WriteLine("Total Sales {0}", myTeams[i].Amount);
                        printSW.WriteLine("Team Members:");
                        printSW.WriteLine("************************************************");
                        for (int k = 0; k < myTeams[i].TeamMembers.Count; k++)
                        {
                            printSW.WriteLine(myTeams[i].TeamMembers[k].FullName + "     "+ "StaffNumber  {0}",myTeams[i].TeamMembers[k].StaffNumber);
                        }


                        printSW.Close();
                        to_print.Close();
                    }
                }
            }
        }

        /*This method is used to create the page to print 
         * all team records
         */
        public void PageToPrintAllTeams()
        {
            FileStream to_print = new FileStream(@"..\..\Printing\recordprint.txt", FileMode.Create);
            StreamWriter printSW = new StreamWriter(to_print);

            for (int i = 0; i < MAX_TEAMS; i++)
            {
                if (myTeams[i] != null)
                {
                    AddTeamAmount();
                    printSW.WriteLine("Team {0}", i + 1);
                    printSW.WriteLine("------------------------------------------------");
                    printSW.WriteLine("Team Name: {0}", myTeams[i].TeamName);
                    printSW.WriteLine("Region: {0}", myTeams[i].TeamAreaName);
                    printSW.WriteLine("Team Leader: {0}", myTeams[i].TeamLeaderName);
                    printSW.WriteLine("Number of Team Members: {0}", myTeams[i].Members);
                    printSW.WriteLine("Total Sales {0}", myTeams[i].Amount);
                    printSW.WriteLine("Team Members:");
                    printSW.WriteLine("************************************************");
                    for (int k = 0; k < myTeams[i].TeamMembers.Count; k++)
                    {
                        printSW.WriteLine(myTeams[i].TeamMembers[k].FullName + "     " + "StaffNumber  {0}", myTeams[i].TeamMembers[k].StaffNumber);
                    }
                    printSW.WriteLine("\n\n\n");
                }
            }
            printSW.Close();
            to_print.Close();
        }
    }
}
