﻿namespace SalesPeople
{
    partial class removeTeam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.okRemoveTeamButton = new System.Windows.Forms.Button();
            this.cancleRemoveTeamButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.removeTeamComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // okRemoveTeamButton
            // 
            this.okRemoveTeamButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okRemoveTeamButton.Location = new System.Drawing.Point(23, 89);
            this.okRemoveTeamButton.Name = "okRemoveTeamButton";
            this.okRemoveTeamButton.Size = new System.Drawing.Size(75, 23);
            this.okRemoveTeamButton.TabIndex = 0;
            this.okRemoveTeamButton.Text = "OK";
            this.okRemoveTeamButton.UseVisualStyleBackColor = true;
            // 
            // cancleRemoveTeamButton
            // 
            this.cancleRemoveTeamButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancleRemoveTeamButton.Location = new System.Drawing.Point(181, 89);
            this.cancleRemoveTeamButton.Name = "cancleRemoveTeamButton";
            this.cancleRemoveTeamButton.Size = new System.Drawing.Size(75, 23);
            this.cancleRemoveTeamButton.TabIndex = 1;
            this.cancleRemoveTeamButton.Text = "Cancel";
            this.cancleRemoveTeamButton.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(186, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Choose the team you want to remove ";
            // 
            // removeTeamComboBox
            // 
            this.removeTeamComboBox.FormattingEnabled = true;
            this.removeTeamComboBox.Location = new System.Drawing.Point(23, 52);
            this.removeTeamComboBox.Name = "removeTeamComboBox";
            this.removeTeamComboBox.Size = new System.Drawing.Size(231, 21);
            this.removeTeamComboBox.TabIndex = 4;
            this.removeTeamComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.removeTeamComboBox_KeyPress);
            // 
            // removeTeam
            // 
            this.AcceptButton = this.okRemoveTeamButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancleRemoveTeamButton;
            this.ClientSize = new System.Drawing.Size(284, 138);
            this.ControlBox = false;
            this.Controls.Add(this.removeTeamComboBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cancleRemoveTeamButton);
            this.Controls.Add(this.okRemoveTeamButton);
            this.Name = "removeTeam";
            this.Text = "Remove Team";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button okRemoveTeamButton;
        private System.Windows.Forms.Button cancleRemoveTeamButton;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.ComboBox removeTeamComboBox;
    }
}