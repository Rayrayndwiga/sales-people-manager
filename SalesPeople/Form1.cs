﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Printing;

namespace SalesPeople
{
    public partial class Form1 : Form
    {
        public SalesPersonDB SDB;
        public string openFile;
        public Printing print = new Printing();
        public Form1()
        {
            InitializeComponent();
            SDB = new SalesPersonDB("SALES IN THE CENTURY PTY LTD");
        }

        

        /*This click event opens the form for one to add a new salesperson
         */
        private void AddPersonButton_Click(object sender, EventArgs e)
        {
            Sales_Person f2 = new Sales_Person(this);
            SDB.Countries(f2.AreaComboBox.Items);
            f2.ShowDialog(this);
        }

        /*This event sets out what happens when the form is closing
         */
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult cl = MessageBox.Show("Are you sure you want to quit?", "Quit", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (cl == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        /*This click event is used to close this form
         */
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /*This click event is used to remove a salesperson from the record
         */
        private void removeSalespersonButton_Click(object sender, EventArgs e)
        {
            
                remove salesRemove = new remove(this);

                salesRemove.ShowDialog(this);
        }


        /*This cick event opens the form for one to be able to update the details
         * of a salesperson
         */
        private void UpdateDetailsButton_Click(object sender, EventArgs e)
        {
            salesPersonChange change = new salesPersonChange(this);

            change.ShowDialog(this);
        }

        /*This click event opens the form that contains details about teams
         */
        private void showTeamsButton_Click(object sender, EventArgs e)
        {
            TeamWindow team = new TeamWindow(this);

            team.ShowDialog(this);  
        }

        

        /*This click event invokes the openfiledialog 
         */
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog opening = new OpenFileDialog();

                String path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal),@"..\..\..\Records");

                opening.InitialDirectory = path;
                opening.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                opening.Title = "Open Saved Personnel Data";

                opening.ShowDialog();


                if (opening.FileName != "")
                {
                    
                    List<string> personnelList = new List<string>();


                    for (int i = 0; i < SDB.GetConstant(); i++)
                    {
                        if (SDB.mySalesPeople[i] != null)
                        {
                            SDB.mySalesPeople[i] = null;
                        }

                        if (SDB.myTeams[i] != null)
                        {
                            SDB.myTeams[i] = null;
                        }
                    }

                    FileStream openFS = new FileStream(opening.FileName, FileMode.Open);
                    openFile = opening.FileName;
                    
                    StreamReader openingSR = new StreamReader(openFS);

                    string lines;

                    while ((lines = openingSR.ReadLine()) != null)
                    {
                        personnelList.Add(lines);
                    }


                    foreach (string s in personnelList)
                    {
                        string[] info = s.Split(new string[] { ">" }, StringSplitOptions.RemoveEmptyEntries);

                        int n = info.Length;


                        string[] person = info[0].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

                        int m = person.Length;

                        

                        if (n == 2)
                        {
                            string[] team = info[1].Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);

                            int k = team.Length;

                            //Adding the teams
                            SDB.AddTeam(team[0], team[1]);

                            for (int i = 0; i < SDB.GetConstant(); i++)
                            {
                                if (SDB.myTeams[i] != null)
                                {
                                    if (SDB.myTeams[i].IsTeamName(team[0]))
                                    {
                                        SDB.myTeams[i].TeamLeaderName = team[2];
                                        SDB.myTeams[i].Amount = Convert.ToInt32(team[4]);
                                    }
                                }
                            }

                            //Adding the Sales People

                            SDB.AddSalesPerson(person[0], person[1], person[2], person[3], person[4], person[5], person[6], Convert.ToSingle(person[7]),Convert.ToInt32(person[9]));

                            for (int i = 0; i < SDB.GetConstant(); i++)
                            {
                                if (SDB.mySalesPeople[i] != null)
                                {
                                    if (SDB.mySalesPeople[i].StaffNumber == Convert.ToInt32(person[9]))
                                    {
                                        SDB.mySalesPeople[i].TeamName = person[8];
                                        SDB.AddTeamMember(SDB.mySalesPeople[i].FullName + ";" + SDB.mySalesPeople[i].StaffNumber, SDB.mySalesPeople[i].TeamName);
                                    }
                                }
                            }
                        }
                        else if (n == 1)
                        {
                            SDB.AddSalesPerson(person[0], person[1], person[2], person[3], person[4], person[5], person[6], Convert.ToSingle(person[7]), Convert.ToInt32(person[9]));

                            for (int i = 0; i < SDB.GetConstant(); i++)
                            {
                                if (SDB.mySalesPeople[i] != null)
                                {
                                    if (SDB.mySalesPeople[i].StaffNumber == Convert.ToInt32(person[9]))
                                    {
                                        SDB.mySalesPeople[i].TeamName = person[8];
                                        SDB.AddTeamMember(SDB.mySalesPeople[i].FullName + ";" + SDB.mySalesPeople[i].StaffNumber, SDB.mySalesPeople[i].TeamName);
                                    }
                                }
                            }
                        }
                    }

                    openFS.Close();
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*This click event is used to save a salesperson record into a new file
         */
        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {

                SaveFileDialog saving = new SaveFileDialog();

                saving.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                saving.Title = "Save Personnel Data";
                saving.RestoreDirectory = true;
                String path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), @"..\..\Records");
                saving.InitialDirectory = path;

                saving.ShowDialog(this);

                if (saving.FileName != "")
                {

                    FileStream savingFS = new FileStream(saving.FileName, FileMode.OpenOrCreate);
                    StreamWriter savingSW = new StreamWriter(savingFS);

                    for (int i = 0; i < SDB.GetConstant(); i++)
                    {
                        if (SDB.mySalesPeople[i] != null || SDB.myTeams[i] != null)
                        {
                            if (SDB.mySalesPeople[i].HomeAddress == "")
                            {
                                SDB.mySalesPeople[i].HomeAddress = "null";
                            }
                            if (SDB.mySalesPeople[i].Email == "")
                            {
                                SDB.mySalesPeople[i].Email = "null";
                            }
                            if (SDB.mySalesPeople[i].Country == "")
                            {
                                SDB.mySalesPeople[i].Country = "null";
                            }
                            if (SDB.mySalesPeople[i].Region == "")
                            {
                                SDB.mySalesPeople[i].Region = "null";
                            }
                            if (SDB.mySalesPeople[i].teamName == "")
                            {
                                SDB.mySalesPeople[i].teamName = "null";
                            }

                            if (SDB.myTeams[i] != null)
                            {
                                if (SDB.myTeams[i].TeamLeaderName == "")
                                {
                                    SDB.myTeams[i].TeamLeaderName = "null";
                                }

                                savingSW.WriteLine(SDB.mySalesPeople[i].FullName + ";" + SDB.mySalesPeople[i].Birthday + ";" + SDB.mySalesPeople[i].Number + ";" + SDB.mySalesPeople[i].Email + ";" + SDB.mySalesPeople[i].HomeAddress + ";" + SDB.mySalesPeople[i].Country + ";" + SDB.mySalesPeople[i].Region + ";" + SDB.mySalesPeople[i].Sales + ";" + SDB.mySalesPeople[i].TeamName + ";" + SDB.mySalesPeople[i].StaffNumber + ">" + SDB.myTeams[i].TeamName + ":" + SDB.myTeams[i].TeamAreaName + ":" + SDB.myTeams[i].TeamLeaderName + ":" + SDB.myTeams[i].Members + ":" + SDB.myTeams[i].Amount);
                            }
                            else
                            {
                                savingSW.WriteLine(SDB.mySalesPeople[i].FullName + ";" + SDB.mySalesPeople[i].Birthday + ";" + SDB.mySalesPeople[i].Number + ";" + SDB.mySalesPeople[i].Email + ";" + SDB.mySalesPeople[i].HomeAddress + ";" + SDB.mySalesPeople[i].Country + ";" + SDB.mySalesPeople[i].Region + ";" + SDB.mySalesPeople[i].Sales + ";" + SDB.mySalesPeople[i].TeamName + ";" + SDB.mySalesPeople[i].StaffNumber + ">"); 
                            }
                        }
                    }
                    savingSW.Close();
                    savingFS.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*This click event is used to save details into a file that is already open
         */
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (File.Exists(openFile))
            {
                FileStream existing = new FileStream(openFile, FileMode.Truncate);

                StreamWriter existingSW = new StreamWriter(existing);

                for (int i = 0; i < SDB.totalSalesPeople; i++)
                {
                    if (SDB.mySalesPeople[i] != null || SDB.myTeams[i]!=null)
                    {
                        if (SDB.mySalesPeople[i] != null)
                        {
                            if (SDB.mySalesPeople[i].HomeAddress == "")
                            {
                                SDB.mySalesPeople[i].HomeAddress = "null";
                            }
                            if (SDB.mySalesPeople[i].Email == "")
                            {
                                SDB.mySalesPeople[i].Email = "null";
                            }
                            if (SDB.mySalesPeople[i].Country == "")
                            {
                                SDB.mySalesPeople[i].Country = "null";
                            }
                            if (SDB.mySalesPeople[i].Region == "")
                            {
                                SDB.mySalesPeople[i].Region = "null";
                            }
                            if (SDB.mySalesPeople[i].teamName == "")
                            {
                                SDB.mySalesPeople[i].teamName = "null";
                            }

                            if (SDB.myTeams[i] != null)
                            {
                                if (SDB.myTeams[i].TeamLeaderName == "")
                                {
                                    SDB.myTeams[i].TeamLeaderName = "null";
                                }

                                existingSW.WriteLine(SDB.mySalesPeople[i].FullName + ";" + SDB.mySalesPeople[i].Birthday + ";" + SDB.mySalesPeople[i].Number + ";" + SDB.mySalesPeople[i].Email + ";" + SDB.mySalesPeople[i].HomeAddress + ";" + SDB.mySalesPeople[i].Country + ";" + SDB.mySalesPeople[i].Region + ";" + SDB.mySalesPeople[i].Sales + ";" + SDB.mySalesPeople[i].TeamName + ";" + SDB.mySalesPeople[i].StaffNumber + ">" + SDB.myTeams[i].TeamName + ":" + SDB.myTeams[i].TeamAreaName + ":" + SDB.myTeams[i].TeamLeaderName + ":" + SDB.myTeams[i].Members + ":" + SDB.myTeams[i].Amount);
                            }
                            else
                            {
                                existingSW.WriteLine(SDB.mySalesPeople[i].FullName + ";" + SDB.mySalesPeople[i].Birthday + ";" + SDB.mySalesPeople[i].Number + ";" + SDB.mySalesPeople[i].Email + ";" + SDB.mySalesPeople[i].HomeAddress + ";" + SDB.mySalesPeople[i].Country + ";" + SDB.mySalesPeople[i].Region + ";" + SDB.mySalesPeople[i].Sales + ";" + SDB.mySalesPeople[i].TeamName + ";" + SDB.mySalesPeople[i].StaffNumber + ">");
                            }
                        }
                    }
                }

                existingSW.Close();
                existing.Close();
            }
        }

        /*This click event opens the help window which shows the help information for 
         * using the system
         */
        private void helpToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FileStream help = new FileStream(@"..\..\Help\Help.txt",FileMode.Open);

            StreamReader helpSR = new StreamReader(help);

             Help helpWindow = new Help();

             helpWindow.helpTextBox.Text = helpSR.ReadToEnd();

             helpSR.Close();

             helpWindow.Show(this);

             
        }

        /*This click event shows the relevant information about the system
         */
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileStream about = new FileStream(@"..\..\Help\About.txt", FileMode.Open);

            StreamReader aboutSR = new StreamReader(about);

            About aboutWindow = new About();

            aboutWindow.aboutTextBox.Text = aboutSR.ReadToEnd();

            aboutSR.Close();

            aboutWindow.Show(this);

            
        }

        
        /*This event opens up the Search form
         */
        private void searchSalesPersonButton_Click(object sender, EventArgs e)
        {
            Search search = new Search(this);

            search.ShowDialog();
        }

        /*This event is used to print a record of a selected sales person
         */
        private void selectedRecordToolStripMenuItem_Click(object sender, EventArgs e)
        {

            int staffnumber;
            string name;
            PrintingWindow printWindow = new PrintingWindow(this);

            if (printWindow.ShowDialog() == DialogResult.OK)
            {
                if (printWindow.printCheckedListBox.CheckedItems.Count > 0)
                {
                    name = printWindow.printCheckedListBox.CheckedItems[0].ToString();

                    string[] names = name.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                    staffnumber = Convert.ToInt32(names[1]);

                    SDB.PageToPrintSelectedPerson(staffnumber);
                }
            }

            PrintDocument document = new PrintDocument();

            document.PrintPage += new PrintPageEventHandler(print.PrintTextHandler);

            PrintDialog printDG = new PrintDialog();

            if (printDG.ShowDialog() == DialogResult.OK)
            {
                document.Print();
            }

        }

        /*This click event is used to print all the sales people records
         */
        private void allRecordsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SDB.PageToPrintAllPeople();

            PrintDocument document = new PrintDocument();

            document.PrintPage += new PrintPageEventHandler(print.PrintTextHandler);

            PrintDialog printDG = new PrintDialog();

            if (printDG.ShowDialog() == DialogResult.OK)
            {
                document.Print();
            }


        }

        /*This click event is used to print a selected record of a team
         */
        private void selectedTeamToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            string name;

            PrintingWindow printWindow = new PrintingWindow(this);

            printWindow.printTextBox.Visible = false;
            printWindow.label1.Visible = false;
            printWindow.label3.Visible = false;
            printWindow.searchPrintButton.Visible = false;

            SDB.ListTeams(printWindow.printCheckedListBox.Items);

            if (printWindow.ShowDialog() == DialogResult.OK)
            {
                if (printWindow.printCheckedListBox.CheckedItems.Count > 0)
                {
                    name = printWindow.printCheckedListBox.CheckedItems[0].ToString();

                    SDB.PageToPrintSelectedTeam(name);
                }
            }

            PrintDocument document = new PrintDocument();

            document.PrintPage += new PrintPageEventHandler(print.PrintTextHandler);

            PrintDialog printDG = new PrintDialog();

            if (printDG.ShowDialog() == DialogResult.OK)
            {
                document.Print();
            }
        }

        /*This event is used to print all the team records
         */
        private void allTeamsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SDB.PageToPrintAllTeams();

            PrintDocument document = new PrintDocument();

            document.PrintPage += new PrintPageEventHandler(print.PrintTextHandler);

            PrintDialog printDG = new PrintDialog();

            if (printDG.ShowDialog() == DialogResult.OK)
            {
                document.Print();
            }
        }


    }
}
