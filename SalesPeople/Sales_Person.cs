﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.Text.RegularExpressions;
using System.IO;
using System.Drawing.Printing;

namespace SalesPeople
{
    public partial class Sales_Person : Form
    {
        private readonly Form1 f1;
        public Sales_Person(Form1 form1)
        {
            InitializeComponent();
            f1=form1;
        }

        /*This click event closes this form
         */
        private void ClosePersonButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /*This click event is used to add a salesperson by filling in the relevant 
         *details into the textboxes
         */
        private void AddPersonButton_Click(object sender, EventArgs e)
        {
            try
            {
                float total;
                bool isValid = float.TryParse(totalTextBox.Text, NumberStyles.Currency,CultureInfo.GetCultureInfo("en-US"), out total);
                string emailAddress = @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";//checking the email address
                int staffNumber=0;

                if (NameTextBox.Text != "" && staffNumberTextBox.Text!="")
                {
                    if (EmailTextBox.Text != "")
                    {
                        if (!System.Text.RegularExpressions.Regex.IsMatch(EmailTextBox.Text, emailAddress))
                        {
                            throw new Exception("Enter the email address in this format username@domain.com");
                        }
                    }
                    if (staffNumberTextBox.Text != "")
                    {
                        if (!int.TryParse(staffNumberTextBox.Text,out staffNumber))
                        {
                            throw new Exception("Please enter a valid integer number as the staff number");
                        }
                    }
                    if (totalTextBox.Text != "")
                    {
                        if (isValid != true)
                        {
                            throw new Exception("Enter a valid amount");
                        }
                    }

                    if (!f1.SDB.IsStaffNumber(staffNumber))
                    {
                        if (!f1.SDB.AddSalesPerson(NameTextBox.Text, dateSalesPersonTimePicker.Text, NumberTextBox.Text, EmailTextBox.Text, AddressTextBox.Text, AreaComboBox.Text, regionTextBox.Text, total, staffNumber))
                        {
                            MessageBox.Show("Cannot add this salesperson!");
                        }
                        else
                        {
                            errorProvider1.Clear();
                            MessageBox.Show("Sales Person added", "Addded Salesperson", MessageBoxButtons.OK);
                            NameTextBox.Text = NumberTextBox.Text = EmailTextBox.Text = AddressTextBox.Text = AreaComboBox.Text = totalTextBox.Text = regionTextBox.Text = staffNumberTextBox.Text = "";
                        }
                    }
                    else
                    {
                        throw new Exception("Another Sales person has the Staff Number you have entered");
                    }
                }
                else
                {
                    errorProvider1.SetError(NameTextBox, "You have to enter the name of a salesperson");
                    errorProvider1.SetError(staffNumberTextBox, "Please enter a staff number");
                }
                
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        /*This sets out what happens when the form is closing
         */
        private void Sales_Person_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult addPerson = MessageBox.Show("Do you want to add another Sales Person", "Add Sales Person", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (addPerson == DialogResult.Yes)
            {
                e.Cancel = true;
            }
        }

        private void AreaComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] Asia = {"Brunei","Cambodia","China","India","Indonesia","Japan","Kazakhstan","North Korea","South Korea","Kyrgyzstan","Laos","Malaysia",

                                "Maldives","Mongolia","Myanmar","Nepal","Philippines","Singapore","Sri Lanka","Taiwan","Tajikistan","Thailand","Turkmenistan",

                                "Uzbekistan","Vietnam","Bangladesh",

                                "Bhutan"};


            string[] MiddleEastNorthAfricaandGreaterArabia = { "Afghanistan","Algeria","Azerbaijan","Bahrain","Egypt","Iran","Iraq",

                                                                 "Israel","Jordan","Kuwait","Lebanon","Libya","Morocco","Oman","Pakistan",

                                                                 "Qatar","Saudi Arabia","Somalia","Syria","Tunisia","Turkey","United Arab Emirates",

                                                                 "Yemen"};

            string[] Europe = {"Albania","Andorra","Armenia","Austria","Belarus","Belgium","Bosnia and Herzegovina","Bulgaria","Croatia","Cyprus","Czech Republic",

                                  "Denmark","Estonia","Finland","France","Georgia","Germany","Greece","Hungary","Iceland","Ireland","Italy","Kosovo","Latvia",

                                  "Liechtenstein","Lithuania","Luxembourg","Macedonia","Malta","Moldova","Monaco","Montenegro","Netherlands","Norway","Poland",

                                  "Portugal","Romania","Russia","San Marino","Serbia","Slovakia","Slovenia","Spain","Sweden","Switzerland","Ukraine",

                                  "United Kingdom of Great Britain","Northern Ireland",

                                  "Vatican City" };

            string[] SubSaharanAfrica = {"Angola","Benin","Botswana","Burkina Faso","Burundi","Cameroon","Cape Verde","Central African Republic","Chad","Comoros",

                                            "Republic of the Congo","Democratic Republic of the Congo","Cote d'Ivoire","Djibouti","Equatorial Guinea","Eritrea",

                                            "Ethiopia","Gabon","The Gambia","Ghana","Guinea","Guinea-Bissau","Kenya","Lesotho","Liberia","Madagascar",

                                            "Malawi","Mali", "Mauritania","Mauritius","Mozambique","Namibia","Niger","Nigeria","Rwanda",

                                            "Sao Tome and Principe","Senegal","Seychelles","Sierra Leone","South Africa","South Sudan","Sudan","Swaziland","Tanzania",

                                            "Togo","Uganda","Zambia","Zimbabwe"};

            string[] NorthAmerica = { "Canada", "Greenland", "Mexico", "United States of America" };

            string[] CentralAmericaAndTheCaribbean = {"Antigua and Barbuda","The Bahamas","Barbados","Belize","Costa Rica","Cuba","Dominica","Dominican Republic",

                                                         "El Salvador","Grenada","Guatemala","Haiti","Honduras","Jamaica","Nicaragua","Panama","Saint Kitts and Nevis",

                                                         "Saint Lucia","Saint Vincent and the Grenadines",

                                                         "Trinidad and Tobago" };

            string[] SouthAmerica = { "Argentina", "Bolivia", "Brazil", "Chile", "Colombia", "Ecuador", "Guyana", "Paraguay", "Peru", "Suriname", "Uruguay", "Venezuela" };

            string[] AustraliaAndOceania = {"Australia","East Timor","Fiji","Kiribati","Marshall Islands","Federated States of Micronesia","Nauru","New Zealand",

                                               "Palau","Papua New Guinea","Samoa","Solomon Islands","Tonga","Tuvalu",

                                               "Vanuatu"};

            if (Asia.Contains(AreaComboBox.SelectedItem))
            {
                regionTextBox.Text = "Asia";
            }
            if (MiddleEastNorthAfricaandGreaterArabia.Contains(AreaComboBox.SelectedItem))
            {
                regionTextBox.Text = "MiddleEastNorthAfricaandGreaterArabia";
            }
            if (SubSaharanAfrica.Contains(AreaComboBox.SelectedItem))
            {
                regionTextBox.Text = "SubSaharanAfrica";
            }
            if (Europe.Contains(AreaComboBox.SelectedItem))
            {
                regionTextBox.Text = "Europe";
            }
            if (CentralAmericaAndTheCaribbean.Contains(AreaComboBox.SelectedItem))
            {
                regionTextBox.Text = "CentralAmericaAndTheCaribbean";
            }
            if (NorthAmerica.Contains(AreaComboBox.SelectedItem))
            {
                regionTextBox.Text = "NorthAmerica";
            }
            if (SouthAmerica.Contains(AreaComboBox.SelectedItem))
            {
                regionTextBox.Text = "SouthAmerica";
            }
            if (AustraliaAndOceania.Contains(AreaComboBox.SelectedItem))
            {
                regionTextBox.Text = "AustraliaAndOceania";
            }
        }

        private void AreaComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        /*This click event is used to save a salesperson record into a new file
         */
        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {

                SaveFileDialog saving = new SaveFileDialog();

                saving.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                saving.Title = "Save Personnel Data";
                saving.RestoreDirectory = true;
                String path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), @"..\..\Records");
                saving.InitialDirectory = path;

                saving.ShowDialog(this);

                if (saving.FileName != "")
                {

                    FileStream savingFS = new FileStream(saving.FileName, FileMode.OpenOrCreate);
                    StreamWriter savingSW = new StreamWriter(savingFS);

                    for (int i = 0; i < f1.SDB.GetConstant(); i++)
                    {
                        if (f1.SDB.mySalesPeople[i] != null || f1.SDB.myTeams[i] != null)
                        {
                            if (f1.SDB.mySalesPeople[i].HomeAddress == "")
                            {
                                f1.SDB.mySalesPeople[i].HomeAddress = "null";
                            }
                            if (f1.SDB.mySalesPeople[i].Email == "")
                            {
                                f1.SDB.mySalesPeople[i].Email = "null";
                            }
                            if (f1.SDB.mySalesPeople[i].Country == "")
                            {
                                f1.SDB.mySalesPeople[i].Country = "null";
                            }
                            if (f1.SDB.mySalesPeople[i].Region == "")
                            {
                                f1.SDB.mySalesPeople[i].Region = "null";
                            }
                            if (f1.SDB.mySalesPeople[i].teamName == "")
                            {
                                f1.SDB.mySalesPeople[i].teamName = "null";
                            }

                            if (f1.SDB.myTeams[i] != null)
                            {
                                if (f1.SDB.myTeams[i].TeamLeaderName == "")
                                {
                                    f1.SDB.myTeams[i].TeamLeaderName = "null";
                                }

                                savingSW.WriteLine(f1.SDB.mySalesPeople[i].FullName + ";" + f1.SDB.mySalesPeople[i].Birthday + ";" + f1.SDB.mySalesPeople[i].Number + ";" + f1.SDB.mySalesPeople[i].Email + ";" + f1.SDB.mySalesPeople[i].HomeAddress + ";" + f1.SDB.mySalesPeople[i].Country + ";" + f1.SDB.mySalesPeople[i].Region + ";" + f1.SDB.mySalesPeople[i].Sales + ";" + f1.SDB.mySalesPeople[i].TeamName + ";" + f1.SDB.mySalesPeople[i].StaffNumber + ">" + f1.SDB.myTeams[i].TeamName + ":" + f1.SDB.myTeams[i].TeamAreaName + ":" + f1.SDB.myTeams[i].TeamLeaderName + ":" + f1.SDB.myTeams[i].Members + ":" + f1.SDB.myTeams[i].Amount);
                            }
                            else
                            {
                                savingSW.WriteLine(f1.SDB.mySalesPeople[i].FullName + ";" + f1.SDB.mySalesPeople[i].Birthday + ";" + f1.SDB.mySalesPeople[i].Number + ";" + f1.SDB.mySalesPeople[i].Email + ";" + f1.SDB.mySalesPeople[i].HomeAddress + ";" + f1.SDB.mySalesPeople[i].Country + ";" + f1.SDB.mySalesPeople[i].Region + ";" + f1.SDB.mySalesPeople[i].Sales + ";" + f1.SDB.mySalesPeople[i].TeamName + ";" + f1.SDB.mySalesPeople[i].StaffNumber + ">");
                            }
                        }
                    }
                    savingSW.Close();
                    savingFS.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*This click event invokes the openfiledialog 
        */
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog opening = new OpenFileDialog();

                String path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), @"..\..\Records");

                opening.InitialDirectory = path;
                opening.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                opening.Title = "Open Saved Personnel Data";

                opening.ShowDialog();


                if (opening.FileName != "")
                {

                    List<string> personnelList = new List<string>();


                    for (int i = 0; i < f1.SDB.GetConstant(); i++)
                    {
                        if (f1.SDB.mySalesPeople[i] != null)
                        {
                            f1.SDB.mySalesPeople[i] = null;
                        }

                        if (f1.SDB.myTeams[i] != null)
                        {
                            f1.SDB.myTeams[i] = null;
                        }
                    }

                    FileStream openFS = new FileStream(opening.FileName, FileMode.Open);
                    f1.openFile = opening.FileName;

                    StreamReader openingSR = new StreamReader(openFS);

                    string lines;

                    while ((lines = openingSR.ReadLine()) != null)
                    {
                        personnelList.Add(lines);
                    }


                    foreach (string s in personnelList)
                    {
                        string[] info = s.Split(new string[] { ">" }, StringSplitOptions.RemoveEmptyEntries);

                        int n = info.Length;


                        string[] person = info[0].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

                        int m = person.Length;



                        if (n == 2)
                        {
                            string[] team = info[1].Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);

                            int k = team.Length;

                            //Adding the teams
                            f1.SDB.AddTeam(team[0], team[1]);

                            for (int i = 0; i < f1.SDB.GetConstant(); i++)
                            {
                                if (f1.SDB.myTeams[i] != null)
                                {
                                    if (f1.SDB.myTeams[i].IsTeamName(team[0]))
                                    {
                                        f1.SDB.myTeams[i].TeamLeaderName = team[2];
                                        f1.SDB.myTeams[i].Amount = Convert.ToInt32(team[4]);
                                    }
                                }
                            }

                            //Adding the Sales People

                            f1.SDB.AddSalesPerson(person[0], person[1], person[2], person[3], person[4], person[5], person[6], Convert.ToSingle(person[7]), Convert.ToInt32(person[9]));

                            for (int i = 0; i < f1.SDB.GetConstant(); i++)
                            {
                                if (f1.SDB.mySalesPeople[i] != null)
                                {
                                    if (f1.SDB.mySalesPeople[i].StaffNumber == Convert.ToInt32(person[9]))
                                    {
                                        f1.SDB.mySalesPeople[i].TeamName = person[8];
                                        f1.SDB.AddTeamMember(f1.SDB.mySalesPeople[i].FullName + ";" + f1.SDB.mySalesPeople[i].StaffNumber, f1.SDB.mySalesPeople[i].TeamName);
                                    }
                                }
                            }
                        }
                        else if (n == 1)
                        {
                            f1.SDB.AddSalesPerson(person[0], person[1], person[2], person[3], person[4], person[5], person[6], Convert.ToSingle(person[7]), Convert.ToInt32(person[9]));

                            for (int i = 0; i < f1.SDB.GetConstant(); i++)
                            {
                                if (f1.SDB.mySalesPeople[i] != null)
                                {
                                    if (f1.SDB.mySalesPeople[i].StaffNumber == Convert.ToInt32(person[9]))
                                    {
                                        f1.SDB.mySalesPeople[i].TeamName = person[8];
                                        f1.SDB.AddTeamMember(f1.SDB.mySalesPeople[i].FullName + ";" + f1.SDB.mySalesPeople[i].StaffNumber, f1.SDB.mySalesPeople[i].TeamName);
                                    }
                                }
                            }
                        }
                    }

                    openFS.Close();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*This click event is used to save details into a file that is already open
         */
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (File.Exists(f1.openFile))
            {
                FileStream existing = new FileStream(f1.openFile, FileMode.Truncate);

                StreamWriter existingSW = new StreamWriter(existing);

                for (int i = 0; i < f1.SDB.totalSalesPeople; i++)
                {
                    if (f1.SDB.mySalesPeople[i] != null || f1.SDB.myTeams[i] != null)
                    {
                        if (f1.SDB.mySalesPeople[i] != null)
                        {
                            if (f1.SDB.mySalesPeople[i].HomeAddress == "")
                            {
                                f1.SDB.mySalesPeople[i].HomeAddress = "null";
                            }
                            if (f1.SDB.mySalesPeople[i].Email == "")
                            {
                                f1.SDB.mySalesPeople[i].Email = "null";
                            }
                            if (f1.SDB.mySalesPeople[i].Country == "")
                            {
                                f1.SDB.mySalesPeople[i].Country = "null";
                            }
                            if (f1.SDB.mySalesPeople[i].Region == "")
                            {
                                f1.SDB.mySalesPeople[i].Region = "null";
                            }
                            if (f1.SDB.mySalesPeople[i].teamName == "")
                            {
                                f1.SDB.mySalesPeople[i].teamName = "null";
                            }

                            if (f1.SDB.myTeams[i] != null)
                            {
                                if (f1.SDB.myTeams[i].TeamLeaderName == "")
                                {
                                    f1.SDB.myTeams[i].TeamLeaderName = "null";
                                }

                                existingSW.WriteLine(f1.SDB.mySalesPeople[i].FullName + ";" + f1.SDB.mySalesPeople[i].Birthday + ";" + f1.SDB.mySalesPeople[i].Number + ";" + f1.SDB.mySalesPeople[i].Email + ";" + f1.SDB.mySalesPeople[i].HomeAddress + ";" + f1.SDB.mySalesPeople[i].Country + ";" + f1.SDB.mySalesPeople[i].Region + ";" + f1.SDB.mySalesPeople[i].Sales + ";" + f1.SDB.mySalesPeople[i].TeamName + ";" + f1.SDB.mySalesPeople[i].StaffNumber + ">" + f1.SDB.myTeams[i].TeamName + ":" + f1.SDB.myTeams[i].TeamAreaName + ":" + f1.SDB.myTeams[i].TeamLeaderName + ":" + f1.SDB.myTeams[i].Members + ":" + f1.SDB.myTeams[i].Amount);
                            }
                            else
                            {
                                existingSW.WriteLine(f1.SDB.mySalesPeople[i].FullName + ";" + f1.SDB.mySalesPeople[i].Birthday + ";" + f1.SDB.mySalesPeople[i].Number + ";" + f1.SDB.mySalesPeople[i].Email + ";" + f1.SDB.mySalesPeople[i].HomeAddress + ";" + f1.SDB.mySalesPeople[i].Country + ";" + f1.SDB.mySalesPeople[i].Region + ";" + f1.SDB.mySalesPeople[i].Sales + ";" + f1.SDB.mySalesPeople[i].TeamName + ";" + f1.SDB.mySalesPeople[i].StaffNumber + ">");
                            }
                        }
                    }
                }

                existingSW.Close();
                existing.Close();
            }
        }

        /*This event is used to print a record of a selected sales person
         */
        private void selectedSalespersonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int staffnumber;
            string name;
            PrintingWindow printWindow = new PrintingWindow(f1);

            if (printWindow.ShowDialog() == DialogResult.OK)
            {
                if (printWindow.printCheckedListBox.CheckedItems.Count > 0)
                {
                    name = printWindow.printCheckedListBox.CheckedItems[0].ToString();

                    string[] names = name.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                    staffnumber = Convert.ToInt32(names[1]);

                    f1.SDB.PageToPrintSelectedPerson(staffnumber);
                }
            }

            PrintDocument document = new PrintDocument();

            document.PrintPage += new PrintPageEventHandler(f1.print.PrintTextHandler);

            PrintDialog printDG = new PrintDialog();

            if (printDG.ShowDialog() == DialogResult.OK)
            {
                document.Print();
            }
        }

        /*This click event is used to print all the sales people records
         */
        private void allSalespeopleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            f1.SDB.PageToPrintAllPeople();

            PrintDocument document = new PrintDocument();

            document.PrintPage += new PrintPageEventHandler(f1.print.PrintTextHandler);

            PrintDialog printDG = new PrintDialog();

            if (printDG.ShowDialog() == DialogResult.OK)
            {
                document.Print();
            }
        }

        /*This click event is used to print a selected record of a team
         */
        private void selectedTeamToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string name;

            PrintingWindow printWindow = new PrintingWindow(f1);

            printWindow.printTextBox.Visible = false;
            printWindow.label1.Visible = false;
            printWindow.label3.Visible = false;
            printWindow.searchPrintButton.Visible = false;

            f1.SDB.ListTeams(printWindow.printCheckedListBox.Items);

            if (printWindow.ShowDialog() == DialogResult.OK)
            {
                if (printWindow.printCheckedListBox.CheckedItems.Count > 0)
                {
                    name = printWindow.printCheckedListBox.CheckedItems[0].ToString();

                    f1.SDB.PageToPrintSelectedTeam(name);
                }
            }

            PrintDocument document = new PrintDocument();

            document.PrintPage += new PrintPageEventHandler(f1.print.PrintTextHandler);

            PrintDialog printDG = new PrintDialog();

            if (printDG.ShowDialog() == DialogResult.OK)
            {
                document.Print();
            }
        }

        /*This event is used to print all the team records
         */
        private void allTeamsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            f1.SDB.PageToPrintAllTeams();

            PrintDocument document = new PrintDocument();

            document.PrintPage += new PrintPageEventHandler(f1.print.PrintTextHandler);

            PrintDialog printDG = new PrintDialog();

            if (printDG.ShowDialog() == DialogResult.OK)
            {
                document.Print();
            }
        }

        /*This click event opens the help window which shows the help information for 
         * using the system
         */
        private void helpToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FileStream help = new FileStream(@"..\..\Help\Help.txt", FileMode.Open);

            StreamReader helpSR = new StreamReader(help);

            Help helpWindow = new Help();

            helpWindow.helpTextBox.Text = helpSR.ReadToEnd();

            helpSR.Close();

            helpWindow.Show(this);
        }

        /*This click event shows the relevant information about the system
         */
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileStream about = new FileStream(@"..\..\Help\About.txt", FileMode.Open);

            StreamReader aboutSR = new StreamReader(about);

            About aboutWindow = new About();

            aboutWindow.aboutTextBox.Text = aboutSR.ReadToEnd();

            aboutSR.Close();

            aboutWindow.Show(this);
        }
    }
}
