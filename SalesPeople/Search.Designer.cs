﻿namespace SalesPeople
{
    partial class Search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.searchPersonButton = new System.Windows.Forms.Button();
            this.searchNameTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.salesOverTextBox = new System.Windows.Forms.TextBox();
            this.searchSalesOverValueRadioButton = new System.Windows.Forms.RadioButton();
            this.searchAreaRadioButton = new System.Windows.Forms.RadioButton();
            this.salesOver1MRadioButton = new System.Windows.Forms.RadioButton();
            this.searchAllRadioButton = new System.Windows.Forms.RadioButton();
            this.searchAreaComboBox = new System.Windows.Forms.ComboBox();
            this.searchListBox = new System.Windows.Forms.ListBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // searchPersonButton
            // 
            this.searchPersonButton.Location = new System.Drawing.Point(274, 10);
            this.searchPersonButton.Name = "searchPersonButton";
            this.searchPersonButton.Size = new System.Drawing.Size(131, 23);
            this.searchPersonButton.TabIndex = 0;
            this.searchPersonButton.Text = "Search Sales Person";
            this.searchPersonButton.UseVisualStyleBackColor = true;
            this.searchPersonButton.Click += new System.EventHandler(this.searchPersonButton_Click);
            // 
            // searchNameTextBox
            // 
            this.searchNameTextBox.Location = new System.Drawing.Point(59, 13);
            this.searchNameTextBox.Name = "searchNameTextBox";
            this.searchNameTextBox.Size = new System.Drawing.Size(209, 20);
            this.searchNameTextBox.TabIndex = 1;
            this.toolTip1.SetToolTip(this.searchNameTextBox, "Fill in a name");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Search";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.salesOverTextBox);
            this.groupBox1.Controls.Add(this.searchSalesOverValueRadioButton);
            this.groupBox1.Controls.Add(this.searchAreaRadioButton);
            this.groupBox1.Controls.Add(this.salesOver1MRadioButton);
            this.groupBox1.Controls.Add(this.searchAllRadioButton);
            this.groupBox1.Controls.Add(this.searchAreaComboBox);
            this.groupBox1.Location = new System.Drawing.Point(15, 57);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(396, 100);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Choose the search category";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(213, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(178, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Press enter after entering an amount";
            // 
            // salesOverTextBox
            // 
            this.salesOverTextBox.Location = new System.Drawing.Point(309, 76);
            this.salesOverTextBox.Name = "salesOverTextBox";
            this.salesOverTextBox.Size = new System.Drawing.Size(81, 20);
            this.salesOverTextBox.TabIndex = 9;
            this.toolTip1.SetToolTip(this.salesOverTextBox, "Fill in an amount");
            this.salesOverTextBox.Visible = false;
            this.salesOverTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.salesOverTextBox_KeyPress);
            // 
            // searchSalesOverValueRadioButton
            // 
            this.searchSalesOverValueRadioButton.AutoSize = true;
            this.searchSalesOverValueRadioButton.Location = new System.Drawing.Point(213, 77);
            this.searchSalesOverValueRadioButton.Name = "searchSalesOverValueRadioButton";
            this.searchSalesOverValueRadioButton.Size = new System.Drawing.Size(80, 17);
            this.searchSalesOverValueRadioButton.TabIndex = 8;
            this.searchSalesOverValueRadioButton.TabStop = true;
            this.searchSalesOverValueRadioButton.Text = "Sales Over:";
            this.searchSalesOverValueRadioButton.UseVisualStyleBackColor = true;
            this.searchSalesOverValueRadioButton.CheckedChanged += new System.EventHandler(this.searchSalesOverValueRadioButton_CheckedChanged);
            // 
            // searchAreaRadioButton
            // 
            this.searchAreaRadioButton.AutoSize = true;
            this.searchAreaRadioButton.Location = new System.Drawing.Point(7, 77);
            this.searchAreaRadioButton.Name = "searchAreaRadioButton";
            this.searchAreaRadioButton.Size = new System.Drawing.Size(59, 17);
            this.searchAreaRadioButton.TabIndex = 7;
            this.searchAreaRadioButton.TabStop = true;
            this.searchAreaRadioButton.Text = "Region";
            this.searchAreaRadioButton.UseVisualStyleBackColor = true;
            this.searchAreaRadioButton.CheckedChanged += new System.EventHandler(this.searchAreaRadioButton_CheckedChanged);
            // 
            // salesOver1MRadioButton
            // 
            this.salesOver1MRadioButton.AutoSize = true;
            this.salesOver1MRadioButton.Location = new System.Drawing.Point(213, 20);
            this.salesOver1MRadioButton.Name = "salesOver1MRadioButton";
            this.salesOver1MRadioButton.Size = new System.Drawing.Size(95, 17);
            this.salesOver1MRadioButton.TabIndex = 6;
            this.salesOver1MRadioButton.TabStop = true;
            this.salesOver1MRadioButton.Text = "Sales Over 1M";
            this.salesOver1MRadioButton.UseVisualStyleBackColor = true;
            this.salesOver1MRadioButton.CheckedChanged += new System.EventHandler(this.salesOver1MRadioButton_CheckedChanged);
            // 
            // searchAllRadioButton
            // 
            this.searchAllRadioButton.AutoSize = true;
            this.searchAllRadioButton.Location = new System.Drawing.Point(7, 20);
            this.searchAllRadioButton.Name = "searchAllRadioButton";
            this.searchAllRadioButton.Size = new System.Drawing.Size(101, 17);
            this.searchAllRadioButton.TabIndex = 5;
            this.searchAllRadioButton.TabStop = true;
            this.searchAllRadioButton.Text = "All Sales People";
            this.searchAllRadioButton.UseVisualStyleBackColor = true;
            this.searchAllRadioButton.CheckedChanged += new System.EventHandler(this.searchAllRadioButton_CheckedChanged);
            // 
            // searchAreaComboBox
            // 
            this.searchAreaComboBox.FormattingEnabled = true;
            this.searchAreaComboBox.Location = new System.Drawing.Point(72, 77);
            this.searchAreaComboBox.Name = "searchAreaComboBox";
            this.searchAreaComboBox.Size = new System.Drawing.Size(121, 21);
            this.searchAreaComboBox.TabIndex = 4;
            this.searchAreaComboBox.Visible = false;
            this.searchAreaComboBox.SelectedIndexChanged += new System.EventHandler(this.searchAreaComboBox_SelectedIndexChanged);
            this.searchAreaComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.searchAreaComboBox_KeyPress);
            // 
            // searchListBox
            // 
            this.searchListBox.FormattingEnabled = true;
            this.searchListBox.Location = new System.Drawing.Point(15, 206);
            this.searchListBox.Name = "searchListBox";
            this.searchListBox.Size = new System.Drawing.Size(396, 290);
            this.searchListBox.TabIndex = 4;
            this.searchListBox.DoubleClick += new System.EventHandler(this.searchListBox_DoubleClick);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 177);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(289, 26);
            this.label2.TabIndex = 16;
            this.label2.Text = "Double Click on a Sales person\'s name to view their details.\r\nThe name is followe" +
                "d by the staffnumber of the sales person";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(290, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "$";
            // 
            // Search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(423, 508);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.searchListBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.searchNameTextBox);
            this.Controls.Add(this.searchPersonButton);
            this.Name = "Search";
            this.Text = "Search";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button searchPersonButton;
        private System.Windows.Forms.TextBox searchNameTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox searchAreaComboBox;
        private System.Windows.Forms.ListBox searchListBox;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.RadioButton searchAreaRadioButton;
        private System.Windows.Forms.RadioButton salesOver1MRadioButton;
        private System.Windows.Forms.RadioButton searchAllRadioButton;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox salesOverTextBox;
        private System.Windows.Forms.RadioButton searchSalesOverValueRadioButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}