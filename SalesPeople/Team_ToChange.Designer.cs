﻿namespace SalesPeople
{
    partial class Team_ToChange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.okButton = new System.Windows.Forms.Button();
            this.cancleButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.teamToChangeComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(21, 120);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 0;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // cancleButton
            // 
            this.cancleButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancleButton.Location = new System.Drawing.Point(181, 120);
            this.cancleButton.Name = "cancleButton";
            this.cancleButton.Size = new System.Drawing.Size(75, 23);
            this.cancleButton.TabIndex = 1;
            this.cancleButton.Text = "Cancel";
            this.cancleButton.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(283, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Enter the name of the team you want to change the details";
            // 
            // teamToChangeComboBox
            // 
            this.teamToChangeComboBox.FormattingEnabled = true;
            this.teamToChangeComboBox.Location = new System.Drawing.Point(21, 76);
            this.teamToChangeComboBox.Name = "teamToChangeComboBox";
            this.teamToChangeComboBox.Size = new System.Drawing.Size(235, 21);
            this.teamToChangeComboBox.TabIndex = 4;
            this.teamToChangeComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.teamToChangeComboBox_KeyPress);
            // 
            // Team_ToChange
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancleButton;
            this.ClientSize = new System.Drawing.Size(284, 180);
            this.ControlBox = false;
            this.Controls.Add(this.teamToChangeComboBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cancleButton);
            this.Controls.Add(this.okButton);
            this.Name = "Team_ToChange";
            this.Text = "Team To Change";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancleButton;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.ComboBox teamToChangeComboBox;
    }
}