﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SalesPeople
{
    public partial class RemoveMembersWindow : Form
    {
        private readonly Form1 f1;
        public string teamName;
        public RemoveMembersWindow(Form1 form)
        {
            InitializeComponent();
            f1 = form;
        }

        /*This button click is used to remove a team member from a team
         */
        private void removeMemberButton_Click(object sender, EventArgs e)
        {
            try
            {
                string name;
                while (removeMembersCheckedListBox.CheckedItems.Count > 0)
                {
                    name = removeMembersCheckedListBox.CheckedItems[0].ToString();

                    f1.SDB.RemoveTeamMember(name, teamName);
                    removeMembersCheckedListBox.Items.Remove(name);
                    if (f1.SDB.IsTeamLeader(name,teamName))
                    {
                        f1.SDB.RemoveTeamLeader(teamName);
                        MessageBox.Show("You need a new team leader since the previous team leader has been removed as a member of this team");
                    }
                    
                }
                f1.SDB.TeamMembersList(removeMembersListBox.Items, teamName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
