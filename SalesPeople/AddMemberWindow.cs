﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SalesPeople
{
    public partial class AddMemberWindow : Form
    {
        private readonly Form1 f1;
        public string teamName;
        public AddMemberWindow(Form1 form)
        {
            InitializeComponent();
            f1 = form;
        }

        /*This butto click is usedd to add a salesperson as a member of a team
         */
        private void addMemberButton_Click(object sender, EventArgs e)
        {
            string name;
            while (membersCheckedListBox.CheckedItems.Count > 0)
            {
                name = membersCheckedListBox.CheckedItems[0].ToString();
                f1.SDB.AddTeamMember(name,teamName);
                membersCheckedListBox.Items.Remove(name);
            }
            f1.SDB.TeamMembersList(teamMembersListBox.Items, teamName);

        }
    }
}
