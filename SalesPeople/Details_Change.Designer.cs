﻿namespace SalesPeople
{
    partial class Details_Change
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.totalChangeTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.NumberChangeTextBox = new System.Windows.Forms.MaskedTextBox();
            this.CloseChangeButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.AreaChangeTextBox = new System.Windows.Forms.TextBox();
            this.AddressChangeTextBox = new System.Windows.Forms.TextBox();
            this.EmailChangeTextBox = new System.Windows.Forms.TextBox();
            this.NameChangeTextBox = new System.Windows.Forms.TextBox();
            this.updateNameButton = new System.Windows.Forms.Button();
            this.updatePhoneNumberButton = new System.Windows.Forms.Button();
            this.updateEmailButton = new System.Windows.Forms.Button();
            this.updateAddressButton = new System.Windows.Forms.Button();
            this.updateAreaButton = new System.Windows.Forms.Button();
            this.updateTotalSalesButton = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.birthdayChangeTextBox1 = new System.Windows.Forms.TextBox();
            this.updateBirthdayButton = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.RegionChangeTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.staffNumberChangeTextBox = new System.Windows.Forms.TextBox();
            this.updateStaffNumberButton = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salespersonRecordsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectedSalespersonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allSalespeopleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.teamRecordsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectedTeamToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allTeamsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label11 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // totalChangeTextBox
            // 
            this.totalChangeTextBox.Location = new System.Drawing.Point(89, 330);
            this.totalChangeTextBox.Name = "totalChangeTextBox";
            this.totalChangeTextBox.ReadOnly = true;
            this.totalChangeTextBox.Size = new System.Drawing.Size(275, 20);
            this.totalChangeTextBox.TabIndex = 28;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 337);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 27;
            this.label6.Text = "Total Sales";
            // 
            // NumberChangeTextBox
            // 
            this.NumberChangeTextBox.Location = new System.Drawing.Point(90, 162);
            this.NumberChangeTextBox.Mask = "(999) 000-0000";
            this.NumberChangeTextBox.Name = "NumberChangeTextBox";
            this.NumberChangeTextBox.ReadOnly = true;
            this.NumberChangeTextBox.Size = new System.Drawing.Size(275, 20);
            this.NumberChangeTextBox.TabIndex = 26;
            // 
            // CloseChangeButton
            // 
            this.CloseChangeButton.Location = new System.Drawing.Point(371, 367);
            this.CloseChangeButton.Name = "CloseChangeButton";
            this.CloseChangeButton.Size = new System.Drawing.Size(95, 45);
            this.CloseChangeButton.TabIndex = 25;
            this.CloseChangeButton.Text = "Close";
            this.CloseChangeButton.UseVisualStyleBackColor = true;
            this.CloseChangeButton.Click += new System.EventHandler(this.CloseChangeButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 268);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Area";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 236);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Address";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 203);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Email";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 165);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Phone Number";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Name";
            // 
            // AreaChangeTextBox
            // 
            this.AreaChangeTextBox.Location = new System.Drawing.Point(90, 261);
            this.AreaChangeTextBox.Name = "AreaChangeTextBox";
            this.AreaChangeTextBox.ReadOnly = true;
            this.AreaChangeTextBox.Size = new System.Drawing.Size(275, 20);
            this.AreaChangeTextBox.TabIndex = 18;
            // 
            // AddressChangeTextBox
            // 
            this.AddressChangeTextBox.Location = new System.Drawing.Point(91, 229);
            this.AddressChangeTextBox.Name = "AddressChangeTextBox";
            this.AddressChangeTextBox.ReadOnly = true;
            this.AddressChangeTextBox.Size = new System.Drawing.Size(274, 20);
            this.AddressChangeTextBox.TabIndex = 17;
            // 
            // EmailChangeTextBox
            // 
            this.EmailChangeTextBox.Location = new System.Drawing.Point(89, 196);
            this.EmailChangeTextBox.Name = "EmailChangeTextBox";
            this.EmailChangeTextBox.ReadOnly = true;
            this.EmailChangeTextBox.Size = new System.Drawing.Size(276, 20);
            this.EmailChangeTextBox.TabIndex = 16;
            // 
            // NameChangeTextBox
            // 
            this.NameChangeTextBox.Location = new System.Drawing.Point(91, 72);
            this.NameChangeTextBox.Name = "NameChangeTextBox";
            this.NameChangeTextBox.ReadOnly = true;
            this.NameChangeTextBox.Size = new System.Drawing.Size(276, 20);
            this.NameChangeTextBox.TabIndex = 15;
            // 
            // updateNameButton
            // 
            this.updateNameButton.Location = new System.Drawing.Point(373, 69);
            this.updateNameButton.Name = "updateNameButton";
            this.updateNameButton.Size = new System.Drawing.Size(95, 23);
            this.updateNameButton.TabIndex = 29;
            this.updateNameButton.Text = "Update";
            this.updateNameButton.UseVisualStyleBackColor = true;
            this.updateNameButton.Click += new System.EventHandler(this.updateNameButton_Click);
            // 
            // updatePhoneNumberButton
            // 
            this.updatePhoneNumberButton.Location = new System.Drawing.Point(371, 160);
            this.updatePhoneNumberButton.Name = "updatePhoneNumberButton";
            this.updatePhoneNumberButton.Size = new System.Drawing.Size(95, 23);
            this.updatePhoneNumberButton.TabIndex = 30;
            this.updatePhoneNumberButton.Text = "Update";
            this.updatePhoneNumberButton.UseVisualStyleBackColor = true;
            this.updatePhoneNumberButton.Click += new System.EventHandler(this.updatePhoneNumberButton_Click);
            // 
            // updateEmailButton
            // 
            this.updateEmailButton.Location = new System.Drawing.Point(371, 194);
            this.updateEmailButton.Name = "updateEmailButton";
            this.updateEmailButton.Size = new System.Drawing.Size(95, 23);
            this.updateEmailButton.TabIndex = 31;
            this.updateEmailButton.Text = "Update";
            this.updateEmailButton.UseVisualStyleBackColor = true;
            this.updateEmailButton.Click += new System.EventHandler(this.updateEmailButton_Click);
            // 
            // updateAddressButton
            // 
            this.updateAddressButton.Location = new System.Drawing.Point(371, 226);
            this.updateAddressButton.Name = "updateAddressButton";
            this.updateAddressButton.Size = new System.Drawing.Size(95, 23);
            this.updateAddressButton.TabIndex = 32;
            this.updateAddressButton.Text = "Update";
            this.updateAddressButton.UseVisualStyleBackColor = true;
            this.updateAddressButton.Click += new System.EventHandler(this.updateAddressButton_Click);
            // 
            // updateAreaButton
            // 
            this.updateAreaButton.Location = new System.Drawing.Point(371, 258);
            this.updateAreaButton.Name = "updateAreaButton";
            this.updateAreaButton.Size = new System.Drawing.Size(95, 23);
            this.updateAreaButton.TabIndex = 33;
            this.updateAreaButton.Text = "Update";
            this.updateAreaButton.UseVisualStyleBackColor = true;
            this.updateAreaButton.Click += new System.EventHandler(this.updateAreaButton_Click);
            // 
            // updateTotalSalesButton
            // 
            this.updateTotalSalesButton.Location = new System.Drawing.Point(371, 328);
            this.updateTotalSalesButton.Name = "updateTotalSalesButton";
            this.updateTotalSalesButton.Size = new System.Drawing.Size(95, 23);
            this.updateTotalSalesButton.TabIndex = 34;
            this.updateTotalSalesButton.Text = "Update";
            this.updateTotalSalesButton.UseVisualStyleBackColor = true;
            this.updateTotalSalesButton.Click += new System.EventHandler(this.updateTotalSalesButton_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 44);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(274, 13);
            this.label7.TabIndex = 35;
            this.label7.Text = "Click on an update button to update the respective fields";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 136);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 13);
            this.label8.TabIndex = 36;
            this.label8.Text = "Date of birth";
            // 
            // birthdayChangeTextBox1
            // 
            this.birthdayChangeTextBox1.Location = new System.Drawing.Point(91, 130);
            this.birthdayChangeTextBox1.Name = "birthdayChangeTextBox1";
            this.birthdayChangeTextBox1.ReadOnly = true;
            this.birthdayChangeTextBox1.Size = new System.Drawing.Size(274, 20);
            this.birthdayChangeTextBox1.TabIndex = 37;
            // 
            // updateBirthdayButton
            // 
            this.updateBirthdayButton.Location = new System.Drawing.Point(371, 131);
            this.updateBirthdayButton.Name = "updateBirthdayButton";
            this.updateBirthdayButton.Size = new System.Drawing.Size(95, 23);
            this.updateBirthdayButton.TabIndex = 38;
            this.updateBirthdayButton.Text = "Update";
            this.updateBirthdayButton.UseVisualStyleBackColor = true;
            this.updateBirthdayButton.Click += new System.EventHandler(this.updateBirthdayButton_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 298);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 13);
            this.label9.TabIndex = 39;
            this.label9.Text = "Region";
            // 
            // RegionChangeTextBox
            // 
            this.RegionChangeTextBox.Location = new System.Drawing.Point(89, 295);
            this.RegionChangeTextBox.Name = "RegionChangeTextBox";
            this.RegionChangeTextBox.ReadOnly = true;
            this.RegionChangeTextBox.Size = new System.Drawing.Size(275, 20);
            this.RegionChangeTextBox.TabIndex = 40;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 102);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 13);
            this.label10.TabIndex = 41;
            this.label10.Text = "Staff Number";
            // 
            // staffNumberChangeTextBox
            // 
            this.staffNumberChangeTextBox.Location = new System.Drawing.Point(91, 102);
            this.staffNumberChangeTextBox.Name = "staffNumberChangeTextBox";
            this.staffNumberChangeTextBox.ReadOnly = true;
            this.staffNumberChangeTextBox.Size = new System.Drawing.Size(276, 20);
            this.staffNumberChangeTextBox.TabIndex = 42;
            // 
            // updateStaffNumberButton
            // 
            this.updateStaffNumberButton.Location = new System.Drawing.Point(373, 98);
            this.updateStaffNumberButton.Name = "updateStaffNumberButton";
            this.updateStaffNumberButton.Size = new System.Drawing.Size(93, 23);
            this.updateStaffNumberButton.TabIndex = 43;
            this.updateStaffNumberButton.Text = "Update";
            this.updateStaffNumberButton.UseVisualStyleBackColor = true;
            this.updateStaffNumberButton.Click += new System.EventHandler(this.updateStaffNumberButton_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(486, 24);
            this.menuStrip1.TabIndex = 44;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveAsToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.printToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.saveAsToolStripMenuItem.Text = "Save As";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salespersonRecordsToolStripMenuItem,
            this.teamRecordsToolStripMenuItem});
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.printToolStripMenuItem.Text = "Print";
            // 
            // salespersonRecordsToolStripMenuItem
            // 
            this.salespersonRecordsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectedSalespersonToolStripMenuItem,
            this.allSalespeopleToolStripMenuItem});
            this.salespersonRecordsToolStripMenuItem.Name = "salespersonRecordsToolStripMenuItem";
            this.salespersonRecordsToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.salespersonRecordsToolStripMenuItem.Text = "Salesperson Records";
            // 
            // selectedSalespersonToolStripMenuItem
            // 
            this.selectedSalespersonToolStripMenuItem.Name = "selectedSalespersonToolStripMenuItem";
            this.selectedSalespersonToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.selectedSalespersonToolStripMenuItem.Text = "Selected Salesperson";
            this.selectedSalespersonToolStripMenuItem.Click += new System.EventHandler(this.selectedSalespersonToolStripMenuItem_Click);
            // 
            // allSalespeopleToolStripMenuItem
            // 
            this.allSalespeopleToolStripMenuItem.Name = "allSalespeopleToolStripMenuItem";
            this.allSalespeopleToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.allSalespeopleToolStripMenuItem.Text = "All Salespeople";
            this.allSalespeopleToolStripMenuItem.Click += new System.EventHandler(this.allSalespeopleToolStripMenuItem_Click);
            // 
            // teamRecordsToolStripMenuItem
            // 
            this.teamRecordsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectedTeamToolStripMenuItem,
            this.allTeamsToolStripMenuItem});
            this.teamRecordsToolStripMenuItem.Name = "teamRecordsToolStripMenuItem";
            this.teamRecordsToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.teamRecordsToolStripMenuItem.Text = "Team Records";
            // 
            // selectedTeamToolStripMenuItem
            // 
            this.selectedTeamToolStripMenuItem.Name = "selectedTeamToolStripMenuItem";
            this.selectedTeamToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.selectedTeamToolStripMenuItem.Text = "Selected Team";
            this.selectedTeamToolStripMenuItem.Click += new System.EventHandler(this.selectedTeamToolStripMenuItem_Click);
            // 
            // allTeamsToolStripMenuItem
            // 
            this.allTeamsToolStripMenuItem.Name = "allTeamsToolStripMenuItem";
            this.allTeamsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.allTeamsToolStripMenuItem.Text = "All Teams";
            this.allTeamsToolStripMenuItem.Click += new System.EventHandler(this.allTeamsToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem1,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // helpToolStripMenuItem1
            // 
            this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            this.helpToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.helpToolStripMenuItem1.Text = "Help";
            this.helpToolStripMenuItem1.Click += new System.EventHandler(this.helpToolStripMenuItem1_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(72, 333);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(13, 13);
            this.label11.TabIndex = 45;
            this.label11.Text = "$";
            // 
            // Details_Change
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 448);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.updateStaffNumberButton);
            this.Controls.Add(this.staffNumberChangeTextBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.RegionChangeTextBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.updateBirthdayButton);
            this.Controls.Add(this.birthdayChangeTextBox1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.updateTotalSalesButton);
            this.Controls.Add(this.updateAreaButton);
            this.Controls.Add(this.updateAddressButton);
            this.Controls.Add(this.updateEmailButton);
            this.Controls.Add(this.updatePhoneNumberButton);
            this.Controls.Add(this.updateNameButton);
            this.Controls.Add(this.totalChangeTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.NumberChangeTextBox);
            this.Controls.Add(this.CloseChangeButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AreaChangeTextBox);
            this.Controls.Add(this.AddressChangeTextBox);
            this.Controls.Add(this.EmailChangeTextBox);
            this.Controls.Add(this.NameChangeTextBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Details_Change";
            this.Text = "Sales Personnel Record";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Details_Change_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button CloseChangeButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button updateTotalSalesButton;
        private System.Windows.Forms.Button updateAreaButton;
        private System.Windows.Forms.Button updateAddressButton;
        private System.Windows.Forms.Button updateEmailButton;
        private System.Windows.Forms.Button updatePhoneNumberButton;
        private System.Windows.Forms.Button updateNameButton;
        public System.Windows.Forms.TextBox totalChangeTextBox;
        public System.Windows.Forms.MaskedTextBox NumberChangeTextBox;
        public System.Windows.Forms.TextBox AreaChangeTextBox;
        public System.Windows.Forms.TextBox AddressChangeTextBox;
        public System.Windows.Forms.TextBox EmailChangeTextBox;
        public System.Windows.Forms.TextBox NameChangeTextBox;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox birthdayChangeTextBox1;
        private System.Windows.Forms.Button updateBirthdayButton;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox RegionChangeTextBox;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox staffNumberChangeTextBox;
        private System.Windows.Forms.Button updateStaffNumberButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salespersonRecordsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectedSalespersonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allSalespeopleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem teamRecordsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectedTeamToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allTeamsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Label label11;
    }
}