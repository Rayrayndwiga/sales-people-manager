﻿namespace SalesPeople
{
    partial class AddMemberWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.membersCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.teamMembersListBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.teamLabelName = new System.Windows.Forms.Label();
            this.addMemberButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // membersCheckedListBox
            // 
            this.membersCheckedListBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.membersCheckedListBox.FormattingEnabled = true;
            this.membersCheckedListBox.Location = new System.Drawing.Point(12, 125);
            this.membersCheckedListBox.Name = "membersCheckedListBox";
            this.membersCheckedListBox.Size = new System.Drawing.Size(312, 199);
            this.membersCheckedListBox.TabIndex = 0;
            // 
            // teamMembersListBox
            // 
            this.teamMembersListBox.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.teamMembersListBox.FormattingEnabled = true;
            this.teamMembersListBox.Location = new System.Drawing.Point(354, 125);
            this.teamMembersListBox.Name = "teamMembersListBox";
            this.teamMembersListBox.Size = new System.Drawing.Size(276, 199);
            this.teamMembersListBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(312, 26);
            this.label1.TabIndex = 2;
            this.label1.Text = "Check the name of the sales person you want to add to the team\r\nand then click on" +
                " the add button\r\n";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(351, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "List of members in this team";
            // 
            // teamLabelName
            // 
            this.teamLabelName.AutoSize = true;
            this.teamLabelName.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teamLabelName.Location = new System.Drawing.Point(12, 9);
            this.teamLabelName.Name = "teamLabelName";
            this.teamLabelName.Size = new System.Drawing.Size(78, 31);
            this.teamLabelName.TabIndex = 5;
            this.teamLabelName.Text = "label4";
            // 
            // addMemberButton
            // 
            this.addMemberButton.Location = new System.Drawing.Point(12, 327);
            this.addMemberButton.Name = "addMemberButton";
            this.addMemberButton.Size = new System.Drawing.Size(75, 23);
            this.addMemberButton.TabIndex = 6;
            this.addMemberButton.Text = "Add";
            this.addMemberButton.UseVisualStyleBackColor = true;
            this.addMemberButton.Click += new System.EventHandler(this.addMemberButton_Click);
            // 
            // AddMemberWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(642, 355);
            this.Controls.Add(this.addMemberButton);
            this.Controls.Add(this.teamLabelName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.teamMembersListBox);
            this.Controls.Add(this.membersCheckedListBox);
            this.Name = "AddMemberWindow";
            this.Text = "AddMemberWindow";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.CheckedListBox membersCheckedListBox;
        public System.Windows.Forms.Label teamLabelName;
        private System.Windows.Forms.Button addMemberButton;
        public System.Windows.Forms.ListBox teamMembersListBox;
    }
}