﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SalesPeople
{
    public partial class AddTeamWindow : Form
    {
        private readonly Form1 f1;
        public AddTeamWindow(Form1 form)
        {
            InitializeComponent();
            f1 = form;
        }

        /* This click event is used to add a new team by entering 
         * details in the form
         */
        private void addTeamButton_Click(object sender, EventArgs e)
        {
            try
            {
                errorProviderAddTeam.Clear();
                if (teamNameTexBox.Text != "")
                {
                    if (!f1.SDB.AddTeam(teamNameTexBox.Text, operationAreaComboBox.Text))
                    {
                        throw new Exception("Cannot Add this team!");
                    }
                    else
                    {
                        MessageBox.Show("Team Added", "Team Add", MessageBoxButtons.OK);
                        teamNameTexBox.Text = operationAreaComboBox.Text = "";
                    }
                }
                else
                {
                    
                    errorProviderAddTeam.SetError(teamNameTexBox, "Fill in the Team Name");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*This click event closes the form
         */
        private void closeTeamButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /*This event sets out what happens when this form is closing
         */
        private void AddTeamWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult addTeam = MessageBox.Show("Do you want to add another Team", "Add Team", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (addTeam == DialogResult.Yes)
            {
                e.Cancel = true;
            }
        }

        /*This event prevents one from changing the values in the combobox
         */
        private void operationAreaComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        /*This event is used to assign a name for a team 
         */
        private void operationAreaComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string teamname;
            int number;
            int changed;

            teamname=operationAreaComboBox.SelectedItem.ToString();

            changed = f1.SDB.ChangedTeams(teamname);
            number = f1.SDB.NumberTeamsRegion(teamname);

            if (changed == number)
            {
                teamNameTexBox.Text = (teamname + " " + (number + 1).ToString());
            }
            else
            {
                teamNameTexBox.Text = (teamname + " " + (changed + 1).ToString());
            }
        }

        /*This event prevents one frome entering anything in the textbox 
         */
        private void teamNameTexBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
    }
}
