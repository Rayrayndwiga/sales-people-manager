﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SalesPeople
{
    public partial class ChangeTeam : Form
    {
        private readonly Form1 f1;
        private string name;

        public ChangeTeam(Form1 form,string name)
        {
            InitializeComponent();
            f1 = form;
            this.name = name;
        }


        

        /*This click event changes the Area of operation of the team 
         */
        private void updateTeamOperationAreaButton_Click(object sender, EventArgs e)
        {
            try
            {
                int teamnumber;
                string teamname;

                TeamAreaChange teamarea = new TeamAreaChange();

                f1.SDB.Regions(teamarea.teamNewAreaComboBox.Items);

                if (teamarea.ShowDialog() == DialogResult.OK)
                {
                    if (teamarea.teamNewAreaComboBox.Text != "")
                    {
                        for (int i = 0; i < f1.SDB.totalTeams; i++)
                        {
                            if (f1.SDB.myTeams[i] != null)
                            {
                                if (f1.SDB.myTeams[i].IsTeamName(updateTeamNameTextBox.Text))
                                {
                                    if (f1.SDB.myTeams[i].TeamAreaName != teamarea.teamNewAreaComboBox.Text)
                                    {
                                        for (int j = 0; j < f1.SDB.myTeams[i].TeamMembers.Count;j++)
                                        {
                                            for (int k = 0; k < f1.SDB.totalSalesPeople; k++)
                                            {
                                                if (f1.SDB.mySalesPeople[k] == f1.SDB.myTeams[i].TeamMembers[j])
                                                {
                                                    f1.SDB.RemoveTeamMember(f1.SDB.mySalesPeople[k].FullName + ";" + f1.SDB.mySalesPeople[k].StaffNumber, updateTeamNameTextBox.Text);
                                                }
                                            }
                                        }

                                        teamnumber = f1.SDB.NumberTeamsRegion(teamarea.teamNewAreaComboBox.Text);
                                        teamname = teamarea.teamNewAreaComboBox.Text + " " +(teamnumber + 1).ToString();

                                        f1.SDB.myTeams[i].TeamAreaName = teamarea.teamNewAreaComboBox.Text;
                                        f1.SDB.myTeams[i].TeamName = teamname;
                                        f1.SDB.myTeams[i].TeamLeaderName = "";
                                        updateTeamLeaderTextBox.Text = f1.SDB.myTeams[i].TeamLeaderName;

                                        updateTeamOperationAreaTextBox.Text = f1.SDB.myTeams[i].TeamAreaName;
                                        updateTeamNameTextBox.Text = f1.SDB.myTeams[i].TeamName;
                                    }
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*This click event closes this form
         */
        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /*This event sets out what happens when the form is closing 
         */
        private void ChangeTeam_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult cl = MessageBox.Show("Are you done making changes?", "Quit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (cl == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void updateTeamLeaderButton_Click(object sender, EventArgs e)
        {
            try
            {
                string name;
                TeamLeaderInfo leader = new TeamLeaderInfo();

                f1.SDB.TeamMembersList(leader.teamMembersCheckedListBox.Items, updateTeamNameTextBox.Text);
                if (leader.ShowDialog() == DialogResult.OK)
                {
                    for (int i = 0; i < f1.SDB.totalTeams; i++)
                    {
                        for (int k = 0; k < f1.SDB.totalSalesPeople; k++)
                        {
                            if (f1.SDB.myTeams[i].IsTeamName(updateTeamNameTextBox.Text))
                            {
                                if (leader.teamMembersCheckedListBox.CheckedItems.Count > 0)
                                {
                                    name = leader.teamMembersCheckedListBox.CheckedItems[0].ToString();
                                    f1.SDB.myTeams[i].TeamLeaderName = name;
                                    updateTeamLeaderTextBox.Text = f1.SDB.myTeams[i].TeamLeaderName;
                                }
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
