﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace SalesPeople
{
    public partial class SalesPersonTotalSalesChange : Form
    {
        public float TotalSales;
        public SalesPersonTotalSalesChange()
        {
            InitializeComponent();
        }

        /*This validates the input in the textbox
         */
        private void salesPersonTotalSalesChangeTextBox_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                float total;

                bool isValid = float.TryParse(salesPersonTotalSalesChangeTextBox.Text, NumberStyles.Currency, CultureInfo.GetCultureInfo("en-US"), out total);

                if (salesPersonTotalSalesChangeTextBox.Text != "")
                {
                    if (isValid != true)
                    {
                        throw new Exception("Enter a valid amount");
                    }
                    else
                    {
                        TotalSales = total;
                    }
                }
                else
                {
                    errorProviderSalesTotalSalesChange.SetError(salesPersonTotalSalesChangeTextBox, "Fill in an amount!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*This sets out what happens when the form is closing
         */
        private void SalesPersonTotalSalesChange_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.OK)
            {
                DialogResult change = MessageBox.Show("Are you sure you want to make the change", "Making Change", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (change == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }


    }
}
