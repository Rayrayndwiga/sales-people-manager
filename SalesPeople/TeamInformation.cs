﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SalesPeople
{
    public partial class TeamInformation : Form
    {
        private readonly Form1 f1;
        public TeamInformation(Form1 form)
        {
            InitializeComponent();
            f1 = form;
        }

        
        /*This click event is used to change the name of the team leader 
         */
        private void ChangeTeamLeaderNameButton_Click_1(object sender, EventArgs e)
        {
            try
            {
                string name;
                TeamLeaderInfo leader = new TeamLeaderInfo();

                f1.SDB.TeamMembersList(leader.teamMembersCheckedListBox.Items, teamNameTexBox.Text);
                if (leader.ShowDialog() == DialogResult.OK)
                {
                    for (int i = 0; i < f1.SDB.totalTeams; i++)
                    {
                        for (int k = 0; k < f1.SDB.totalSalesPeople; k++)
                        {
                            if (f1.SDB.myTeams[i].IsTeamName(teamNameTexBox.Text))
                            {
                                if(leader.teamMembersCheckedListBox.CheckedItems.Count > 0)
                                {
                                    name = leader.teamMembersCheckedListBox.CheckedItems[0].ToString();
                                    int staffnumber = 0;

                                    string[] names = name.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                                    staffnumber = Convert.ToInt32(names[1]);

                                    if (f1.SDB.mySalesPeople[k].StaffNumber == staffnumber)
                                    {
                                        f1.SDB.myTeams[i].TeamLeaderName = f1.SDB.mySalesPeople[k].FullName + "Staff Number;" + f1.SDB.mySalesPeople[k].StaffNumber;
                                        teamLeaderNameTextBox.Text = f1.SDB.myTeams[i].TeamLeaderName;
                                    }
                                }
                            }
                        }
                    }
                }

                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*This click event is used to list the members of a certain team
         */
        private void showMembersButton_Click(object sender, EventArgs e)
        {
            this.Close();
            TeamMembersList memberList = new TeamMembersList(f1);

            f1.SDB.TeamMembersList(memberList.membersListBox.Items, teamNameTexBox.Text);

            memberList.ShowDialog();

        }
    }
}
