﻿namespace SalesPeople
{
    partial class TeamWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.teamsListBox = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.addTeamButton = new System.Windows.Forms.Button();
            this.removeTeamButton = new System.Windows.Forms.Button();
            this.listTeamButton = new System.Windows.Forms.Button();
            this.updateTeamDetailsButton = new System.Windows.Forms.Button();
            this.addTeamMembersButton = new System.Windows.Forms.Button();
            this.removeTeamMembersButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.searchTeamSalesOverTextBox = new System.Windows.Forms.TextBox();
            this.searchTeamSalesOverButton = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salespersonRecordsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.teamRecordsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectedSalespersonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allSalespeopleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectedTeamToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allTeamsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label10 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // teamsListBox
            // 
            this.teamsListBox.FormattingEnabled = true;
            this.teamsListBox.Location = new System.Drawing.Point(13, 231);
            this.teamsListBox.Name = "teamsListBox";
            this.teamsListBox.Size = new System.Drawing.Size(509, 277);
            this.teamsListBox.TabIndex = 1;
            this.teamsListBox.Click += new System.EventHandler(this.teamsListBox_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 215);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(265, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Click on Teams to show Team perfomance and Details";
            // 
            // addTeamButton
            // 
            this.addTeamButton.Location = new System.Drawing.Point(17, 43);
            this.addTeamButton.Name = "addTeamButton";
            this.addTeamButton.Size = new System.Drawing.Size(125, 38);
            this.addTeamButton.TabIndex = 3;
            this.addTeamButton.Text = "Add Team";
            this.addTeamButton.UseVisualStyleBackColor = true;
            this.addTeamButton.Click += new System.EventHandler(this.addTeamButton_Click);
            // 
            // removeTeamButton
            // 
            this.removeTeamButton.Location = new System.Drawing.Point(184, 43);
            this.removeTeamButton.Name = "removeTeamButton";
            this.removeTeamButton.Size = new System.Drawing.Size(131, 38);
            this.removeTeamButton.TabIndex = 4;
            this.removeTeamButton.Text = "Remove Team";
            this.removeTeamButton.UseVisualStyleBackColor = true;
            this.removeTeamButton.Click += new System.EventHandler(this.removeTeamButton_Click);
            // 
            // listTeamButton
            // 
            this.listTeamButton.Location = new System.Drawing.Point(387, 101);
            this.listTeamButton.Name = "listTeamButton";
            this.listTeamButton.Size = new System.Drawing.Size(135, 45);
            this.listTeamButton.TabIndex = 5;
            this.listTeamButton.Text = "List Teams";
            this.listTeamButton.UseVisualStyleBackColor = true;
            this.listTeamButton.Click += new System.EventHandler(this.listTeamButton_Click);
            // 
            // updateTeamDetailsButton
            // 
            this.updateTeamDetailsButton.Location = new System.Drawing.Point(387, 43);
            this.updateTeamDetailsButton.Name = "updateTeamDetailsButton";
            this.updateTeamDetailsButton.Size = new System.Drawing.Size(135, 38);
            this.updateTeamDetailsButton.TabIndex = 6;
            this.updateTeamDetailsButton.Text = "Update Team Details";
            this.updateTeamDetailsButton.UseVisualStyleBackColor = true;
            this.updateTeamDetailsButton.Click += new System.EventHandler(this.updateTeamDetailsButton_Click);
            // 
            // addTeamMembersButton
            // 
            this.addTeamMembersButton.Location = new System.Drawing.Point(17, 101);
            this.addTeamMembersButton.Name = "addTeamMembersButton";
            this.addTeamMembersButton.Size = new System.Drawing.Size(125, 45);
            this.addTeamMembersButton.TabIndex = 7;
            this.addTeamMembersButton.Text = "Add Members to Team";
            this.addTeamMembersButton.UseVisualStyleBackColor = true;
            this.addTeamMembersButton.Click += new System.EventHandler(this.addTeamMembersButton_Click);
            // 
            // removeTeamMembersButton
            // 
            this.removeTeamMembersButton.Location = new System.Drawing.Point(184, 101);
            this.removeTeamMembersButton.Name = "removeTeamMembersButton";
            this.removeTeamMembersButton.Size = new System.Drawing.Size(131, 45);
            this.removeTeamMembersButton.TabIndex = 8;
            this.removeTeamMembersButton.Text = "Remove Member from team";
            this.removeTeamMembersButton.UseVisualStyleBackColor = true;
            this.removeTeamMembersButton.Click += new System.EventHandler(this.removeTeamMembersButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(254, 160);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(268, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Search for teams with sales over a value entered below\r\n";
            // 
            // searchTeamSalesOverTextBox
            // 
            this.searchTeamSalesOverTextBox.Location = new System.Drawing.Point(257, 177);
            this.searchTeamSalesOverTextBox.Name = "searchTeamSalesOverTextBox";
            this.searchTeamSalesOverTextBox.Size = new System.Drawing.Size(143, 20);
            this.searchTeamSalesOverTextBox.TabIndex = 10;
            // 
            // searchTeamSalesOverButton
            // 
            this.searchTeamSalesOverButton.Location = new System.Drawing.Point(406, 177);
            this.searchTeamSalesOverButton.Name = "searchTeamSalesOverButton";
            this.searchTeamSalesOverButton.Size = new System.Drawing.Size(113, 23);
            this.searchTeamSalesOverButton.TabIndex = 11;
            this.searchTeamSalesOverButton.Text = "Search";
            this.searchTeamSalesOverButton.UseVisualStyleBackColor = true;
            this.searchTeamSalesOverButton.Click += new System.EventHandler(this.searchTeamSalesOverButton_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(533, 24);
            this.menuStrip1.TabIndex = 12;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveAsToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.printToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem1,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // helpToolStripMenuItem1
            // 
            this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            this.helpToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.helpToolStripMenuItem1.Text = "Help";
            this.helpToolStripMenuItem1.Click += new System.EventHandler(this.helpToolStripMenuItem1_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.saveAsToolStripMenuItem.Text = "Save As";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salespersonRecordsToolStripMenuItem,
            this.teamRecordsToolStripMenuItem});
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.printToolStripMenuItem.Text = "Print";
            // 
            // salespersonRecordsToolStripMenuItem
            // 
            this.salespersonRecordsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectedSalespersonToolStripMenuItem,
            this.allSalespeopleToolStripMenuItem});
            this.salespersonRecordsToolStripMenuItem.Name = "salespersonRecordsToolStripMenuItem";
            this.salespersonRecordsToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.salespersonRecordsToolStripMenuItem.Text = "Salesperson Records";
            // 
            // teamRecordsToolStripMenuItem
            // 
            this.teamRecordsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectedTeamToolStripMenuItem,
            this.allTeamsToolStripMenuItem});
            this.teamRecordsToolStripMenuItem.Name = "teamRecordsToolStripMenuItem";
            this.teamRecordsToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.teamRecordsToolStripMenuItem.Text = "Team Records";
            // 
            // selectedSalespersonToolStripMenuItem
            // 
            this.selectedSalespersonToolStripMenuItem.Name = "selectedSalespersonToolStripMenuItem";
            this.selectedSalespersonToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.selectedSalespersonToolStripMenuItem.Text = "Selected Salesperson";
            this.selectedSalespersonToolStripMenuItem.Click += new System.EventHandler(this.selectedSalespersonToolStripMenuItem_Click);
            // 
            // allSalespeopleToolStripMenuItem
            // 
            this.allSalespeopleToolStripMenuItem.Name = "allSalespeopleToolStripMenuItem";
            this.allSalespeopleToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.allSalespeopleToolStripMenuItem.Text = "All Salespeople";
            this.allSalespeopleToolStripMenuItem.Click += new System.EventHandler(this.allSalespeopleToolStripMenuItem_Click);
            // 
            // selectedTeamToolStripMenuItem
            // 
            this.selectedTeamToolStripMenuItem.Name = "selectedTeamToolStripMenuItem";
            this.selectedTeamToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.selectedTeamToolStripMenuItem.Text = "Selected Team";
            this.selectedTeamToolStripMenuItem.Click += new System.EventHandler(this.selectedTeamToolStripMenuItem_Click);
            // 
            // allTeamsToolStripMenuItem
            // 
            this.allTeamsToolStripMenuItem.Name = "allTeamsToolStripMenuItem";
            this.allTeamsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.allTeamsToolStripMenuItem.Text = "All Teams";
            this.allTeamsToolStripMenuItem.Click += new System.EventHandler(this.allTeamsToolStripMenuItem_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(238, 180);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "$";
            // 
            // TeamWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 528);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.searchTeamSalesOverButton);
            this.Controls.Add(this.searchTeamSalesOverTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.removeTeamMembersButton);
            this.Controls.Add(this.addTeamMembersButton);
            this.Controls.Add(this.updateTeamDetailsButton);
            this.Controls.Add(this.listTeamButton);
            this.Controls.Add(this.removeTeamButton);
            this.Controls.Add(this.addTeamButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.teamsListBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "TeamWindow";
            this.Text = "TeamWindow";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ListBox teamsListBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button addTeamButton;
        private System.Windows.Forms.Button removeTeamButton;
        private System.Windows.Forms.Button listTeamButton;
        private System.Windows.Forms.Button updateTeamDetailsButton;
        private System.Windows.Forms.Button addTeamMembersButton;
        private System.Windows.Forms.Button removeTeamMembersButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox searchTeamSalesOverTextBox;
        private System.Windows.Forms.Button searchTeamSalesOverButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salespersonRecordsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectedSalespersonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allSalespeopleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem teamRecordsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectedTeamToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allTeamsToolStripMenuItem;
        private System.Windows.Forms.Label label10;

    }
}