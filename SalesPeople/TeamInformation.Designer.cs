﻿namespace SalesPeople
{
    partial class TeamInformation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.AreaNameTextBox = new System.Windows.Forms.TextBox();
            this.teamLeaderNameTextBox = new System.Windows.Forms.TextBox();
            this.memberNumberTextBox = new System.Windows.Forms.TextBox();
            this.salesTextBox = new System.Windows.Forms.TextBox();
            this.ChangeTeamLeaderNameButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.showMembersButton = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.teamNameTexBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Team Leader Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Number of team Members";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 137);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Total Sales";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Area of Operation";
            // 
            // AreaNameTextBox
            // 
            this.AreaNameTextBox.Location = new System.Drawing.Point(184, 37);
            this.AreaNameTextBox.Name = "AreaNameTextBox";
            this.AreaNameTextBox.ReadOnly = true;
            this.AreaNameTextBox.Size = new System.Drawing.Size(165, 20);
            this.AreaNameTextBox.TabIndex = 4;
            // 
            // teamLeaderNameTextBox
            // 
            this.teamLeaderNameTextBox.Location = new System.Drawing.Point(184, 66);
            this.teamLeaderNameTextBox.Name = "teamLeaderNameTextBox";
            this.teamLeaderNameTextBox.ReadOnly = true;
            this.teamLeaderNameTextBox.Size = new System.Drawing.Size(165, 20);
            this.teamLeaderNameTextBox.TabIndex = 5;
            // 
            // memberNumberTextBox
            // 
            this.memberNumberTextBox.Location = new System.Drawing.Point(184, 97);
            this.memberNumberTextBox.Name = "memberNumberTextBox";
            this.memberNumberTextBox.ReadOnly = true;
            this.memberNumberTextBox.Size = new System.Drawing.Size(28, 20);
            this.memberNumberTextBox.TabIndex = 6;
            // 
            // salesTextBox
            // 
            this.salesTextBox.Location = new System.Drawing.Point(184, 134);
            this.salesTextBox.Name = "salesTextBox";
            this.salesTextBox.ReadOnly = true;
            this.salesTextBox.Size = new System.Drawing.Size(165, 20);
            this.salesTextBox.TabIndex = 7;
            // 
            // ChangeTeamLeaderNameButton
            // 
            this.ChangeTeamLeaderNameButton.Location = new System.Drawing.Point(182, 176);
            this.ChangeTeamLeaderNameButton.Name = "ChangeTeamLeaderNameButton";
            this.ChangeTeamLeaderNameButton.Size = new System.Drawing.Size(167, 23);
            this.ChangeTeamLeaderNameButton.TabIndex = 8;
            this.ChangeTeamLeaderNameButton.Text = "Team Leader Name";
            this.ChangeTeamLeaderNameButton.UseVisualStyleBackColor = true;
            this.ChangeTeamLeaderNameButton.Click += new System.EventHandler(this.ChangeTeamLeaderNameButton_Click_1);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(0, 176);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(178, 26);
            this.label5.TabIndex = 9;
            this.label5.Text = "Click To enter Team Leaders Name \r\nor to Change it";
            // 
            // showMembersButton
            // 
            this.showMembersButton.Location = new System.Drawing.Point(182, 218);
            this.showMembersButton.Name = "showMembersButton";
            this.showMembersButton.Size = new System.Drawing.Size(167, 23);
            this.showMembersButton.TabIndex = 10;
            this.showMembersButton.Text = "Show members of this team";
            this.showMembersButton.UseVisualStyleBackColor = true;
            this.showMembersButton.Click += new System.EventHandler(this.showMembersButton_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Team Name";
            // 
            // teamNameTexBox
            // 
            this.teamNameTexBox.Location = new System.Drawing.Point(184, 10);
            this.teamNameTexBox.Name = "teamNameTexBox";
            this.teamNameTexBox.ReadOnly = true;
            this.teamNameTexBox.Size = new System.Drawing.Size(165, 20);
            this.teamNameTexBox.TabIndex = 12;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(165, 137);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "$";
            // 
            // TeamInformation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 253);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.teamNameTexBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.showMembersButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ChangeTeamLeaderNameButton);
            this.Controls.Add(this.salesTextBox);
            this.Controls.Add(this.memberNumberTextBox);
            this.Controls.Add(this.teamLeaderNameTextBox);
            this.Controls.Add(this.AreaNameTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "TeamInformation";
            this.Text = "Team Information";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button ChangeTeamLeaderNameButton;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox AreaNameTextBox;
        public System.Windows.Forms.TextBox teamLeaderNameTextBox;
        public System.Windows.Forms.TextBox memberNumberTextBox;
        public System.Windows.Forms.TextBox salesTextBox;
        private System.Windows.Forms.Button showMembersButton;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox teamNameTexBox;
        private System.Windows.Forms.Label label10;
    }
}