﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SalesPeople
{
    public partial class TeamMembersList : Form
    {
        private readonly Form1 f1;
        public TeamMembersList(Form1 form)
        {
            InitializeComponent();
            f1 = form;
        }

       
        

        /*This click vent is used to open the form in which you can update the details of a salesperson 
        * by clicking the name of the salesperson from the list
        */
        private void membersListBox_DoubleClick(object sender, EventArgs e)
        {
            if (membersListBox.SelectedItem != null)
            {
                string name = membersListBox.SelectedItem.ToString();

                int staffnumber = 0;

                string[] names = name.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                staffnumber = Convert.ToInt32(names[1]);

                for (int i = 0; i < f1.SDB.totalSalesPeople; i++)
                {
                    if (f1.SDB.mySalesPeople[i] != null)
                    {
                        if (f1.SDB.mySalesPeople[i].StaffNumber==staffnumber)
                        {
                            Details_Change record = new Details_Change(f1, f1.SDB.mySalesPeople[i].FullName);

                            record.NameChangeTextBox.Text = f1.SDB.mySalesPeople[i].FullName;
                            record.staffNumberChangeTextBox.Text = f1.SDB.mySalesPeople[i].StaffNumber.ToString();
                            record.birthdayChangeTextBox1.Text = f1.SDB.mySalesPeople[i].Birthday;
                            record.NumberChangeTextBox.Text = f1.SDB.mySalesPeople[i].Number;
                            record.EmailChangeTextBox.Text = f1.SDB.mySalesPeople[i].Email;
                            record.AddressChangeTextBox.Text = f1.SDB.mySalesPeople[i].HomeAddress;
                            record.AreaChangeTextBox.Text = f1.SDB.mySalesPeople[i].Country;
                            record.RegionChangeTextBox.Text = f1.SDB.mySalesPeople[i].Region;
                            record.totalChangeTextBox.Text = f1.SDB.mySalesPeople[i].Sales.ToString();

                            record.ShowDialog();
                        }
                    }
                }
            }
        }
    }
}
