﻿namespace SalesPeople
{
    partial class TeamLeaderInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.errorProviderTeamLeaderInfo = new System.Windows.Forms.ErrorProvider(this.components);
            this.teamMembersCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.cancelButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderTeamLeaderInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(241, 52);
            this.label1.TabIndex = 0;
            this.label1.Text = "Check the name of the team leader from the list of\r\nteam members and then click o" +
                "n the OK button\r\n\r\nYou can only add one member at a time";
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(12, 386);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 2;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // errorProviderTeamLeaderInfo
            // 
            this.errorProviderTeamLeaderInfo.ContainerControl = this;
            // 
            // teamMembersCheckedListBox
            // 
            this.teamMembersCheckedListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.teamMembersCheckedListBox.FormattingEnabled = true;
            this.teamMembersCheckedListBox.Location = new System.Drawing.Point(-1, 76);
            this.teamMembersCheckedListBox.Name = "teamMembersCheckedListBox";
            this.teamMembersCheckedListBox.Size = new System.Drawing.Size(352, 304);
            this.teamMembersCheckedListBox.TabIndex = 4;
            this.teamMembersCheckedListBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.teamMembersCheckedListBox_ItemCheck);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(266, 386);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 5;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // TeamLeaderInfo
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(353, 421);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.teamMembersCheckedListBox);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.label1);
            this.Name = "TeamLeaderInfo";
            this.Text = " Change Team Leader";
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderTeamLeaderInfo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.ErrorProvider errorProviderTeamLeaderInfo;
        public System.Windows.Forms.CheckedListBox teamMembersCheckedListBox;
        private System.Windows.Forms.Button cancelButton;
    }
}