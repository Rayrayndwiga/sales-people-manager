﻿namespace SalesPeople
{
    partial class RemoveMembersWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.removeMembersCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.removeMembersListBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.removeMemberButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // removeMembersCheckedListBox
            // 
            this.removeMembersCheckedListBox.FormattingEnabled = true;
            this.removeMembersCheckedListBox.Location = new System.Drawing.Point(2, 64);
            this.removeMembersCheckedListBox.Name = "removeMembersCheckedListBox";
            this.removeMembersCheckedListBox.Size = new System.Drawing.Size(292, 214);
            this.removeMembersCheckedListBox.TabIndex = 0;
            // 
            // removeMembersListBox
            // 
            this.removeMembersListBox.FormattingEnabled = true;
            this.removeMembersListBox.Location = new System.Drawing.Point(342, 66);
            this.removeMembersListBox.Name = "removeMembersListBox";
            this.removeMembersListBox.Size = new System.Drawing.Size(306, 212);
            this.removeMembersListBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(-1, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(269, 26);
            this.label1.TabIndex = 2;
            this.label1.Text = "Check on a person name to remove them from the team\r\nand then click on the remove" +
                " button\r\n";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(339, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "List of members in the team";
            // 
            // removeMemberButton
            // 
            this.removeMemberButton.Location = new System.Drawing.Point(2, 284);
            this.removeMemberButton.Name = "removeMemberButton";
            this.removeMemberButton.Size = new System.Drawing.Size(75, 23);
            this.removeMemberButton.TabIndex = 4;
            this.removeMemberButton.Text = "Remove";
            this.removeMemberButton.UseVisualStyleBackColor = true;
            this.removeMemberButton.Click += new System.EventHandler(this.removeMemberButton_Click);
            // 
            // RemoveMembersWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(657, 319);
            this.Controls.Add(this.removeMemberButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.removeMembersListBox);
            this.Controls.Add(this.removeMembersCheckedListBox);
            this.Name = "RemoveMembersWindow";
            this.Text = "RemoveMembersWindow";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button removeMemberButton;
        public System.Windows.Forms.CheckedListBox removeMembersCheckedListBox;
        public System.Windows.Forms.ListBox removeMembersListBox;
    }
}