﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using System.Drawing.Printing;

namespace SalesPeople
{
    public class Printing
    {
        private Font printingFont = new Font("Verdana", 14);


        public void PrintTextHandler(object sender, PrintPageEventArgs ppeArgs)
        {
            FileStream print = new FileStream(@"..\..\Printing\recordprint.txt", FileMode.Open);
            StreamReader reader = new StreamReader(print);

            //Get the Graphics object
            Graphics graphics = ppeArgs.Graphics;

            float linesPerPage = 0;
            float yPos = 0;
            int count = 0;

            //Read margins from PrintPageEventArgs

            float leftMargin = ppeArgs.MarginBounds.Left;
            float topMargin = ppeArgs.MarginBounds.Top;
            string line = null;

            //Calculate the lines per page

            linesPerPage = ppeArgs.MarginBounds.Height / printingFont.GetHeight(graphics);

            while (count < linesPerPage && ((line = reader.ReadLine()) != null))
            {
                //Calculate starting position
                yPos = topMargin + (count * printingFont.GetHeight(graphics));

                //Draw Text
                graphics.DrawString(line, printingFont, Brushes.Black, leftMargin, yPos, new StringFormat());

                //Move to the next line
                count++;
            }
            //If PrintPageEventArgs has more pages to print
            if (line != null)
            {
                ppeArgs.HasMorePages = true;
            }
            else
            {
                ppeArgs.HasMorePages = false;
            }
            reader.Close();
            print.Close();
        }
    }
}
