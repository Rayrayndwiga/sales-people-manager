﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SalesPeople
{
    public partial class salesPersonChange : Form
    {
        private readonly Form1 f1;
        public salesPersonChange(Form1 form)
        {
            InitializeComponent();
            f1 = form;
        }

        

        private void searchChangeButton_Click(object sender, EventArgs e)
        {
            try
            {
                errorProvider.Clear();

                if (searchChangeTextBox.Text != "")
                {
                    if (f1.SDB.NameContained(searchChangeTextBox.Text) == true)
                    {
                        f1.SDB.ListClientName(searchChangeTextBox.Text, changeListBox.Items);
                        searchChangeTextBox.Text = "";
                    }
                    else
                    {
                        throw new Exception("This name is not associated with any of the sales people");
                    }
                }
                else
                {
                    errorProvider.SetError(searchChangeButton, "Fill in a name");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void changeListBox_DoubleClick(object sender, EventArgs e)
        {
            if (changeListBox.SelectedItem != null)
            {
                string name = changeListBox.SelectedItem.ToString();

                int staffnumber = 0;

                string[] names = name.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                staffnumber = Convert.ToInt32(names[1]);

                for (int i = 0; i < f1.SDB.totalSalesPeople; i++)
                {
                    if (f1.SDB.mySalesPeople[i] != null)
                    {
                        if (f1.SDB.mySalesPeople[i].StaffNumber == staffnumber)
                        {
                            Details_Change record = new Details_Change(f1, f1.SDB.mySalesPeople[i].FullName);

                            record.NameChangeTextBox.Text = f1.SDB.mySalesPeople[i].FullName;
                            record.staffNumberChangeTextBox.Text = f1.SDB.mySalesPeople[i].StaffNumber.ToString();
                            record.birthdayChangeTextBox1.Text = f1.SDB.mySalesPeople[i].Birthday;
                            record.NumberChangeTextBox.Text = f1.SDB.mySalesPeople[i].Number;
                            record.EmailChangeTextBox.Text = f1.SDB.mySalesPeople[i].Email;
                            record.AddressChangeTextBox.Text = f1.SDB.mySalesPeople[i].HomeAddress;
                            record.AreaChangeTextBox.Text = f1.SDB.mySalesPeople[i].Country;
                            record.RegionChangeTextBox.Text = f1.SDB.mySalesPeople[i].Region;
                            record.totalChangeTextBox.Text = f1.SDB.mySalesPeople[i].Sales.ToString();

                            record.ShowDialog(this);
                        }
                    }
                }
            }
        }
    }
}
