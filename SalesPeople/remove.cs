﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SalesPeople
{
    public partial class remove : Form
    {
        private readonly Form1 f1;
        public remove(Form1 form)
        {
            InitializeComponent();
            f1 = form;
        }

        /*This sets out what happens when the form is closing
         */
        private void remove_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.OK)
            {
                DialogResult remove = MessageBox.Show("Are you sure you want to remove this Personnel", "Removing Personnel", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (remove == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }

        private void removePersonButton_Click(object sender, EventArgs e)
        {
            try
            {
                string name="";
                string teamName = "";
                while (removeSalespersonCheckListBox.CheckedItems.Count > 0)
                {
                    name = removeSalespersonCheckListBox.CheckedItems[0].ToString();

                    teamName = f1.SDB.memberTeamName(name);
                    f1.SDB.RemoveTeamMember(name, teamName);

                    if (f1.SDB.IsTeamLeader(name,teamName))
                    {
                        MessageBox.Show("You need a new team leader for" + " " + teamName + " team because the previous leader has been removed as a salesperson", "Team Leader Removed", MessageBoxButtons.OK);
                        f1.SDB.RemoveTeamLeader(teamName);
                    }

                    if (!f1.SDB.RemoveSalesPerson(name))
                    {
                        throw new Exception("Cannot remove this salesperson");
                    }
                    else
                    {
                        removeSalespersonCheckListBox.Items.Remove(name);
                        throw new Exception("Sales person has been successfully removed");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void removeSalesPersonSearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                errorProvider.Clear();

                if (removeSalesPersonTextBox.Text != "")
                {
                    if (f1.SDB.NameContained(removeSalesPersonTextBox.Text) == true)
                    {
                        f1.SDB.ListClientName(removeSalesPersonTextBox.Text, removeSalespersonCheckListBox.Items);
                        removeSalesPersonTextBox.Text = "";
                    }
                    else
                    {
                        throw new Exception("This name is not associated with any of the sales people");
                    }
                }
                else
                {
                    errorProvider.SetError(removeSalesPersonSearchButton, "Fill in a name");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        
    }
}
