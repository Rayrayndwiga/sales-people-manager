﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SalesPeople
{
    public partial class SalesPersonAreaChange : Form
    {
        public SalesPersonAreaChange()
        {
            InitializeComponent();
        }

        /*This sets out what happens when the form is closing
         */
        private void SalesPersonAreaChange_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.OK)
            {
                DialogResult change = MessageBox.Show("Are you sure you want to make the change", "Making Change", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (change == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }

        private void areaChangeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] Asia = {"Brunei","Cambodia","China","India","Indonesia","Japan","Kazakhstan","North Korea","South Korea","Kyrgyzstan","Laos","Malaysia",

                                "Maldives","Mongolia","Myanmar","Nepal","Philippines","Singapore","Sri Lanka","Taiwan","Tajikistan","Thailand","Turkmenistan",

                                "Uzbekistan","Vietnam","Bangladesh",

                                "Bhutan"};


            string[] MiddleEastNorthAfricaandGreaterArabia = { "Afghanistan","Algeria","Azerbaijan","Bahrain","Egypt","Iran","Iraq",

                                                                 "Israel","Jordan","Kuwait","Lebanon","Libya","Morocco","Oman","Pakistan",

                                                                 "Qatar","Saudi Arabia","Somalia","Syria","Tunisia","Turkey","United Arab Emirates",

                                                                 "Yemen"};

            string[] Europe = {"Albania","Andorra","Armenia","Austria","Belarus","Belgium","Bosnia and Herzegovina","Bulgaria","Croatia","Cyprus","Czech Republic",

                                  "Denmark","Estonia","Finland","France","Georgia","Germany","Greece","Hungary","Iceland","Ireland","Italy","Kosovo","Latvia",

                                  "Liechtenstein","Lithuania","Luxembourg","Macedonia","Malta","Moldova","Monaco","Montenegro","Netherlands","Norway","Poland",

                                  "Portugal","Romania","Russia","San Marino","Serbia","Slovakia","Slovenia","Spain","Sweden","Switzerland","Ukraine",

                                  "United Kingdom of Great Britain","Northern Ireland",

                                  "Vatican City" };

            string[] SubSaharanAfrica = {"Angola","Benin","Botswana","Burkina Faso","Burundi","Cameroon","Cape Verde","Central African Republic","Chad","Comoros",

                                            "Republic of the Congo","Democratic Republic of the Congo","Cote d'Ivoire","Djibouti","Equatorial Guinea","Eritrea",

                                            "Ethiopia","Gabon","The Gambia","Ghana","Guinea","Guinea-Bissau","Kenya","Lesotho","Liberia","Madagascar",

                                            "Malawi","Mali", "Mauritania","Mauritius","Mozambique","Namibia","Niger","Nigeria","Rwanda",

                                            "Sao Tome and Principe","Senegal","Seychelles","Sierra Leone","South Africa","South Sudan","Sudan","Swaziland","Tanzania",

                                            "Togo","Uganda","Zambia","Zimbabwe"};

            string[] NorthAmerica = { "Canada", "Greenland", "Mexico", "United States of America" };

            string[] CentralAmericaAndTheCaribbean = {"Antigua and Barbuda","The Bahamas","Barbados","Belize","Costa Rica","Cuba","Dominica","Dominican Republic",

                                                         "El Salvador","Grenada","Guatemala","Haiti","Honduras","Jamaica","Nicaragua","Panama","Saint Kitts and Nevis",

                                                         "Saint Lucia","Saint Vincent and the Grenadines",

                                                         "Trinidad and Tobago" };

            string[] SouthAmerica = { "Argentina", "Bolivia", "Brazil", "Chile", "Colombia", "Ecuador", "Guyana", "Paraguay", "Peru", "Suriname", "Uruguay", "Venezuela" };

            string[] AustraliaAndOceania = {"Australia","East Timor","Fiji","Kiribati","Marshall Islands","Federated States of Micronesia","Nauru","New Zealand",

                                               "Palau","Papua New Guinea","Samoa","Solomon Islands","Tonga","Tuvalu",

                                               "Vanuatu"};

            if (Asia.Contains(areaChangeComboBox.SelectedItem))
            {
                regionChangeSalesPersonTextBox.Text = "Asia";
            }
            if (MiddleEastNorthAfricaandGreaterArabia.Contains(areaChangeComboBox.SelectedItem))
            {
                regionChangeSalesPersonTextBox.Text = "MiddleEastNorthAfricaandGreaterArabia";
            }
            if (SubSaharanAfrica.Contains(areaChangeComboBox.SelectedItem))
            {
                regionChangeSalesPersonTextBox.Text = "SubSaharanAfrica";
            }
            if (Europe.Contains(areaChangeComboBox.SelectedItem))
            {
                regionChangeSalesPersonTextBox.Text = "Europe";
            }
            if (CentralAmericaAndTheCaribbean.Contains(areaChangeComboBox.SelectedItem))
            {
                regionChangeSalesPersonTextBox.Text = "CentralAmericaAndTheCaribbean";
            }
            if (NorthAmerica.Contains(areaChangeComboBox.SelectedItem))
            {
                regionChangeSalesPersonTextBox.Text = "NorthAmerica";
            }
            if (SouthAmerica.Contains(areaChangeComboBox.SelectedItem))
            {
                regionChangeSalesPersonTextBox.Text = "SouthAmerica";
            }
            if (AustraliaAndOceania.Contains(areaChangeComboBox.SelectedItem))
            {
                regionChangeSalesPersonTextBox.Text = "AustraliaAndOceania";
            }
        }

        private void areaChangeComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
    }
}
