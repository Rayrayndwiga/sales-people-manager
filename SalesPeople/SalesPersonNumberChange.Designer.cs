﻿namespace SalesPeople
{
    partial class SalesPersonNumberChange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.salesPersonNumberChangeTextBox = new System.Windows.Forms.MaskedTextBox();
            this.errorProviderSalesNumberChange = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderSalesNumberChange)).BeginInit();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(23, 119);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 0;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(197, 119);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(242, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Enter the new Phone Number of the Sales Person";
            // 
            // salesPersonNumberChangeTextBox
            // 
            this.salesPersonNumberChangeTextBox.Location = new System.Drawing.Point(23, 73);
            this.salesPersonNumberChangeTextBox.Mask = "(999) 000-0000";
            this.salesPersonNumberChangeTextBox.Name = "salesPersonNumberChangeTextBox";
            this.salesPersonNumberChangeTextBox.Size = new System.Drawing.Size(249, 20);
            this.salesPersonNumberChangeTextBox.TabIndex = 4;
            // 
            // errorProviderSalesNumberChange
            // 
            this.errorProviderSalesNumberChange.ContainerControl = this;
            // 
            // SalesPersonNumberChange
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(284, 170);
            this.ControlBox = false;
            this.Controls.Add(this.salesPersonNumberChangeTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Name = "SalesPersonNumberChange";
            this.Text = "Sales Person Number Change";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SalesPersonNumberChange_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderSalesNumberChange)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.MaskedTextBox salesPersonNumberChangeTextBox;
        public System.Windows.Forms.ErrorProvider errorProviderSalesNumberChange;
    }
}