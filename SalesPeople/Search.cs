﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace SalesPeople
{
    public partial class Search : Form
    {
        private readonly Form1 f1;
        public Search(Form1 form)
        {
            InitializeComponent();
            f1 = form;
        }

        /*This click event lists all the salespeople in the listbox
         */
        private void searchAllRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            salesOverTextBox.Visible = false;
            searchAreaComboBox.Visible = false;
            f1.SDB.ListClients(searchListBox.Items);
        }

        /*This event lists all the sales people whose sales are above 1 Million
         */
        private void salesOver1MRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            salesOverTextBox.Visible = false;
            searchAreaComboBox.Visible = false;
            f1.SDB.overMillion(searchListBox.Items);
        }

        private void searchAreaRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            salesOverTextBox.Visible = false;
            searchAreaComboBox.Visible = true;
            searchAreaComboBox.Items.Clear();
            f1.SDB.Regions(searchAreaComboBox.Items);
        }

        private void searchAreaComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string area = "";

            if (searchAreaComboBox.SelectedItem != null)
            {
                area = searchAreaComboBox.SelectedItem.ToString();
            }
            f1.SDB.AreaList(area, searchListBox.Items);
        }

        /*This  event checks lists the salespeople within a certin area
         */
        private void searchAreaComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        /*This click event checks if the name enterd in the textbox beside
         * it is associated with any of the salespeople
         */
        private void searchPersonButton_Click(object sender, EventArgs e)
        {
            try
            {
                errorProvider.Clear();

                if (searchNameTextBox.Text != "")
                {
                    if (f1.SDB.NameContained(searchNameTextBox.Text) == true)
                    {
                        f1.SDB.ListClientName(searchNameTextBox.Text, searchListBox.Items);
                        searchNameTextBox.Text = "";
                    }
                    else
                    {
                        throw new Exception("This name is not associated with any of the sales people");
                    }
                }
                else
                {
                    errorProvider.SetError(searchPersonButton, "Fill in a name");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*This click event is used to open the form where one can update the details of 
         * salesperson when one double clicks on the name of the salesperson from the listbox
         */
        private void searchListBox_DoubleClick(object sender, EventArgs e)
        {
            if (searchListBox.SelectedItem != null)
            {
                string name = searchListBox.SelectedItem.ToString();

                int staffnumber = 0;

                string[] names = name.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                staffnumber = Convert.ToInt32(names[1]);

                for (int i = 0; i < f1.SDB.totalSalesPeople; i++)
                {
                    if (f1.SDB.mySalesPeople[i] != null)
                    {
                        if (f1.SDB.mySalesPeople[i].StaffNumber == staffnumber)
                        {
                            Details_Change record = new Details_Change(f1, f1.SDB.mySalesPeople[i].FullName);

                            record.NameChangeTextBox.Text = f1.SDB.mySalesPeople[i].FullName;
                            record.staffNumberChangeTextBox.Text = f1.SDB.mySalesPeople[i].StaffNumber.ToString();
                            record.birthdayChangeTextBox1.Text = f1.SDB.mySalesPeople[i].Birthday;
                            record.NumberChangeTextBox.Text = f1.SDB.mySalesPeople[i].Number;
                            record.EmailChangeTextBox.Text = f1.SDB.mySalesPeople[i].Email;
                            record.AddressChangeTextBox.Text = f1.SDB.mySalesPeople[i].HomeAddress;
                            record.AreaChangeTextBox.Text = f1.SDB.mySalesPeople[i].Country;
                            record.RegionChangeTextBox.Text = f1.SDB.mySalesPeople[i].Region;
                            record.totalChangeTextBox.Text = f1.SDB.mySalesPeople[i].Sales.ToString();

                            record.ShowDialog(this);
                        }
                    }
                }
            }
        }

        private void salesOverTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (!Char.IsDigit(e.KeyChar) && e.KeyChar != '\r' && e.KeyChar != '\b')
                {
                    e.Handled = true;
                }

                if (e.KeyChar == '\r')
                {
                    float total;
                    bool isValid = float.TryParse(salesOverTextBox.Text, NumberStyles.Currency, CultureInfo.GetCultureInfo("en-US"), out total);

                    if (isValid != true)
                    {
                        throw new Exception("Enter a valid amount");
                    }
                    else
                    {
                        f1.SDB.OverSalesValue(searchListBox.Items, total);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void searchSalesOverValueRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            searchAreaComboBox.Visible = false;
            salesOverTextBox.Visible = true;
        }

        
    }
}
