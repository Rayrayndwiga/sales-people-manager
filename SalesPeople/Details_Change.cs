﻿using System;
using System.Windows.Forms;
using System.Globalization;
using System.IO;
using System.Collections.Generic;
using System.Drawing.Printing;

namespace SalesPeople
{
    public partial class Details_Change : Form
    {
        private readonly Form1 f1;
        private string name;
        public Details_Change(Form1 f1,string name)
        {
            InitializeComponent();
            this.f1 = f1;
            this.name = name;
        }

          
        /*This event closes the form
         */
        private void CloseChangeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /*This click event is used to update the name of the salesperson
         * from the current one to a new one
         */
        private void updateNameButton_Click(object sender, EventArgs e)
        {
            try
            {
                SalesPersonNameChange name = new SalesPersonNameChange();

                if (name.ShowDialog() == DialogResult.OK)
                {
                    if (name.salesPersonNameChangeTextBox.Text != "")
                    {
                        for (int i = 0; i < f1.SDB.totalSalesPeople; i++)
                        {
                            if (f1.SDB.mySalesPeople[i] != null)
                            {
                                if (f1.SDB.mySalesPeople[i].StaffNumber == Convert.ToInt32(staffNumberChangeTextBox.Text))
                                {
                                    f1.SDB.mySalesPeople[i].FullName = name.salesPersonNameChangeTextBox.Text;
                                    NameChangeTextBox.Text = f1.SDB.mySalesPeople[i].FullName;
                                }
                            }
                        }
                    }
                    else
                    {
                        name.errorProviderSalesNameChange.SetError(name.salesPersonNameChangeTextBox, "Fill in a name!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*This click event is used to update the phone number of the salesperson
         * from the current one to a new one
         */
        private void updatePhoneNumberButton_Click(object sender, EventArgs e)
        {
            try
            {
                SalesPersonNumberChange number = new SalesPersonNumberChange();

                if (number.ShowDialog() == DialogResult.OK)
                {
                    if (number.salesPersonNumberChangeTextBox.Text != "")
                    {
                        for (int i = 0; i < f1.SDB.totalSalesPeople; i++)
                        {
                            if (f1.SDB.mySalesPeople[i] != null)
                            {
                                if (f1.SDB.mySalesPeople[i].StaffNumber == Convert.ToInt32(staffNumberChangeTextBox.Text))
                                {
                                    f1.SDB.mySalesPeople[i].Number = number.salesPersonNumberChangeTextBox.Text;
                                    NumberChangeTextBox.Text = f1.SDB.mySalesPeople[i].Number;
                                }
                            }
                        }
                    }
                    else
                    {
                        number.errorProviderSalesNumberChange.SetError(number.salesPersonNumberChangeTextBox, "Fill in a name!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*This click event is used to update the email of the salesperson
         * from the current one to a new one
         */
        private void updateEmailButton_Click(object sender, EventArgs e)
        {
            try
            {
                string emailAddress = @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";//checking the email address

                SalesPersonEmailChange email = new SalesPersonEmailChange();

                if (email.ShowDialog() == DialogResult.OK)
                {
                    if (email.salesPersonEmailChangeTextBox.Text != "")
                    {
                        if (!System.Text.RegularExpressions.Regex.IsMatch(email.salesPersonEmailChangeTextBox.Text, emailAddress))
                        {
                            throw new Exception("Enter the email address in this format username@domain.com");
                        }

                        for (int i = 0; i < f1.SDB.totalSalesPeople; i++)
                        {
                            if (f1.SDB.mySalesPeople[i] != null)
                            {
                                if (f1.SDB.mySalesPeople[i].StaffNumber == Convert.ToInt32(staffNumberChangeTextBox.Text))
                                {
                                    f1.SDB.mySalesPeople[i].Email = email.salesPersonEmailChangeTextBox.Text;
                                    EmailChangeTextBox.Text = f1.SDB.mySalesPeople[i].Email;
                                }
                            }
                        }
                    }
                    else
                    {
                        email.errorProviderSalesEmailChange.SetError(email.salesPersonEmailChangeTextBox, "Fill in an Email Address!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*This click event is used to update the Address of the salesperson
         * from the current one to a new one
         */
        private void updateAddressButton_Click(object sender, EventArgs e)
        {
            try
            {
                SalesPersonAddressChange address = new SalesPersonAddressChange();

                if (address.ShowDialog() == DialogResult.OK)
                {
                    if (address.salesPersonAddressChangeTextBox.Text != "")
                    {
                        for (int i = 0; i < f1.SDB.totalSalesPeople; i++)
                        {
                            if (f1.SDB.mySalesPeople[i] != null)
                            {
                                if (f1.SDB.mySalesPeople[i].StaffNumber == Convert.ToInt32(staffNumberChangeTextBox.Text))
                                {
                                    f1.SDB.mySalesPeople[i].HomeAddress = address.salesPersonAddressChangeTextBox.Text;
                                    AddressChangeTextBox.Text = f1.SDB.mySalesPeople[i].HomeAddress;
                                }
                            }
                        }
                    }
                    else
                    {
                        address.errorProviderSalesAddressChange.SetError(address.salesPersonAddressChangeTextBox, "Fill in an Address!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*This click event is used to update the Area of operation of the salesperson
         * from the current one to a new one
         */
        private void updateAreaButton_Click(object sender, EventArgs e)
        {
            try
            {
                SalesPersonAreaChange area = new SalesPersonAreaChange();

                f1.SDB.Countries(area.areaChangeComboBox.Items);
                if (area.ShowDialog() == DialogResult.OK)
                {
                    if (area.areaChangeComboBox.Text != "")
                    {
                        for (int i = 0; i < f1.SDB.totalSalesPeople; i++)
                        {
                            if (f1.SDB.mySalesPeople[i] != null)
                            {
                                if (f1.SDB.mySalesPeople[i].StaffNumber == Convert.ToInt32(staffNumberChangeTextBox.Text))
                                {
                                    f1.SDB.mySalesPeople[i].Region = area.regionChangeSalesPersonTextBox.Text;
                                    AreaChangeTextBox.Text = area.areaChangeComboBox.Text;
                                    RegionChangeTextBox.Text = f1.SDB.mySalesPeople[i].Region;
                                }
                            }
                        }
                    }
                    else
                    {
                        area.errorProviderSalesAreaChange.SetError(area.areaChangeComboBox, "Fill in an Area Name!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*This click event is used to update the sales of the salesperson
         * from the current one to a new one
         */
        private void updateTotalSalesButton_Click(object sender, EventArgs e)
        {
            try
            {
                SalesPersonTotalSalesChange totalSales = new SalesPersonTotalSalesChange();

                float total;
     
                if (totalSales.ShowDialog() == DialogResult.OK)
                {
                    if (totalSales.salesPersonTotalSalesChangeTextBox.Text != "")
                    {
                        
                        for (int i = 0; i < f1.SDB.totalSalesPeople; i++)
                        {
                            if (f1.SDB.mySalesPeople[i] != null)
                            {
                                if (f1.SDB.mySalesPeople[i].StaffNumber == Convert.ToInt32(staffNumberChangeTextBox.Text))
                                {
                                    total = totalSales.TotalSales;
                                    f1.SDB.mySalesPeople[i].Sales = total;
                                    totalChangeTextBox.Text = f1.SDB.mySalesPeople[i].Sales.ToString();
                                }
                            }
                        }
                    }
                    else
                    {
                        totalSales.errorProviderSalesTotalSalesChange.SetError(totalSales.salesPersonTotalSalesChangeTextBox, "Fill in the total sales amount!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*This event sets out what happens whe the form is closing
         */
        private void Details_Change_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult cl = MessageBox.Show("Are you done making changes?", "Quit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (cl == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        /*This click eventis used to update the date of birth of the Sales
         * Person
         */
        private void updateBirthdayButton_Click(object sender, EventArgs e)
        {
            try
            {
                SalesPersonBirthdayChange birthday = new SalesPersonBirthdayChange();

                if (birthday.ShowDialog() == DialogResult.OK)
                {
                    if (birthday.changeDateTimePicker.Text != "")
                    {

                        for (int i = 0; i < f1.SDB.totalSalesPeople; i++)
                        {
                            if (f1.SDB.mySalesPeople[i] != null)
                            {
                                if (f1.SDB.mySalesPeople[i].StaffNumber == Convert.ToInt32(staffNumberChangeTextBox.Text))
                                {
                                    f1.SDB.mySalesPeople[i].Birthday = birthday.changeDateTimePicker.Text; ;
                                    birthdayChangeTextBox1.Text = f1.SDB.mySalesPeople[i].Birthday;
                                }
                            }
                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*This click event is used to change the staff number of an exusting sales person
         */
        private void updateStaffNumberButton_Click(object sender, EventArgs e)
        {
            try
            {
                int Number;
                SalesPersonStaffNumberChange staffnumber = new SalesPersonStaffNumberChange(f1);

                if (staffnumber.ShowDialog() == DialogResult.OK)
                {
                    if (staffnumber.updateStaffNumberTextBox.Text != "")
                    {
                        for (int i = 0; i < f1.SDB.totalSalesPeople; i++)
                        {
                            if (f1.SDB.mySalesPeople[i] != null)
                            {
                                if (f1.SDB.mySalesPeople[i].StaffNumber == Convert.ToInt32(staffNumberChangeTextBox.Text))
                                {
                                    if (!int.TryParse(staffnumber.updateStaffNumberTextBox.Text, out Number))
                                    {
                                        throw new Exception("Please enter a valid integer number as a staff number");
                                    }

                                    if (!f1.SDB.IsStaffNumber(Number))
                                    {
                                        f1.SDB.mySalesPeople[i].StaffNumber = Number;
                                        staffNumberChangeTextBox.Text = f1.SDB.mySalesPeople[i].StaffNumber.ToString();
                                    }
                                    else
                                    {
                                        throw new Exception("Another Sales person has the Staff Number you have entered");
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*This click event is used to save a salesperson record into a new file
         */
        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {

                SaveFileDialog saving = new SaveFileDialog();

                saving.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                saving.Title = "Save Personnel Data";
                saving.RestoreDirectory = true;
                String path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), @"..\..\Records");
                saving.InitialDirectory = path;

                saving.ShowDialog(this);

                if (saving.FileName != "")
                {

                    FileStream savingFS = new FileStream(saving.FileName, FileMode.OpenOrCreate);
                    StreamWriter savingSW = new StreamWriter(savingFS);

                    for (int i = 0; i < f1.SDB.GetConstant(); i++)
                    {
                        if (f1.SDB.mySalesPeople[i] != null || f1.SDB.myTeams[i] != null)
                        {
                            if (f1.SDB.mySalesPeople[i].HomeAddress == "")
                            {
                                f1.SDB.mySalesPeople[i].HomeAddress = "null";
                            }
                            if (f1.SDB.mySalesPeople[i].Email == "")
                            {
                                f1.SDB.mySalesPeople[i].Email = "null";
                            }
                            if (f1.SDB.mySalesPeople[i].Country == "")
                            {
                                f1.SDB.mySalesPeople[i].Country = "null";
                            }
                            if (f1.SDB.mySalesPeople[i].Region == "")
                            {
                                f1.SDB.mySalesPeople[i].Region = "null";
                            }
                            if (f1.SDB.mySalesPeople[i].teamName == "")
                            {
                                f1.SDB.mySalesPeople[i].teamName = "null";
                            }

                            if (f1.SDB.myTeams[i] != null)
                            {
                                if (f1.SDB.myTeams[i].TeamLeaderName == "")
                                {
                                    f1.SDB.myTeams[i].TeamLeaderName = "null";
                                }

                                savingSW.WriteLine(f1.SDB.mySalesPeople[i].FullName + ";" + f1.SDB.mySalesPeople[i].Birthday + ";" + f1.SDB.mySalesPeople[i].Number + ";" + f1.SDB.mySalesPeople[i].Email + ";" + f1.SDB.mySalesPeople[i].HomeAddress + ";" + f1.SDB.mySalesPeople[i].Country + ";" + f1.SDB.mySalesPeople[i].Region + ";" + f1.SDB.mySalesPeople[i].Sales + ";" + f1.SDB.mySalesPeople[i].TeamName + ";" + f1.SDB.mySalesPeople[i].StaffNumber + ">" + f1.SDB.myTeams[i].TeamName + ":" + f1.SDB.myTeams[i].TeamAreaName + ":" + f1.SDB.myTeams[i].TeamLeaderName + ":" + f1.SDB.myTeams[i].Members + ":" + f1.SDB.myTeams[i].Amount);
                            }
                            else
                            {
                                savingSW.WriteLine(f1.SDB.mySalesPeople[i].FullName + ";" + f1.SDB.mySalesPeople[i].Birthday + ";" + f1.SDB.mySalesPeople[i].Number + ";" + f1.SDB.mySalesPeople[i].Email + ";" + f1.SDB.mySalesPeople[i].HomeAddress + ";" + f1.SDB.mySalesPeople[i].Country + ";" + f1.SDB.mySalesPeople[i].Region + ";" + f1.SDB.mySalesPeople[i].Sales + ";" + f1.SDB.mySalesPeople[i].TeamName + ";" + f1.SDB.mySalesPeople[i].StaffNumber + ">");
                            }
                        }
                    }
                    savingSW.Close();
                    savingFS.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*This click event invokes the openfiledialog 
       */
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog opening = new OpenFileDialog();

                String path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), @"..\..\Records");

                opening.InitialDirectory = path;
                opening.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                opening.Title = "Open Saved Personnel Data";

                opening.ShowDialog();


                if (opening.FileName != "")
                {

                    List<string> personnelList = new List<string>();


                    for (int i = 0; i < f1.SDB.GetConstant(); i++)
                    {
                        if (f1.SDB.mySalesPeople[i] != null)
                        {
                            f1.SDB.mySalesPeople[i] = null;
                        }

                        if (f1.SDB.myTeams[i] != null)
                        {
                            f1.SDB.myTeams[i] = null;
                        }
                    }

                    FileStream openFS = new FileStream(opening.FileName, FileMode.Open);
                    f1.openFile = opening.FileName;

                    StreamReader openingSR = new StreamReader(openFS);

                    string lines;

                    while ((lines = openingSR.ReadLine()) != null)
                    {
                        personnelList.Add(lines);
                    }


                    foreach (string s in personnelList)
                    {
                        string[] info = s.Split(new string[] { ">" }, StringSplitOptions.RemoveEmptyEntries);

                        int n = info.Length;


                        string[] person = info[0].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

                        int m = person.Length;



                        if (n == 2)
                        {
                            string[] team = info[1].Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);

                            int k = team.Length;

                            //Adding the teams
                            f1.SDB.AddTeam(team[0], team[1]);

                            for (int i = 0; i < f1.SDB.GetConstant(); i++)
                            {
                                if (f1.SDB.myTeams[i] != null)
                                {
                                    if (f1.SDB.myTeams[i].IsTeamName(team[0]))
                                    {
                                        f1.SDB.myTeams[i].TeamLeaderName = team[2];
                                        f1.SDB.myTeams[i].Amount = Convert.ToInt32(team[4]);
                                    }
                                }
                            }

                            //Adding the Sales People

                            f1.SDB.AddSalesPerson(person[0], person[1], person[2], person[3], person[4], person[5], person[6], Convert.ToSingle(person[7]), Convert.ToInt32(person[9]));

                            for (int i = 0; i < f1.SDB.GetConstant(); i++)
                            {
                                if (f1.SDB.mySalesPeople[i] != null)
                                {
                                    if (f1.SDB.mySalesPeople[i].StaffNumber == Convert.ToInt32(person[9]))
                                    {
                                        f1.SDB.mySalesPeople[i].TeamName = person[8];
                                        f1.SDB.AddTeamMember(f1.SDB.mySalesPeople[i].FullName + ";" + f1.SDB.mySalesPeople[i].StaffNumber, f1.SDB.mySalesPeople[i].TeamName);
                                    }
                                }
                            }
                        }
                        else if (n == 1)
                        {
                            f1.SDB.AddSalesPerson(person[0], person[1], person[2], person[3], person[4], person[5], person[6], Convert.ToSingle(person[7]), Convert.ToInt32(person[9]));

                            for (int i = 0; i < f1.SDB.GetConstant(); i++)
                            {
                                if (f1.SDB.mySalesPeople[i] != null)
                                {
                                    if (f1.SDB.mySalesPeople[i].StaffNumber == Convert.ToInt32(person[9]))
                                    {
                                        f1.SDB.mySalesPeople[i].TeamName = person[8];
                                        f1.SDB.AddTeamMember(f1.SDB.mySalesPeople[i].FullName + ";" + f1.SDB.mySalesPeople[i].StaffNumber, f1.SDB.mySalesPeople[i].TeamName);
                                    }
                                }
                            }
                        }
                    }

                    openFS.Close();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /*This click event is used to save details into a file that is already open
         */
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (File.Exists(f1.openFile))
            {
                FileStream existing = new FileStream(f1.openFile, FileMode.Truncate);

                StreamWriter existingSW = new StreamWriter(existing);

                for (int i = 0; i < f1.SDB.totalSalesPeople; i++)
                {
                    if (f1.SDB.mySalesPeople[i] != null || f1.SDB.myTeams[i] != null)
                    {
                        if (f1.SDB.mySalesPeople[i] != null)
                        {
                            if (f1.SDB.mySalesPeople[i].HomeAddress == "")
                            {
                                f1.SDB.mySalesPeople[i].HomeAddress = "null";
                            }
                            if (f1.SDB.mySalesPeople[i].Email == "")
                            {
                                f1.SDB.mySalesPeople[i].Email = "null";
                            }
                            if (f1.SDB.mySalesPeople[i].Country == "")
                            {
                                f1.SDB.mySalesPeople[i].Country = "null";
                            }
                            if (f1.SDB.mySalesPeople[i].Region == "")
                            {
                                f1.SDB.mySalesPeople[i].Region = "null";
                            }
                            if (f1.SDB.mySalesPeople[i].teamName == "")
                            {
                                f1.SDB.mySalesPeople[i].teamName = "null";
                            }

                            if (f1.SDB.myTeams[i] != null)
                            {
                                if (f1.SDB.myTeams[i].TeamLeaderName == "")
                                {
                                    f1.SDB.myTeams[i].TeamLeaderName = "null";
                                }

                                existingSW.WriteLine(f1.SDB.mySalesPeople[i].FullName + ";" + f1.SDB.mySalesPeople[i].Birthday + ";" + f1.SDB.mySalesPeople[i].Number + ";" + f1.SDB.mySalesPeople[i].Email + ";" + f1.SDB.mySalesPeople[i].HomeAddress + ";" + f1.SDB.mySalesPeople[i].Country + ";" + f1.SDB.mySalesPeople[i].Region + ";" + f1.SDB.mySalesPeople[i].Sales + ";" + f1.SDB.mySalesPeople[i].TeamName + ";" + f1.SDB.mySalesPeople[i].StaffNumber + ">" + f1.SDB.myTeams[i].TeamName + ":" + f1.SDB.myTeams[i].TeamAreaName + ":" + f1.SDB.myTeams[i].TeamLeaderName + ":" + f1.SDB.myTeams[i].Members + ":" + f1.SDB.myTeams[i].Amount);
                            }
                            else
                            {
                                existingSW.WriteLine(f1.SDB.mySalesPeople[i].FullName + ";" + f1.SDB.mySalesPeople[i].Birthday + ";" + f1.SDB.mySalesPeople[i].Number + ";" + f1.SDB.mySalesPeople[i].Email + ";" + f1.SDB.mySalesPeople[i].HomeAddress + ";" + f1.SDB.mySalesPeople[i].Country + ";" + f1.SDB.mySalesPeople[i].Region + ";" + f1.SDB.mySalesPeople[i].Sales + ";" + f1.SDB.mySalesPeople[i].TeamName + ";" + f1.SDB.mySalesPeople[i].StaffNumber + ">");
                            }
                        }
                    }
                }

                existingSW.Close();
                existing.Close();
            }
        }

        /*This event is used to print a record of a selected sales person
         */
        private void selectedSalespersonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int staffnumber;
            string name;
            PrintingWindow printWindow = new PrintingWindow(f1);

            if (printWindow.ShowDialog() == DialogResult.OK)
            {
                if (printWindow.printCheckedListBox.CheckedItems.Count > 0)
                {
                    name = printWindow.printCheckedListBox.CheckedItems[0].ToString();

                    string[] names = name.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                    staffnumber = Convert.ToInt32(names[1]);

                    f1.SDB.PageToPrintSelectedPerson(staffnumber);
                }
            }

            PrintDocument document = new PrintDocument();

            document.PrintPage += new PrintPageEventHandler(f1.print.PrintTextHandler);

            PrintDialog printDG = new PrintDialog();

            if (printDG.ShowDialog() == DialogResult.OK)
            {
                document.Print();
            }
        }

        /*This click event is used to print all the sales people records
         */
        private void allSalespeopleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            f1.SDB.PageToPrintAllPeople();

            PrintDocument document = new PrintDocument();

            document.PrintPage += new PrintPageEventHandler(f1.print.PrintTextHandler);

            PrintDialog printDG = new PrintDialog();

            if (printDG.ShowDialog() == DialogResult.OK)
            {
                document.Print();
            }
        }

        /*This click event is used to print a selected record of a team
         */
        private void selectedTeamToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string name;

            PrintingWindow printWindow = new PrintingWindow(f1);

            printWindow.printTextBox.Visible = false;
            printWindow.label1.Visible = false;
            printWindow.label3.Visible = false;
            printWindow.searchPrintButton.Visible = false;

            f1.SDB.ListTeams(printWindow.printCheckedListBox.Items);

            if (printWindow.ShowDialog() == DialogResult.OK)
            {
                if (printWindow.printCheckedListBox.CheckedItems.Count > 0)
                {
                    name = printWindow.printCheckedListBox.CheckedItems[0].ToString();

                    f1.SDB.PageToPrintSelectedTeam(name);
                }
            }

            PrintDocument document = new PrintDocument();

            document.PrintPage += new PrintPageEventHandler(f1.print.PrintTextHandler);

            PrintDialog printDG = new PrintDialog();

            if (printDG.ShowDialog() == DialogResult.OK)
            {
                document.Print();
            }
        }

        /*This event is used to print all the team records
         */
        private void allTeamsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            f1.SDB.PageToPrintAllTeams();

            PrintDocument document = new PrintDocument();

            document.PrintPage += new PrintPageEventHandler(f1.print.PrintTextHandler);

            PrintDialog printDG = new PrintDialog();

            if (printDG.ShowDialog() == DialogResult.OK)
            {
                document.Print();
            }
        }

        /*This click event opens the help window which shows the help information for 
         * using the system
         */
        private void helpToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FileStream help = new FileStream(@"..\..\Help\Help.txt", FileMode.Open);

            StreamReader helpSR = new StreamReader(help);

            Help helpWindow = new Help();

            helpWindow.helpTextBox.Text = helpSR.ReadToEnd();

            helpSR.Close();

            helpWindow.Show(this);
        }

        /*This click event shows the relevant information about the system
         */
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileStream about = new FileStream(@"..\..\Help\About.txt", FileMode.Open);

            StreamReader aboutSR = new StreamReader(about);

            About aboutWindow = new About();

            aboutWindow.aboutTextBox.Text = aboutSR.ReadToEnd();

            aboutSR.Close();

            aboutWindow.Show(this);
        }
    }
}
