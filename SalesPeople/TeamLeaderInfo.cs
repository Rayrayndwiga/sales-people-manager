﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SalesPeople
{
    public partial class TeamLeaderInfo : Form
    {
        
        public TeamLeaderInfo()
        {
            InitializeComponent();
            
        }

        /*This event prevents one from choosing multiple items in the checked
         * listbox
         */
        private void teamMembersCheckedListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (teamMembersCheckedListBox.CheckedItems.Count >= 1 && e.CurrentValue != CheckState.Checked)
            {
                e.NewValue = e.CurrentValue;
            }
        }

        

    }
}
