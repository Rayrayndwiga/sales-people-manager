﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesPeople
{
    public class SalesPerson
    {
        private string firstName;
        private string secondName;
        private string lastName;
        private string AreaName;
        private string address;
        private string email;
        private string number;
        private float sales;
        private string birthday;
        private string country;
        private int staffnumber;
        public string teamName="";

        /*This is the constructor for the Salesperson class
         */
        public SalesPerson(string name,string birthday,string number,string email,string address,string country,string area,float sales,int staffnumber)
        {
            SetNames(name);
            this.number = number;
            this.email = email;
            this.birthday = birthday;
            this.address = address;
            this.country = country;
            AreaName = area;
            this.sales = sales;
            this.staffnumber = staffnumber;
        }

        /*This is a method that is used to assign the full name of a sales person
         */
        private void SetNames(string name)
        {
            string[] names = name.Split(new string[] { " ", "." }, StringSplitOptions.RemoveEmptyEntries);

            int n = names.Length;

            firstName = secondName = lastName = "";

            if (n == 1)
                firstName = names[0];
            else if (n == 2)
            {
                firstName = names[0];
                lastName = names[n - 1];
            }
            else
            {
                firstName = names[0];
                secondName = names[1];
                lastName = names[n - 1];

            }
        }

       
        /*This is a property for the full name of the sales person
         */
        public string FullName
        {
            get { return firstName + " " + secondName + " " + lastName; }
            set { SetNames(value); }
        }

        /*This is a property for the date of birth of the sales person
         */
        public string Birthday
        {
            get { return birthday; }
            set { birthday = value; }
        }
        /*This is a property for the home address of a sales person
         */
        public string HomeAddress
        {
            get { return address; }
            set { address = value; }
        }

        /*This is a property for the email address of a salesperson
         */
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        /*This is a property for the country of 
         * a sales person
         */
        public string Country
        {
            get { return country; }
            set { country = value; }
        }
        /*This is a property of the Region of operation of a sales person
         */
        public string Region
        {
            get { return AreaName; }
            set { AreaName = value; }
        }

        /*This is a property of the phone number of a sales person
         */
        public string Number
        {
            get { return number; }
            set { number = value; }
        }

        /*This is a proerty for the total sales of a sales person
         */
        public float Sales
        {
            get { return sales; }
            set { sales = value; }
        }

        /*This property is used to store the staff number of the sales person
         */
        public int StaffNumber
        {
            get { return staffnumber; }
            set { staffnumber = value; }
        }


        /*This property is used to show the team that a salesperson is in
         */
        public string TeamName
        {
            get { return teamName; }
            set { teamName = value; }
        }

        /*This method checks whether the a name is equal to the name of a partiular sales person
         */
        public bool IsSame(string name)
        {
            string[] names = name.Split(new string[] { " ", "." }, StringSplitOptions.RemoveEmptyEntries);

            int n = names.Length;

            if (n > 1)
                return lastName.Equals(names[n - 1]) && firstName.Equals(names[0]);
            else if (n == 1)
                return firstName.Equals(names[0]);
            else
                return false;

        }

        /*This methos is used to check if the Area of the sales person passed into the method is the same as
         * the area of a particular sales person
         */
        public bool IsPersonArea(string name)
        {
            string[] names = name.Split(new string[] { " ", "." }, StringSplitOptions.RemoveEmptyEntries);

            int n = names.Length;


            if (n == 1)
                return Region.Equals(names[0]);
            else
                return false;
        }

        
    }
}
