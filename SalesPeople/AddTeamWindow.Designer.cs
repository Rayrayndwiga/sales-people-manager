﻿namespace SalesPeople
{
    partial class AddTeamWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.addTeamButton = new System.Windows.Forms.Button();
            this.closeTeamButton = new System.Windows.Forms.Button();
            this.teamNameTexBox = new System.Windows.Forms.TextBox();
            this.errorProviderAddTeam = new System.Windows.Forms.ErrorProvider(this.components);
            this.operationAreaComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderAddTeam)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Team Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Area of Operation";
            // 
            // addTeamButton
            // 
            this.addTeamButton.Location = new System.Drawing.Point(143, 110);
            this.addTeamButton.Name = "addTeamButton";
            this.addTeamButton.Size = new System.Drawing.Size(75, 23);
            this.addTeamButton.TabIndex = 2;
            this.addTeamButton.Text = "Add";
            this.addTeamButton.UseVisualStyleBackColor = true;
            this.addTeamButton.Click += new System.EventHandler(this.addTeamButton_Click);
            // 
            // closeTeamButton
            // 
            this.closeTeamButton.Location = new System.Drawing.Point(277, 110);
            this.closeTeamButton.Name = "closeTeamButton";
            this.closeTeamButton.Size = new System.Drawing.Size(75, 23);
            this.closeTeamButton.TabIndex = 3;
            this.closeTeamButton.Text = "Close";
            this.closeTeamButton.UseVisualStyleBackColor = true;
            this.closeTeamButton.Click += new System.EventHandler(this.closeTeamButton_Click);
            // 
            // teamNameTexBox
            // 
            this.teamNameTexBox.Location = new System.Drawing.Point(143, 77);
            this.teamNameTexBox.Name = "teamNameTexBox";
            this.teamNameTexBox.Size = new System.Drawing.Size(209, 20);
            this.teamNameTexBox.TabIndex = 4;
            this.teamNameTexBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.teamNameTexBox_KeyPress);
            // 
            // errorProviderAddTeam
            // 
            this.errorProviderAddTeam.ContainerControl = this;
            // 
            // operationAreaComboBox
            // 
            this.operationAreaComboBox.Location = new System.Drawing.Point(143, 37);
            this.operationAreaComboBox.Name = "operationAreaComboBox";
            this.operationAreaComboBox.Size = new System.Drawing.Size(209, 21);
            this.operationAreaComboBox.TabIndex = 6;
            this.operationAreaComboBox.SelectedIndexChanged += new System.EventHandler(this.operationAreaComboBox_SelectedIndexChanged);
            this.operationAreaComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.operationAreaComboBox_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(315, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Choose the area of operation and the team name will be assigned";
            // 
            // AddTeamWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(356, 150);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.operationAreaComboBox);
            this.Controls.Add(this.teamNameTexBox);
            this.Controls.Add(this.closeTeamButton);
            this.Controls.Add(this.addTeamButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AddTeamWindow";
            this.Text = "Add Team Window";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AddTeamWindow_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderAddTeam)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button addTeamButton;
        private System.Windows.Forms.Button closeTeamButton;
        private System.Windows.Forms.TextBox teamNameTexBox;
        private System.Windows.Forms.ErrorProvider errorProviderAddTeam;
        public System.Windows.Forms.ComboBox operationAreaComboBox;
        private System.Windows.Forms.Label label3;
    }
}