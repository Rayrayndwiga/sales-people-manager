﻿namespace SalesPeople
{
    partial class PrintingWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.printCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.searchPrintButton = new System.Windows.Forms.Button();
            this.printTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.printButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // printCheckedListBox
            // 
            this.printCheckedListBox.FormattingEnabled = true;
            this.printCheckedListBox.Location = new System.Drawing.Point(2, 75);
            this.printCheckedListBox.Name = "printCheckedListBox";
            this.printCheckedListBox.Size = new System.Drawing.Size(454, 229);
            this.printCheckedListBox.TabIndex = 0;
            this.printCheckedListBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.printCheckedListBox_ItemCheck);
            // 
            // searchPrintButton
            // 
            this.searchPrintButton.Location = new System.Drawing.Point(307, 26);
            this.searchPrintButton.Name = "searchPrintButton";
            this.searchPrintButton.Size = new System.Drawing.Size(125, 23);
            this.searchPrintButton.TabIndex = 1;
            this.searchPrintButton.Text = "Search Sales Person";
            this.searchPrintButton.UseVisualStyleBackColor = true;
            this.searchPrintButton.Click += new System.EventHandler(this.searchPrintButton_Click);
            // 
            // printTextBox
            // 
            this.printTextBox.Location = new System.Drawing.Point(61, 29);
            this.printTextBox.Name = "printTextBox";
            this.printTextBox.Size = new System.Drawing.Size(240, 20);
            this.printTextBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Search";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(165, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Choose the Item you want to print";
            // 
            // printButton
            // 
            this.printButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.printButton.Location = new System.Drawing.Point(2, 327);
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(75, 23);
            this.printButton.TabIndex = 5;
            this.printButton.Text = "Print";
            this.printButton.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(287, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Search for the Sales Person whose record you want to print";
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // PrintingWindow
            // 
            this.AcceptButton = this.printButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 362);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.printButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.printTextBox);
            this.Controls.Add(this.searchPrintButton);
            this.Controls.Add(this.printCheckedListBox);
            this.Name = "PrintingWindow";
            this.Text = "Printing Window";
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button printButton;
        public System.Windows.Forms.Button searchPrintButton;
        public System.Windows.Forms.TextBox printTextBox;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.CheckedListBox printCheckedListBox;
        private System.Windows.Forms.ErrorProvider errorProvider;
    }
}