﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesPeople
{
    public class Teams
    {
        private double sales = 0;
        public int members = 0;
        private string AreaName;
        private string firstTeamLeaderName;
        private string secondTeamLeaderName;
        private string lastTeamLeaderName;
        private string firstTeamName;
        private string secondTeamName;
        private string lastTeamName;
        public const int MAX_MEMBERS = 1000;
        public List<SalesPerson>TeamMembers;

        /*This is the constructor for the Teams class
         */
        public Teams(string name,string areaName)
        {
            SetTeamNames(name);
            AreaName = areaName;
            TeamMembers = new List<SalesPerson>(); 
        }


        /*This method sets the full name of the team leader
         */
        private void SetTeamLeaderNames(string name)
        {
            string[] names = name.Split(new string[] { " ", "." }, StringSplitOptions.RemoveEmptyEntries);

            int n = names.Length;

            firstTeamLeaderName = secondTeamLeaderName = lastTeamLeaderName = "";

            if (n == 0)
            {
                firstTeamLeaderName = "";
            }
            else if (n == 1)
            {
                firstTeamLeaderName = names[0];
            }
            else if (n == 2)
            {
                firstTeamLeaderName = names[0];
                lastTeamLeaderName = names[n - 1];
            }
            else
            {
                firstTeamLeaderName = names[0];
                secondTeamLeaderName = names[1];
                lastTeamLeaderName = names[n - 1];

            }
        }

        /*This method sets the full name of the team
         */
        private void SetTeamNames(string name)
        {
            string[] names = name.Split(new string[] { " ", "." }, StringSplitOptions.RemoveEmptyEntries);

            int n = names.Length;

            firstTeamName = secondTeamName = lastTeamName = "";

            if (n == 1)
            {
                firstTeamName = names[0];
            }
            else if (n == 2)
            {
                firstTeamName = names[0];
                lastTeamName = names[n - 1];
            }
            else
            {
                firstTeamName = names[0];
                secondTeamName = names[1];
                lastTeamName = names[n - 1];

            }
        }

        /*This is a property for the team name
         */
        public string TeamName
        {
            get {return firstTeamName + " " + secondTeamName + " " + lastTeamName; }
            set { SetTeamNames(value); }
        }

        /*This is a property of the area of a team
         */
        public string TeamAreaName
        {
            get { return AreaName; }
            set { AreaName=value; }
        }

        /*This is a property for the name of the team leader
         */
        public string TeamLeaderName
        {
            get { return firstTeamLeaderName + " " + secondTeamLeaderName + " " + lastTeamLeaderName; }
            set { SetTeamLeaderNames(value); }
        }

        /*This is a property for the number of members in a team
         */
        public int Members
        {
            get { return this.members; }
            set { this.members = value; }
        }

        /*This is a property for the total sales amount of the team
         */
        public double Amount
        {
            get { return this.sales; }
            set { this.sales = value; }
        }


        public bool IsTeamArea(string name)
        {
            string[] names = name.Split(new string[] { " ", "." }, StringSplitOptions.RemoveEmptyEntries);

            int n = names.Length;

            if (n == 1)
                return TeamAreaName.Equals(names[0]);
            else
                return false;
        }

        /*This is a method that is used to check if the name that is passed into the method is the same
         * as the name of a team
         */
        public bool IsTeamName(string name)
        {
            string[] names = name.Split(new string[] { " ", "." }, StringSplitOptions.RemoveEmptyEntries);

            int n = names.Length;

            if (n > 1)
                return lastTeamName.Equals(names[n - 1]) && firstTeamName.Equals(names[0]);
            else if (n == 1)
                return firstTeamName.Equals(names[0]);
            else
                return false;
        }

    }
}
