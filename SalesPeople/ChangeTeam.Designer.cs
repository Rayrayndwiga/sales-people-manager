﻿namespace SalesPeople
{
    partial class ChangeTeam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.updateTeamOperationAreaButton = new System.Windows.Forms.Button();
            this.updateTeamNameTextBox = new System.Windows.Forms.TextBox();
            this.updateTeamOperationAreaTextBox = new System.Windows.Forms.TextBox();
            this.closeButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.updateTeamLeaderButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.updateTeamLeaderTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // updateTeamOperationAreaButton
            // 
            this.updateTeamOperationAreaButton.Location = new System.Drawing.Point(281, 39);
            this.updateTeamOperationAreaButton.Name = "updateTeamOperationAreaButton";
            this.updateTeamOperationAreaButton.Size = new System.Drawing.Size(75, 23);
            this.updateTeamOperationAreaButton.TabIndex = 1;
            this.updateTeamOperationAreaButton.Text = "Update";
            this.updateTeamOperationAreaButton.UseVisualStyleBackColor = true;
            this.updateTeamOperationAreaButton.Click += new System.EventHandler(this.updateTeamOperationAreaButton_Click);
            // 
            // updateTeamNameTextBox
            // 
            this.updateTeamNameTextBox.Location = new System.Drawing.Point(107, 104);
            this.updateTeamNameTextBox.Name = "updateTeamNameTextBox";
            this.updateTeamNameTextBox.ReadOnly = true;
            this.updateTeamNameTextBox.Size = new System.Drawing.Size(165, 20);
            this.updateTeamNameTextBox.TabIndex = 2;
            // 
            // updateTeamOperationAreaTextBox
            // 
            this.updateTeamOperationAreaTextBox.Location = new System.Drawing.Point(110, 43);
            this.updateTeamOperationAreaTextBox.Name = "updateTeamOperationAreaTextBox";
            this.updateTeamOperationAreaTextBox.ReadOnly = true;
            this.updateTeamOperationAreaTextBox.Size = new System.Drawing.Size(165, 20);
            this.updateTeamOperationAreaTextBox.TabIndex = 3;
            // 
            // closeButton
            // 
            this.closeButton.Location = new System.Drawing.Point(281, 196);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 4;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Team Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Area of Operation";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(61, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(271, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Click the update button to change the respective details";
            // 
            // updateTeamLeaderButton
            // 
            this.updateTeamLeaderButton.Location = new System.Drawing.Point(281, 158);
            this.updateTeamLeaderButton.Name = "updateTeamLeaderButton";
            this.updateTeamLeaderButton.Size = new System.Drawing.Size(75, 23);
            this.updateTeamLeaderButton.TabIndex = 8;
            this.updateTeamLeaderButton.Text = "Update";
            this.updateTeamLeaderButton.UseVisualStyleBackColor = true;
            this.updateTeamLeaderButton.Click += new System.EventHandler(this.updateTeamLeaderButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 168);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Team Leader";
            // 
            // updateTeamLeaderTextBox
            // 
            this.updateTeamLeaderTextBox.Location = new System.Drawing.Point(110, 161);
            this.updateTeamLeaderTextBox.Name = "updateTeamLeaderTextBox";
            this.updateTeamLeaderTextBox.ReadOnly = true;
            this.updateTeamLeaderTextBox.Size = new System.Drawing.Size(165, 20);
            this.updateTeamLeaderTextBox.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 88);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(334, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "The team name will be automatically assigned after choosing the area";
            // 
            // ChangeTeam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(368, 255);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.updateTeamLeaderTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.updateTeamLeaderButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.updateTeamOperationAreaTextBox);
            this.Controls.Add(this.updateTeamNameTextBox);
            this.Controls.Add(this.updateTeamOperationAreaButton);
            this.Name = "ChangeTeam";
            this.Text = "Change Team";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ChangeTeam_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button updateTeamOperationAreaButton;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox updateTeamNameTextBox;
        public System.Windows.Forms.TextBox updateTeamOperationAreaTextBox;
        private System.Windows.Forms.Button updateTeamLeaderButton;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox updateTeamLeaderTextBox;
        private System.Windows.Forms.Label label5;
    }
}