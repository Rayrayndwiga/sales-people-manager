﻿namespace SalesPeople
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.AddPersonButton = new System.Windows.Forms.Button();
            this.removeSalespersonButton = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesPersonRecordsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectedRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allRecordsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.teamRecordsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectedTeamToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.allTeamsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTipForm1 = new System.Windows.Forms.ToolTip(this.components);
            this.errorProviderForm1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.UpdateDetailsButton = new System.Windows.Forms.Button();
            this.showTeamsButton = new System.Windows.Forms.Button();
            this.searchSalesPersonButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderForm1)).BeginInit();
            this.SuspendLayout();
            // 
            // AddPersonButton
            // 
            this.AddPersonButton.Location = new System.Drawing.Point(310, 62);
            this.AddPersonButton.Name = "AddPersonButton";
            this.AddPersonButton.Size = new System.Drawing.Size(177, 51);
            this.AddPersonButton.TabIndex = 1;
            this.AddPersonButton.Text = "Add Sales Person\r\n\r\n";
            this.AddPersonButton.UseVisualStyleBackColor = true;
            this.AddPersonButton.Click += new System.EventHandler(this.AddPersonButton_Click);
            // 
            // removeSalespersonButton
            // 
            this.removeSalespersonButton.Location = new System.Drawing.Point(310, 136);
            this.removeSalespersonButton.Name = "removeSalespersonButton";
            this.removeSalespersonButton.Size = new System.Drawing.Size(177, 51);
            this.removeSalespersonButton.TabIndex = 3;
            this.removeSalespersonButton.Text = "Remove Sales Person";
            this.removeSalespersonButton.UseVisualStyleBackColor = true;
            this.removeSalespersonButton.Click += new System.EventHandler(this.removeSalespersonButton_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(770, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveAsToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.printToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.saveAsToolStripMenuItem.Text = "Save As";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salesPersonRecordsToolStripMenuItem,
            this.teamRecordsToolStripMenuItem});
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.printToolStripMenuItem.Text = "Print";
            // 
            // salesPersonRecordsToolStripMenuItem
            // 
            this.salesPersonRecordsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectedRecordToolStripMenuItem,
            this.allRecordsToolStripMenuItem});
            this.salesPersonRecordsToolStripMenuItem.Name = "salesPersonRecordsToolStripMenuItem";
            this.salesPersonRecordsToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.salesPersonRecordsToolStripMenuItem.Text = "SalesPerson Records";
            // 
            // selectedRecordToolStripMenuItem
            // 
            this.selectedRecordToolStripMenuItem.Name = "selectedRecordToolStripMenuItem";
            this.selectedRecordToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.selectedRecordToolStripMenuItem.Text = "Selected Record";
            this.selectedRecordToolStripMenuItem.Click += new System.EventHandler(this.selectedRecordToolStripMenuItem_Click);
            // 
            // allRecordsToolStripMenuItem
            // 
            this.allRecordsToolStripMenuItem.Name = "allRecordsToolStripMenuItem";
            this.allRecordsToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.allRecordsToolStripMenuItem.Text = "All Records";
            this.allRecordsToolStripMenuItem.Click += new System.EventHandler(this.allRecordsToolStripMenuItem_Click);
            // 
            // teamRecordsToolStripMenuItem
            // 
            this.teamRecordsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectedTeamToolStripMenuItem1,
            this.allTeamsToolStripMenuItem1});
            this.teamRecordsToolStripMenuItem.Name = "teamRecordsToolStripMenuItem";
            this.teamRecordsToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.teamRecordsToolStripMenuItem.Text = "Team Records";
            // 
            // selectedTeamToolStripMenuItem1
            // 
            this.selectedTeamToolStripMenuItem1.Name = "selectedTeamToolStripMenuItem1";
            this.selectedTeamToolStripMenuItem1.Size = new System.Drawing.Size(151, 22);
            this.selectedTeamToolStripMenuItem1.Text = "Selected Team";
            this.selectedTeamToolStripMenuItem1.Click += new System.EventHandler(this.selectedTeamToolStripMenuItem1_Click);
            // 
            // allTeamsToolStripMenuItem1
            // 
            this.allTeamsToolStripMenuItem1.Name = "allTeamsToolStripMenuItem1";
            this.allTeamsToolStripMenuItem1.Size = new System.Drawing.Size(151, 22);
            this.allTeamsToolStripMenuItem1.Text = "All Teams";
            this.allTeamsToolStripMenuItem1.Click += new System.EventHandler(this.allTeamsToolStripMenuItem1_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem1,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // helpToolStripMenuItem1
            // 
            this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            this.helpToolStripMenuItem1.Size = new System.Drawing.Size(107, 22);
            this.helpToolStripMenuItem1.Text = "Help";
            this.helpToolStripMenuItem1.Click += new System.EventHandler(this.helpToolStripMenuItem1_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // errorProviderForm1
            // 
            this.errorProviderForm1.ContainerControl = this;
            // 
            // UpdateDetailsButton
            // 
            this.UpdateDetailsButton.Location = new System.Drawing.Point(310, 289);
            this.UpdateDetailsButton.Name = "UpdateDetailsButton";
            this.UpdateDetailsButton.Size = new System.Drawing.Size(177, 56);
            this.UpdateDetailsButton.TabIndex = 13;
            this.UpdateDetailsButton.Text = "Update Details of  a sales person";
            this.UpdateDetailsButton.UseVisualStyleBackColor = true;
            this.UpdateDetailsButton.Click += new System.EventHandler(this.UpdateDetailsButton_Click);
            // 
            // showTeamsButton
            // 
            this.showTeamsButton.Location = new System.Drawing.Point(310, 363);
            this.showTeamsButton.Name = "showTeamsButton";
            this.showTeamsButton.Size = new System.Drawing.Size(177, 56);
            this.showTeamsButton.TabIndex = 14;
            this.showTeamsButton.Text = " Teams ";
            this.showTeamsButton.UseVisualStyleBackColor = true;
            this.showTeamsButton.Click += new System.EventHandler(this.showTeamsButton_Click);
            // 
            // searchSalesPersonButton
            // 
            this.searchSalesPersonButton.Location = new System.Drawing.Point(310, 206);
            this.searchSalesPersonButton.Name = "searchSalesPersonButton";
            this.searchSalesPersonButton.Size = new System.Drawing.Size(177, 59);
            this.searchSalesPersonButton.TabIndex = 16;
            this.searchSalesPersonButton.Text = "Search Sales Person";
            this.searchSalesPersonButton.UseVisualStyleBackColor = true;
            this.searchSalesPersonButton.Click += new System.EventHandler(this.searchSalesPersonButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(326, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 25);
            this.label1.TabIndex = 17;
            this.label1.Text = "MAIN MENU";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(770, 533);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.searchSalesPersonButton);
            this.Controls.Add(this.showTeamsButton);
            this.Controls.Add(this.UpdateDetailsButton);
            this.Controls.Add(this.removeSalespersonButton);
            this.Controls.Add(this.AddPersonButton);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "SALES IN THE CENTURY PTY LTD";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderForm1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AddPersonButton;
        private System.Windows.Forms.Button removeSalespersonButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTipForm1;
        private System.Windows.Forms.ErrorProvider errorProviderForm1;
        private System.Windows.Forms.Button UpdateDetailsButton;
        private System.Windows.Forms.Button showTeamsButton;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.Button searchSalesPersonButton;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesPersonRecordsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectedRecordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allRecordsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem teamRecordsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectedTeamToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem allTeamsToolStripMenuItem1;
        private System.Windows.Forms.Label label1;
    }
}

