﻿namespace SalesPeople
{
    partial class remove
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.removePersonButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.removeSalesPersonTextBox = new System.Windows.Forms.TextBox();
            this.removeSalesPersonSearchButton = new System.Windows.Forms.Button();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.removeSalespersonCheckListBox = new System.Windows.Forms.CheckedListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(216, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Search the Salesperson you want to remove";
            // 
            // removePersonButton
            // 
            this.removePersonButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.removePersonButton.Location = new System.Drawing.Point(16, 392);
            this.removePersonButton.Name = "removePersonButton";
            this.removePersonButton.Size = new System.Drawing.Size(75, 23);
            this.removePersonButton.TabIndex = 4;
            this.removePersonButton.Text = "Remove";
            this.removePersonButton.UseVisualStyleBackColor = true;
            this.removePersonButton.Click += new System.EventHandler(this.removePersonButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Search";
            // 
            // removeSalesPersonTextBox
            // 
            this.removeSalesPersonTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.removeSalesPersonTextBox.Location = new System.Drawing.Point(61, 32);
            this.removeSalesPersonTextBox.Name = "removeSalesPersonTextBox";
            this.removeSalesPersonTextBox.Size = new System.Drawing.Size(149, 20);
            this.removeSalesPersonTextBox.TabIndex = 6;
            this.toolTip1.SetToolTip(this.removeSalesPersonTextBox, "Fill in the name");
            // 
            // removeSalesPersonSearchButton
            // 
            this.removeSalesPersonSearchButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.removeSalesPersonSearchButton.Location = new System.Drawing.Point(216, 30);
            this.removeSalesPersonSearchButton.Name = "removeSalesPersonSearchButton";
            this.removeSalesPersonSearchButton.Size = new System.Drawing.Size(93, 23);
            this.removeSalesPersonSearchButton.TabIndex = 7;
            this.removeSalesPersonSearchButton.Text = "Search";
            this.removeSalesPersonSearchButton.UseVisualStyleBackColor = true;
            this.removeSalesPersonSearchButton.Click += new System.EventHandler(this.removeSalesPersonSearchButton_Click);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // removeSalespersonCheckListBox
            // 
            this.removeSalespersonCheckListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.removeSalespersonCheckListBox.FormattingEnabled = true;
            this.removeSalespersonCheckListBox.Location = new System.Drawing.Point(16, 105);
            this.removeSalespersonCheckListBox.Name = "removeSalespersonCheckListBox";
            this.removeSalespersonCheckListBox.Size = new System.Drawing.Size(293, 274);
            this.removeSalespersonCheckListBox.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(275, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Check the name of the Sales person you want to remove";
            // 
            // remove
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(328, 427);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.removeSalespersonCheckListBox);
            this.Controls.Add(this.removeSalesPersonSearchButton);
            this.Controls.Add(this.removeSalesPersonTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.removePersonButton);
            this.Controls.Add(this.label1);
            this.Name = "remove";
            this.Text = "Remove Sales Person";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.remove_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button removePersonButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox removeSalesPersonTextBox;
        private System.Windows.Forms.Button removeSalesPersonSearchButton;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.CheckedListBox removeSalespersonCheckListBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}