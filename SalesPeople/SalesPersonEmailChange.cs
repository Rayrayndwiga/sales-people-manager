﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SalesPeople
{
    public partial class SalesPersonEmailChange : Form
    {
        public SalesPersonEmailChange()
        {
            InitializeComponent();
        }

        /*This sets out what happens when the form is closing
         */
        private void SalesPersonEmailChange_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.OK)
            {
                DialogResult change = MessageBox.Show("Are you sure you want to make the change", "Making Change", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (change == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }
    }
}
