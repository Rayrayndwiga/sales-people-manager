﻿namespace SalesPeople
{
    partial class SalesPersonTotalSalesChange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.salesPersonTotalSalesChangeTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.errorProviderSalesTotalSalesChange = new System.Windows.Forms.ErrorProvider(this.components);
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderSalesTotalSalesChange)).BeginInit();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(26, 121);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 0;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(185, 121);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // salesPersonTotalSalesChangeTextBox
            // 
            this.salesPersonTotalSalesChangeTextBox.Location = new System.Drawing.Point(26, 76);
            this.salesPersonTotalSalesChangeTextBox.Name = "salesPersonTotalSalesChangeTextBox";
            this.salesPersonTotalSalesChangeTextBox.Size = new System.Drawing.Size(234, 20);
            this.salesPersonTotalSalesChangeTextBox.TabIndex = 2;
            this.salesPersonTotalSalesChangeTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.salesPersonTotalSalesChangeTextBox_Validating);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(209, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Enter the new Total Sales of the Personnel";
            // 
            // errorProviderSalesTotalSalesChange
            // 
            this.errorProviderSalesTotalSalesChange.ContainerControl = this;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 79);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "$";
            // 
            // SalesPersonTotalSalesChange
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(284, 181);
            this.ControlBox = false;
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.salesPersonTotalSalesChangeTextBox);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Name = "SalesPersonTotalSalesChange";
            this.Text = "Sales Person Total Sales Change";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SalesPersonTotalSalesChange_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderSalesTotalSalesChange)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox salesPersonTotalSalesChangeTextBox;
        public System.Windows.Forms.ErrorProvider errorProviderSalesTotalSalesChange;
        private System.Windows.Forms.Label label10;
    }
}